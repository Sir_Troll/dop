﻿#region

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using NAPI;
using Torch.Commands;
using TorchPlugin;

#endregion

namespace Slime.GUI
{
	public partial class CommandsGui : UserControl {
        public CommandsGui()
        {
            InitializeComponent();


            var sb = new StringBuilder();
            foreach (var t in CommandsPlugin.commands)
            {
                var a = t.GetCustomAttribute<CategoryAttribute>();
                var prefix = "!";
                if (a != null)
                {
                    var p = a.Path;
                    prefix += String.Join(" ", p) + " ";
                }

                var l = new List<Torch.Commands.CommandAttribute>();
                Ext2.GetCommandsInfo(t, l);

                sb.Append(t.Name + ":\n");
                foreach (var x in l)
                {
                    sb.Append("\t").Append(prefix).Append (x.Name).Append(": ").AppendLine(x.Description);
                }
            }

            Commands.Text = sb.ToString();
            
        }
    }
}