﻿using System.Collections.Generic;
using Sandbox.Game.Entities;
using Torch.Commands;

namespace Slime.Features.Commands {
    [Category("fix")]
    class FixSafezone : CommandModule {
        [Command("safezone", "Removes safezones, that are assigned to blocks, but that blocks haven't found")]
        public void FixSafezoneCommand() {
            HashSet<long> list = new HashSet<long>();
            foreach (var x in MySessionComponentSafeZones.SafeZones) {
                if (x.SafeZoneBlockId != 0) {
                    if (!MyEntities.TryGetEntityById(x.SafeZoneBlockId, out var block)) { list.Add(x.EntityId); }
                }
            }

            foreach (var x in list) {
                MySessionComponentSafeZones.RequestDeleteSafeZone(x);
            }
        }
    }
}