﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Torch.Commands;
using Sandbox.Game.World;
using VRage.Game.ModAPI;
using Sandbox.Game.Screens.Helpers;
using Torch.Commands.Permissions;
using NAPI;
using Sandbox.Engine.Physics;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.ModAPI.Ingame;
using Scripts.Shared;
using VRageMath;
using VRageMath.Spatial;

namespace Slime.Features.Commands {

    [Category("show")]
    class FixClippingGrids : CommandModule {


        private static Func<MyClusterTree, Dictionary<ulong, object>> hkWorld = typeof(MyClusterTree).easyField("m_objectsData").CreateGetter<MyClusterTree, Dictionary<ulong, object>>();
        //m_objectsData[objectId]


       [Command("clipping-grids")]
        [Permission(MyPromoteLevel.None)]
        public void FixSmallGridsCommand() {
            var grids = UtilitesEntity.GetAllGrids(
                (x)=>
                {
                    var p = x.Physics;


                    var r = p != null && p.Gravity.LengthSquared() > 0 && p.IsActive &&
                            p.LinearVelocity.LengthSquared() > 0 &&
                            p.LinearVelocity.LengthSquared() < 10 * 10;


                    if (!r) return false;
                    if (!TryGetPlanetElevation (x, MyPlanetElevation.Surface, out var elevation) || elevation > 300)
                    {
                        return false;
                    }
                    return true;
                });

            Dictionary<MyCubeGrid, List<IMyCubeGrid>> gridGroups = new Dictionary<MyCubeGrid, List<IMyCubeGrid>>();

            foreach (var x in grids)
            {
                if (gridGroups.ContainsKey(x)) continue;
                var gg = x.GetConnectedGrids(GridLinkTypeEnum.Physical);

                var station = grids.Find ((g)=>g.IsStatic);
                if (station !=null) continue;

                foreach (var y in gg)
                {
                    gridGroups[y as MyCubeGrid] = gg;
                }
            }

            //var dictionary 
            //UtilitesEntity.Cluster<>

            var sb = new StringBuilder ("Possible grids in voxels: ");



            //Match Grid to lagging hkWorld

            //MyPhysics.Clusters.GetAll()[0].
            //foreach (var c in MyPhysics.Clusters.GetClusters())
            //{
            //    c.Objects
            //    c.Objects.Contains (grid.Physics.RigidBody.NativeObject);
            //}

            foreach (var x in gridGroups.Values)
            {
                for (var y=0; y<x.Count; y++)
                {
                    var grid = (x[y] as MyCubeGrid);
                    if (grid.BlocksCount == 1 && x[y].HasBlockType("MyWheel")) continue;

                    if (Voxels.IsInVoxels(grid.WorldMatrix, x[y].LocalAABB, 0.05f))
                    {
                        sb.Append ($"{grid.DisplayName} at {grid.WorldMatrix.Translation} HkEntityId = {grid.Physics.RigidBody.NativeObject} HkWorldId = {grid.Physics.RigidBody.NativeObject} \n");
                    }
                }
            }


        }

        bool TryGetPlanetElevation(MyCubeGrid grid, MyPlanetElevation detail, out double elevation)
        {
            var boundingBox = grid.PositionComp.WorldAABB;
            var nearestPlanet = MyGamePruningStructure.GetClosestPlanet(ref boundingBox);
            if (nearestPlanet == null)
            {
                elevation = double.PositiveInfinity;
                return false;
            }

            switch (detail)
            {
                case MyPlanetElevation.Sealevel:
                    elevation = ((boundingBox.Center - nearestPlanet.PositionComp.GetPosition()).Length() - nearestPlanet.AverageRadius);
                    return true;

                case MyPlanetElevation.Surface:
                    var controlledEntityPosition = grid.Physics.CenterOfMassWorld;
                    Vector3D closestPoint = nearestPlanet.GetClosestSurfacePointGlobal(ref controlledEntityPosition);
                    elevation = Vector3D.Distance(closestPoint, controlledEntityPosition);
                    return true;

                default:
                    throw new ArgumentOutOfRangeException("detail", detail, null);
            }
        }
    }
}