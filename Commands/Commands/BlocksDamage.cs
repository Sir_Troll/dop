﻿using Torch.Commands;
using System;
using VRage.ObjectBuilders;
using Slime.Utils;
using NAPI;

namespace Slime.Features {
    class BlocksDamage : CommandModule {
        [Command("damage", "Damage block by integrity, if FatOnly - only functional blocks but it is MUCH FASTER")]
        public void DamageBlocks2(string like, float integrityDamage, float minIntegrity = -1, bool fatOnly = false) {
            Damage2(like, integrityDamage, minIntegrity, fatOnly);
        }
        
        public void Damage2(string like, float integrityDamage, float minimum, bool fatOnly) {
            try {
                var blocksToDamage = fatOnly ? UtilitesEntity.GetFatBlocksFromAllGrids((b) => b.idIsLike(like)) : UtilitesEntity.GetBlocksFromAllGrids((b) => b.idIsLike(like));
                UtilitiesDamageIntegrity.DamageBlocks2(blocksToDamage, integrityDamage, minimum);
                Context.Respond($"Damaged {blocksToDamage.Count} blocks by {integrityDamage} integrity.");
            } catch (Exception e) {
                Log.Error($"ERROR: {e.Message}");
            }
        }
    }
}