﻿using System;
using System.Collections.Generic;
using Sandbox.ModAPI;
using VRage.Game;
using VRageMath;

namespace Foogs
{
    public static class Communication
    {
        public const ushort NETWORK_ID = 10666;
        public static void RegisterHandlers()
        {
            MyAPIGateway.Multiplayer.RegisterMessageHandler(NETWORK_ID, MessageHandler);
            SGarage.Instance.SomeLog("Register communication handlers");
        }

        public static void UnregisterHandlers()
        {
            MyAPIGateway.Multiplayer.UnregisterMessageHandler(NETWORK_ID, MessageHandler);
        }

        private static void MessageHandler(byte[] bytes)
        {
            try
            {
                var type = (MessageType)bytes[0];
                SGarage.Instance.SomeLog($"Received message: {bytes[0]}: {type}");
                var data = new byte[bytes.Length - 1];
                Array.Copy(bytes, 1, data, 0, data.Length);
                switch (type)
                {
                    case MessageType.SaveGridReq:
                        OnSaveGridReq(data);
                        break;
                    case MessageType.ClearGridReq:
                        OnClearGridReq(data);
                        break;
                    case MessageType.LoadGridReq:
                        OnLoadGridReq(data);
                        break;
                    case MessageType.ShowGridReq:
                        OnShowGridReq(data);
                        break;
                    case MessageType.Notificaion:
                        OnNotificaion(data);
                        break;
                    case MessageType.ClientSettings:
                        OnClientReqSettings(data);
                        break;
                    case MessageType.GetBlockStatus:
                        OnClientReqGetBlockStatus(data);
                        break;
                    case MessageType.SetBlockStatus:
                        OnClientReqSetBlockStatus(data);
                        break;
                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                SGarage.Instance.SomeLog($"Error during message handle! {ex}");
            }
        }

        #region Receive
        private static void OnSaveGridReq(byte[] data)
        {
            var savegridreq = MyAPIGateway.Utilities.SerializeFromBinary<ClientReq>(data);
            IMyProjector p = MyAPIGateway.Entities.GetEntityById(savegridreq.BLOCK_entityId) as IMyProjector;
            //cheat check
            var grid = MyAPIGateway.Entities.GetEntityById(savegridreq.GridId);
            if (grid == null)
            {
                SGarage.Instance.SomeLog($"Grid find failed, try again.");
                return;
            }
            lock (MainLogic.ActionQueue)
            {
                MainLogic.ActionQueue.Enqueue(new MsgInfo
                (savegridreq.SteamId, ReqType.Save, p, grid, true, MatrixD.Zero, BoundingBoxD.CreateInvalid()));
            }
        }

        private static void OnLoadGridReq(byte[] data)
        {
            var loadgridreq = MyAPIGateway.Utilities.SerializeFromBinary<ClientReq>(data);
            IMyProjector p = MyAPIGateway.Entities.GetEntityById(loadgridreq.BLOCK_entityId) as IMyProjector;
            //cheat check
            var m = new MatrixD();
            m.Translation = loadgridreq.Pos;
            m.Up = loadgridreq.r_up;
            m.Forward = loadgridreq.r_forward;
            lock (MainLogic.ActionQueue)
            {
                MainLogic.ActionQueue.Enqueue(new MsgInfo
                (loadgridreq.SteamId, ReqType.Load, p, null, true, m, loadgridreq.WAABB));
            }
        }

        private static void OnShowGridReq(byte[] data)
        {
            var showgridreq = MyAPIGateway.Utilities.SerializeFromBinary<ClientReq>(data);
            IMyProjector p = MyAPIGateway.Entities.GetEntityById(showgridreq.BLOCK_entityId) as IMyProjector;
            //cheat check
            lock (MainLogic.ActionQueue)
            {
                MainLogic.ActionQueue.Enqueue(new MsgInfo
                (showgridreq.SteamId, ReqType.Show, p, null, true, MatrixD.Zero, BoundingBoxD.CreateInvalid()));
            }
        }

        private static void OnClearGridReq(byte[] data)
        {
            var loadgridreq = MyAPIGateway.Utilities.SerializeFromBinary<ClientReq>(data);
            IMyProjector p = MyAPIGateway.Entities.GetEntityById(loadgridreq.BLOCK_entityId) as IMyProjector;
            //cheat check
            lock (MainLogic.ActionQueue)
            {
                MainLogic.ActionQueue.Enqueue(new MsgInfo
                (loadgridreq.SteamId, ReqType.Clear, p, null, true, MatrixD.Zero, BoundingBoxD.CreateInvalid()));
            }
        }

        private static void OnSoundMessage(byte[] data)
        {
            //todo
        }

        private static void OnNotificaion(byte[] data)
        {
            var notificaion = MyAPIGateway.Utilities.SerializeFromBinary<Notification>(data);

            MyAPIGateway.Utilities.ShowNotification(notificaion.Message, notificaion.TimeoutMs, notificaion.Font);
        }

        /// <summary>
        /// Init player session, (key gen in future or secured request)
        /// </summary>
        /// <param name="data"></param>
        private static void OnClientReqSettings(byte[] data)
        {
            var reqsettings = MyAPIGateway.Utilities.SerializeFromBinary<ClientSettings>(data);
            if (reqsettings.SteamId != 0)
            {
                //TODO send key here
                byte[] message = MyAPIGateway.Utilities.SerializeToBinary(new ClientSettings
                {
                    SteamId = reqsettings.SteamId
                });
                SendToClient(MessageType.ClientSettings, message, reqsettings.SteamId);
            }
        }

        private static void OnClientReqSetBlockStatus(byte[] data)
        {
            MainLogic.OnClientReqSetBlockStatus(data);
        }
        private static void OnClientReqGetBlockStatus(byte[] data)
        {
            MainLogic.OnClientReqGetBlockStatus(data);
        }
        #endregion
        #region Send

        /// <summary>
        /// Send block status to one player with personal cooldowns
        /// </summary>
        /// <param name="steamid"></param>
        /// <param name="block_entid"></param>
        /// <param name="state"></param>
        /// <param name="new_cooldown"></param>
        public static void SendBlockStatusToPlayer(ulong steamid, long block_entid, BlockState state, Dictionary<string, DateTime> new_cooldown, bool keepbuider, string myinfo)
        {
            //SGarage.Instance.SomeLog($"Sending block status to " + steamid);
            var status = new BlockStatus
            {
                BlockEntId = block_entid,
                Cooldowns = new_cooldown,
                State = state,
                SteamId = steamid,
                ShareBuilder = keepbuider,
                InvAccesGet = 1,
                InvAccesPut = 1,
                SavedShipInfo = myinfo
            };
            byte[] data = MyAPIGateway.Utilities.SerializeToBinary(status);
            SendToClient(MessageType.SetBlockStatus, data, steamid);
        }

        /// <summary>
        /// Send new block status to all players, without cooldowns
        /// </summary>
        /// <param name="block_entid"></param>
        /// <param name="state"></param>
        public static void SendBlockStatusToAll(long block_entid, BlockState state, bool keepbuiler, string myinfo)
        {
            // SGarage.Instance.SomeLog($"Sending block status");
            var cooldown = new BlockStatus
            {
                BlockEntId = block_entid,
                State = state,
                SteamId = 0,
                ShareBuilder = keepbuiler,
                SavedShipInfo = myinfo
            };
            byte[] data = MyAPIGateway.Utilities.SerializeToBinary(cooldown);
            BroadcastToClients(MessageType.SetBlockStatus, data);
        }

        public static void SendNotification(ulong steamId, string message, string font = MyFontEnum.White, int timeoutMs = 2000)
        {
            var notification = new Notification
            {
                Message = message,
                Font = font,
                TimeoutMs = timeoutMs
            };
            byte[] data = MyAPIGateway.Utilities.SerializeToBinary(notification);
            if (steamId != 0)
                SendToClient(MessageType.Notificaion, data, steamId);
            else
                BroadcastToClients(MessageType.Notificaion, data);
        }
        #endregion

        #region Helpers
        public static void BroadcastToClients(MessageType type, byte[] data)
        {
            var newData = new byte[data.Length + 1];
            newData[0] = (byte)type;
            data.CopyTo(newData, 1);
            SGarage.Instance.SomeLog($"Sending message to others: {type}");
            MyAPIGateway.Utilities.InvokeOnGameThread(() => { MyAPIGateway.Multiplayer.SendMessageToOthers(NETWORK_ID, newData); });
        }

        public static void SendToClient(MessageType type, byte[] data, ulong recipient)
        {
            var newData = new byte[data.Length + 1];
            newData[0] = (byte)type;
            data.CopyTo(newData, 1);
            SGarage.Instance.SomeLog($"Sending message to {recipient}: {type}");
            MyAPIGateway.Utilities.InvokeOnGameThread(() => { MyAPIGateway.Multiplayer.SendMessageTo(NETWORK_ID, newData, recipient); });
        }
        #endregion
    }
}
