﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.GameSystems;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.Utils;
using VRageMath;

namespace Foogs
{
    public static class MainLogic
    {
        private const int PROJECTION_TTL = 1;
        private static Guid STORAGE_GUID = new Guid("49d74f80-540e-4e6e-9e16-3928719b5f0d");
        private static Guid SETTINGS_GUID = new Guid("308830f6-0ebe-41cb-b729-27a4fb851c7e");
        private static string[] garage_subtypes = { "LargeGarage" };
        private static MyStringHash[] garage_subtypes_h = { MyStringHash.GetOrCompute("LargeGarage") };
        private static Thread m_queue = null;
        private static Dictionary<long, Dictionary<string, DateTime>> m_all_cooldowns;
        private static Dictionary<long, DateTime> m_projections_times = new Dictionary<long, DateTime>();//Должно быть в lock (m_projections_times) { }
        private static System.Timers.Timer m_timerCheck = new System.Timers.Timer(5000);
        private static MethodInfo SendNewBlueprint;

        public static Queue<MsgInfo> ActionQueue = new Queue<MsgInfo>();

        public static void Init()
        {
            try
            {
                SendNewBlueprint = typeof(MyProjectorBase).GetMethod("SendNewBlueprint", BindingFlags.Instance | BindingFlags.NonPublic);
            }
            catch (Exception e)
            {
                SGarage.Log.Warn(e, "Reflection error! MyProjectorBase.SendNewBlueprint not found");
                return;
            }

            if (!string.IsNullOrEmpty(SGarage.Instance.Config.GaragePath))
            {
                if (!Directory.Exists(SGarage.Instance.Config.GaragePath))
                {
                    SGarage.Log.Warn($"Garage backup directory created!");
                    Directory.CreateDirectory(SGarage.Instance.Config.GaragePath);
                }
            }
            else
            {
                SGarage.Log.Warn($"Garage path is empty! Fatal error!");
                return;
            }

            var def = MyDefinitionManager.Static.GetEntityComponentDefinitions<VRage.Game.Definitions.MyModStorageComponentDefinition>(); //Выдели в отдельную функцию
            var guids = def.FirstOrDefault().RegisteredStorageGuids.ToList();
            guids.Add(STORAGE_GUID);
            guids.Add(SETTINGS_GUID);
            def.FirstOrDefault().RegisteredStorageGuids = guids.ToArray();

            m_queue = new Thread(ProcessQueue);
            m_queue.Priority = ThreadPriority.Lowest;
            m_queue.Start();

            Communication.RegisterHandlers();
            InterModComms.RegisterHandlers();

            m_timerCheck.Elapsed += (a, b) =>
            {
                TryCheckProjectionsTimers();
            };
            m_timerCheck.Start();

            if (MyAPIGateway.Utilities.GetVariable("SlimGarage", out string value))
            {
                m_all_cooldowns = MyAPIGateway.Utilities.SerializeFromBinary<Dictionary<long, Dictionary<string, DateTime>>>(Convert.FromBase64String(value));
            }
            else
            {
                m_all_cooldowns = new Dictionary<long, Dictionary<string, DateTime>>();
            }

            SaveCooldowns();
            InterModComms.SendFuncToMod();
        }

        public static void Dispose()
        {
            Communication.UnregisterHandlers();
            InterModComms.UnregisterHandlers();
            m_queue.Abort();
            m_queue = null;
        }

        //spread our invoke queue over many updates to avoid lag spikes and collisions!
        private static void ProcessQueue()
        {
            Thread.Sleep(2000);
            SGarage.Log.Warn($"Garage process queue start");
            while (true)
            {
                Thread.Sleep(1000);
                try
                {
                    if (ActionQueue.Count == 0)
                    {
                        continue;
                    }

                    MsgInfo item;
                    if (!ActionQueue.TryDequeue(out item))
                    {
                        continue;
                    }

                    switch (item.Action)
                    {
                        case ReqType.Save:
                            SaveGrid(ref item);
                            break;
                        case ReqType.Load:
                            LoadGrid(ref item);
                            break;
                        case ReqType.Clear:
                            GrindGrid(ref item);
                            break;
                        case ReqType.Show:
                            ShowGrid(ref item);
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    SGarage.Log.Warn($"Garage main loop exception catch: " + ex.ToString());
                }
            }
        }

        #region player actions
        /// <summary>
        /// Show projection (load into projector block)
        /// </summary>
        /// <param name="item"></param>
        private static void ShowGrid(ref MsgInfo info)
        {
            var steamid = info.SteamId;
            SGarage.Instance.SendMsgToChat("Show garage started.", steamid);
            var player_identity = Sync.Players.TryGetIdentityId(steamid);

            if (player_identity == 0)
            {
                SGarage.Instance.SendMsgToChat($"Player not found: {steamid}", steamid);
            }

            if (info.Projector == null)
            {
                SGarage.Instance.SendMsgToChat("Garage is null.", steamid);
                return;
            };

            MyObjectBuilder_Definitions myObjectBuilder_Definitions = Sandbox.Game.GUI.MyBlueprintUtils.LoadPrefab(GetStoragePath(info.Projector as MyProjectorBase));

            if (myObjectBuilder_Definitions == null)
            {
                SGarage.Instance.SendMsgToChat("Cant read ship from garage", info.SteamId);
                return;
            }
            var gridss = myObjectBuilder_Definitions.ShipBlueprints[0].CubeGrids.ToList();
            var settings = GetBlockSettingsStorage(info.Projector);

            info.Projector.SetProjectedGrid(gridss[0]);
            Thread.Sleep(20); // Делвем паузу между сообщениями. Чтобы сообщение отправилось первым.
            var TargetProjector = info.Projector as MyProjectorBase;
            SendNewBlueprint.Invoke(TargetProjector, new object[] { gridss });
            lock (m_projections_times)
            {
                m_projections_times[info.Projector.EntityId] = DateTime.Now.ToUniversalTime().AddMinutes(PROJECTION_TTL);
            }
            Thread.Sleep(33); //чтобы успела появиться проэкция.
            info.Projector.ProjectionRotation = settings.RotOffset;
            info.Projector.ProjectionOffset = settings.PosOffset;
            TargetProjector.SendNewOffset(settings.PosOffset, settings.RotOffset, 1, false);
            SGarage.Instance.SendMsgToChat("Show garage end.", steamid);
        }

        /// <summary>
        /// Clear all data from block and push items to cargo's in grid, or print error
        /// </summary>
        /// <param name="info"></param>
        private static void GrindGrid(ref MsgInfo info)
        {
            // SGarage.Instance.SendMsgToChat("Grind garage started.", info.SteamId);
            var player_identity = Sync.Players.TryGetIdentityId(info.SteamId);
            BlockState newstate = BlockState.Busy;
            if (GetBlockGridInfoStorage(info.Projector, out ModStorageGrid gridstorage))
            {
                if (CanUseGarage(gridstorage, ref info))
                {
                    List<MyInventory> cargosneighbours;
                    List<MyInventory> accessible_inv;

                    InventoryUtils.GetNeighboursAndTargetCargos(info.Projector, out cargosneighbours, out accessible_inv);
                    if (InventoryUtils.CanPutToCargos(info.Projector, cargosneighbours, accessible_inv))
                    {
                        InventoryUtils.TransferToCargos(info.Projector, cargosneighbours, accessible_inv);
                        ClearGarage(ref info);
                        newstate = BlockState.Empty_ReadyForSave;
                        SGarage.Instance.SendMsgToChat("Inventory transfered.", info.SteamId);
                    }
                    else
                    {
                        SGarage.Instance.SendMsgToChat("Inventory not transfered. No available space.", info.SteamId);
                        newstate = BlockState.Contains_ReadyForLoad;
                    }
                }
                else
                {
                    SGarage.Instance.SendMsgToChat("You cant use this garage.", info.SteamId);
                    newstate = BlockState.Busy;
                }
            }
            else
            {
                SGarage.Instance.SendMsgToChat("This garage is broken, fixing now...", info.SteamId);
                ClearGarage(ref info);
                newstate = BlockState.Contains_ReadyForLoad;
            }
            GetCooldown(player_identity, out Dictionary<string, DateTime> cooldown);
            ModStorageSettings blocksetting = GetBlockSettingsStorage(info.Projector);
            Communication.SendBlockStatusToAll(info.Projector.EntityId, newstate, blocksetting.ShareBuilder, "");
            Communication.SendBlockStatusToPlayer(info.SteamId, info.Projector.EntityId, newstate, cooldown, blocksetting.ShareBuilder, "");
            //SGarage.Instance.SendMsgToChat("Grind garage end.", info.SteamId);
        }

        /// <summary>
        /// Just delete all data
        /// </summary>
        /// <param name="info"></param>
        private static void ClearGarage(ref MsgInfo info)
        {
            SGarage.Instance.SendMsgToChat("Clear garage started.", info.SteamId);
            info.Projector.SetProjectedGrid(null);
            info.Projector.Storage.Remove(info.Projector.Storage.Where(x => x.Key == MainLogic.STORAGE_GUID).FirstOrDefault());
            info.Projector.GetInventory().Clear();
            // SGarage.Instance.SendMsgToChat("Clear garage end.", info.SteamId);
        }

        private static void LoadGrid(ref MsgInfo info)
        {
            try
            {
                var target_projector = (info.Projector as MyProjectorBase);
                var steamid = info.SteamId;
                SGarage.Instance.SendMsgToChat("Load from garage started.", steamid);
                var player_identity = Sync.Players.TryGetIdentityId(steamid);
                var player = Sync.Players.TryGetIdentity(player_identity);

                if (player == null)
                {
                    SGarage.Instance.SendMsgToChat("Cant find you. Contact admin.", steamid);
                    SGarage.Log.Warn("(LoadGrid) player == null !, steamid = {steamid}");
                    SendCurrentStatus(target_projector, steamid);
                    return;
                }

                if (HasCooldown("SlimGarage.LoadButton", player_identity))
                {
                    SGarage.Instance.SendMsgToChat("You have a cooldown.Aborted.", info.SteamId);
                    SendCurrentStatus(target_projector, steamid);
                    return;
                }

                if (info.Projector.GetInventory().ItemCount == 0)
                {
                    ClearGarage(ref info);
                    SGarage.Instance.SendMsgToChat("Incorrect state cleared.", info.SteamId);
                    SendCurrentStatus(target_projector, steamid);
                    return;
                }

                var grav = MyGravityProviderSystem.CalculateNaturalGravityInPoint(info.pos.Translation, out float gravmult);
                if (gravmult <= 0.99 && gravmult >= 0.05)
                {
                    SGarage.Instance.SendMsgToChat("Cant save in that gravity, operation aborted!", info.SteamId);
                    SendCurrentStatus(target_projector, steamid);
                    return;
                }

                var userpath = GetStoragePath(target_projector);
                MyObjectBuilder_Definitions myObjectBuilder_Definitions = Sandbox.Game.GUI.MyBlueprintUtils.LoadPrefab(userpath);
                if (myObjectBuilder_Definitions == null)
                {
                    SGarage.Instance.SendMsgToChat("Cant get ship from garage, contact admin.", info.SteamId);
                    SendCurrentStatus(target_projector, steamid);
                    return;
                }
                MyObjectBuilder_CubeGrid[] cubeGrids = myObjectBuilder_Definitions.ShipBlueprints[0].CubeGrids;
                var blocksetting = GetBlockSettingsStorage(target_projector);
                SpawnUtils.RemapOwnership(cubeGrids, player.IdentityId, blocksetting.ShareBuilder);
                SpawnUtils.SpawnSomeGrids(cubeGrids, ref info, player_identity, userpath);
            }
            catch (Exception ex)
            {
                SGarage.Instance.SendMsgToChat("Exception while load grid, contact admin.", info.SteamId);
                SGarage.Log.Warn(ex, $"Exception while load grid send logs too Foogs!");
                SendCurrentStatus(info.Projector, info.SteamId);
            }
        }

        private static void SaveGrid(ref MsgInfo info)
        {
            //TODO maybe grid lock but later
            ulong steamid = info.SteamId;
            var player_identity = Sync.Players.TryGetIdentityId(steamid);
            var player = Sync.Players.TryGetIdentity(player_identity);
            SGarage.Instance.SendMsgToChat("Ship save in progress", steamid);
            var grid = (MyCubeGrid)info.MyGrid;
            var target_projector = (info.Projector as MyProjectorBase);

            if (grid == null || grid.MarkedForClose || grid.Closed)
            {
                SGarage.Instance.SendMsgToChat("Ship is null.", steamid);
                SendCurrentStatus(target_projector, steamid);
                return;
            }

            if (target_projector == null)
            {
                SGarage.Instance.SendMsgToChat("Garage is null.", steamid);
                SendCurrentStatus(target_projector, steamid);
                return;
            }

            if (target_projector.CubeGrid.Physics.LinearVelocity.Length() > 1 | target_projector.CubeGrid.Physics.AngularVelocity.Length() > 2)
            {
                SGarage.Instance.SendMsgToChat("Stop your ship before use garage!.", steamid);
                SendCurrentStatus(target_projector, steamid);
                return;
            }

            /*if (HasCooldown("SlimGarage.SaveButton", player_identity))
            {
            SGarage.Instance.SendMsgToChat("You have cooldown.Aborted.", steamid);
            SendCurrentStatus(target_projector, steamid);
            return;

            }
            SetCooldown("SlimGarage.SaveButton", player_identity, 1);
            */

            var grids = MyCubeGridGroups.Static.GetGroups(GridLinkTypeEnum.Logical).GetGroupNodes(grid);
            grids.SortNoAlloc((x, y) => x.BlocksCount.CompareTo(y.BlocksCount));
            grids.Reverse();
            grids.SortNoAlloc((x, y) => x.GridSizeEnum.CompareTo(y.GridSizeEnum));

            if (!CheckCanSaveShip(grids, target_projector, steamid, player))
            {
                SendCurrentStatus(target_projector, steamid);
                return;
            }

            int index = 0;
            int TotalPCU = 0;
            int TotalBlocks = 0;
            var gridstosave = new MyObjectBuilder_CubeGrid[grids.Count];

            foreach (MyCubeGrid x in grids)
            {
                TotalPCU += x.BlocksPCU;
                TotalBlocks += x.BlocksCount;
                var ob = (MyObjectBuilder_CubeGrid)x.GetObjectBuilder(true);
                gridstosave[index] = ob;
                index++;
            }

            if (SpawnUtils.SaveShipsToFile(steamid, TotalPCU, TotalBlocks, ref gridstosave, out string filename, out MyObjectBuilder_Definitions obs))
            {
                MyAPIGateway.Utilities.InvokeOnGameThread(() =>
                {
                    foreach (MyCubeGrid g in grids)
                    {
                        g.Close();
                    }
                });

                if (target_projector.Storage == null)
                {
                    target_projector.Storage = new MyModStorageComponent();
                    target_projector.Components.Add<MyModStorageComponentBase>(target_projector.Storage);
                }
                // Dissasemble grid to components and put to inv.
                var grid_components = ReassemblyUtils.GetAllComponentsAndInventories(ref gridstosave);
                foreach (var item in grid_components)
                {
                    InventoryUtils.InventoryAdd(target_projector.GetInventory(), item.Value, item.Key, false);
                }
                // Save to ship info to storage + strong base64 protection
                var data = new ModStorageGrid(steamid, target_projector.EntityId, filename);
                target_projector.SetStorageData(STORAGE_GUID, Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(data)));

                //Create/save block settings to storage
                var block_settings = GetBlockSettingsStorage(target_projector);
                target_projector.SetStorageData(SETTINGS_GUID, Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(block_settings)));


                var shipinfo = SpawnUtils.GetStoredGridInfo(ref gridstosave);
                Communication.SendBlockStatusToAll(target_projector.EntityId, BlockState.Contains_ReadyForLoad, block_settings.ShareBuilder, shipinfo);
                Communication.SendBlockStatusToPlayer(info.SteamId, target_projector.EntityId, BlockState.Contains_ReadyForLoad, m_all_cooldowns[player_identity], block_settings.ShareBuilder, shipinfo);

                SaveCooldowns();
                SGarage.Instance.SendMsgToChat("Grid saved to the garage!", steamid);
            }
            else
            {
                SGarage.Instance.SendMsgToChat("Error during save,contact admin!", steamid);
            }
        }
        #endregion

        #region helpers
        public static bool CanUseGarage(ModStorageGrid block_savedgridinfo, ref MsgInfo info)
        {
            if (block_savedgridinfo.Original_blockid == info.Projector.EntityId && (info.Projector as MyTerminalBlock).HasPlayerAccess(info.Projector.OwnerId))
            {
                return true;
            }
            return false;
        }

        public static void AfterSpawn(ref MsgInfo info)
        {
            ulong steamid = info.SteamId;
            var player_identity = Sync.Players.TryGetIdentityId(steamid);
            ClearGarage(ref info);
            var proj = info.Projector;
            MainLogic.SetCooldown("SlimGarage.LoadButton", player_identity, SGarage.Instance.Config.LoadCooldown);
            var blocksetting = GetBlockSettingsStorage(proj);
            Communication.SendBlockStatusToAll(proj.EntityId, BlockState.Empty_ReadyForSave, blocksetting.ShareBuilder, "");
            Communication.SendBlockStatusToPlayer(info.SteamId, proj.EntityId, BlockState.Empty_ReadyForSave, m_all_cooldowns[player_identity], blocksetting.ShareBuilder, "");
            SaveCooldowns();
            SGarage.Instance.SendMsgToChat("Load ended.", info.SteamId);
        }

        private static string GetStoragePath(MyProjectorBase target_projector)
        {
            var data = target_projector.GetStorageData(STORAGE_GUID);
            if (data == null)
            {
                //TODO: add logging
                return null;
            }

            var storage = MyAPIGateway.Utilities.SerializeFromBinary<ModStorageGrid>(Convert.FromBase64String(data));
            return storage.GetStoragePath();
        }

        public static void SendCurrentStatus(IMyProjector p, ulong steamid)
        {
            SGarage.Instance.SomeLog($"Player requested.Sending back block status to " + steamid);
            var player_identity = Sandbox.Game.Multiplayer.Sync.Players.TryGetIdentityId(steamid);
            var state = MainLogic.GetBlockState(p);
            var settings = MainLogic.GetBlockSettingsStorage(p);
            MainLogic.GetCooldown(player_identity, out Dictionary<string, DateTime> cooldown);
            var gridsInfo = GetShipInfo(p);
            Communication.SendBlockStatusToPlayer(steamid, p.EntityId, state, cooldown, settings.ShareBuilder, gridsInfo);
        }

        private static bool HasCooldown(string name, long player_id)
        {
            var curr_time = DateTime.Now.ToUniversalTime();
            if (m_all_cooldowns.ContainsKey(player_id))
            {
                if (m_all_cooldowns[player_id].ContainsKey(name))
                {
                    if (m_all_cooldowns[player_id][name] > curr_time)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private static bool CheckCanSaveShip(List<MyCubeGrid> grids, IMyProjector target_projector, ulong steamid, MyIdentity player)
        {
            foreach (MyCubeGrid g in grids)
            {
                if (g.IsPreview)
                {
                    var translated_msg = "Try save projection is bad, u are poor guy. Disable it pls.";
                    SGarage.Instance.SendMsgToChat(translated_msg, steamid);
                    SGarage.Log.Warn($"Player trying to silent cheat (save a projection), operation aborted! Player { player.DisplayName} [{ steamid}]");
                    SendCurrentStatus(target_projector, steamid);
                    return false;
                }

                var blocks = g.GetFatBlocks();
                foreach (var b in blocks)
                {
                    if (garage_subtypes_h.Contains(b.BlockDefinition.Id.SubtypeId) && b.EntityId == target_projector.EntityId)
                    {
                        SGarage.Instance.SendMsgToChat("Cant save garage into garage, result is blackhole. Operation aborted.", steamid);
                        SGarage.Log.Warn($"Cant save garage into garage, result is blackhole, operation aborted! " +
                        $"Player: " + player.DisplayName + '(' + steamid + ')');
                        return false;
                    }

                    if (b is MyCockpit)
                    {
                        if ((b as MyCockpit).Pilot != null)
                        {
                            SGarage.Instance.SendMsgToChat("Grid have pilot in: [ " + b.DisplayNameText + " ], operation aborted!", steamid);
                            SGarage.Log.Warn($"Grid have pilot in : [ " + b.DisplayNameText + " ], operation aborted! " +
                            $"Player: " + player.DisplayName + '(' + steamid + ')');
                            return false;
                        }
                    }
                }
            }

            if (!InterModComms.Garage_Mod_SaveChecks.Invoke(grids.ToArray<IMyCubeGrid>(), player.IdentityId))
            {
                SGarage.Instance.SendMsgToChat("Grid have enemy spec block, operation aborted!", steamid);
                SGarage.Log.Warn($"Grid have enemy spec block, operation aborted! " +
                $"Player: " + player.DisplayName + '(' + steamid + ')');
                return false;
            }

            return true;
        }

        private static bool GetCooldown(long player_id, out Dictionary<string, DateTime> cooldown)
        {
            if (m_all_cooldowns.ContainsKey(player_id))
            {
                cooldown = m_all_cooldowns[player_id];
                return true;
            }

            cooldown = new Dictionary<string, DateTime>();
            return false;
        }

        private static void SetCooldown(string name, long player_id, int minutes)
        {
            var curr_time = DateTime.Now.ToUniversalTime();
            var cooldown_time = curr_time.AddMinutes(minutes);
            var new_client_cooldown = new Dictionary<string, DateTime>() { { name, cooldown_time } };
            if (m_all_cooldowns.ContainsKey(player_id))
            {
                if (m_all_cooldowns[player_id].ContainsKey(name))
                {
                    m_all_cooldowns[player_id][name] = cooldown_time;
                }
                else
                {
                    m_all_cooldowns[player_id].Add(name, cooldown_time);
                }
            }
            else
            {
                m_all_cooldowns.Add(player_id, new_client_cooldown);
            }
        }
        /// <summary>
        /// LOAD BP FROM DISK! Blocking!
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private static string GetShipInfo(IMyProjector p)
        {
            try
            {
                if (p != null)
                {
                    var rawstoragedata = p.GetStorageData(STORAGE_GUID);
                    if (rawstoragedata != null)
                    {
                        var storage = MyAPIGateway.Utilities.SerializeFromBinary<ModStorageGrid>(Convert.FromBase64String(rawstoragedata));
                        if (!String.IsNullOrWhiteSpace(storage.Path))
                        {
                            string userpath = GetStoragePath(p as MyProjectorBase);

                            if (File.Exists(userpath))
                            {
                                MyObjectBuilder_Definitions myObjectBuilder_Definitions = Sandbox.Game.GUI.MyBlueprintUtils.LoadPrefab(userpath);
                                return SpawnUtils.GetStoredGridInfo(ref myObjectBuilder_Definitions.ShipBlueprints[0].CubeGrids);
                            }
                            else
                            {
                                SGarage.Log.Warn($"[GetShipInfo] user path not defined.");
                            }
                        }
                        else
                        {
                            SGarage.Log.Warn($"[GetShipInfo] user path not exist.");
                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                SGarage.Log.Warn(ex, "GetShipInfo exception!");
                return "";
            }
        }

        private static void SaveCooldowns()
        {
            MyAPIGateway.Utilities.SetVariable("SlimGarage", Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(m_all_cooldowns)));
        }

        private static void SaveProjectionSettings(IMyProjector p)
        {
            var t = GetBlockSettingsStorage(p);
            SetBlockSettingsStorage(p, new ModStorageSettings(t.ShareBuilder, t.InvAccesPut, t.InvAccesGet, p.ProjectionOffset, p.ProjectionRotation), p.OwnerId);
        }
        #endregion

        #region block settings
        //Invokes from game thread! Don't block!!
        public static void OnClientReqSetBlockStatus(byte[] data)
        {
            var reqstatus = MyAPIGateway.Utilities.SerializeFromBinary<BlockStatus>(data);
            if (reqstatus.SteamId != 0)
            {
                SGarage.Instance.SomeLog($"Player requested.Updating block status. " + reqstatus.SteamId);
                var player_identity = Sandbox.Game.Multiplayer.Sync.Players.TryGetIdentityId(reqstatus.SteamId);
                IMyProjector p = MyAPIGateway.Entities.GetEntityById(reqstatus.BlockEntId) as IMyProjector;
                // MyAPIGateway.Parallel.StartBackground(() => //MAYBE NOT NEEDED????
                //{
                var newsettings = new ModStorageSettings(reqstatus.ShareBuilder, reqstatus.InvAccesPut, reqstatus.InvAccesPut, p.ProjectionOffset, p.ProjectionRotation);
                if (MainLogic.SetBlockSettingsStorage(p, newsettings, player_identity))
                {
                    SGarage.Instance.SomeLog($"Player requested.Updating block status.OK " + reqstatus.SteamId);
                    var stus = MainLogic.GetBlockState(p);
                    var gridinfo = MainLogic.GetShipInfo(p);
                    MainLogic.GetCooldown(player_identity, out Dictionary<string, DateTime> cooldown);
                    Communication.SendBlockStatusToPlayer(reqstatus.SteamId, reqstatus.BlockEntId, stus, cooldown, reqstatus.ShareBuilder, gridinfo);
                    Communication.SendBlockStatusToAll(reqstatus.BlockEntId, stus, reqstatus.ShareBuilder, gridinfo);
                }
                // });
            }
        }

        //Invokes from game thread! Don't block!!
        public static void OnClientReqGetBlockStatus(byte[] data)
        {
            var reqstatus = MyAPIGateway.Utilities.SerializeFromBinary<BlockStatus>(data);
            if (reqstatus.SteamId != 0)
            {
                IMyProjector p = MyAPIGateway.Entities.GetEntityById(reqstatus.BlockEntId) as IMyProjector;
                // MyAPIGateway.Parallel.StartBackground(() =>
                //{
                SGarage.Instance.SomeLog($"Player requested.Sending back block status to " + reqstatus.SteamId);
                var stus = MainLogic.GetBlockState(p);
                var player_identity = Sandbox.Game.Multiplayer.Sync.Players.TryGetIdentityId(reqstatus.SteamId);
                MainLogic.GetCooldown(player_identity, out Dictionary<string, DateTime> cooldown);
                var settings = MainLogic.GetBlockSettingsStorage(p);
                var mmyinfo = MainLogic.GetShipInfo(p);
                Communication.SendBlockStatusToPlayer(reqstatus.SteamId, reqstatus.BlockEntId, stus, cooldown, settings.ShareBuilder, mmyinfo);
                // });
            }
        }

        private static BlockState GetBlockState(IMyProjector p)
        {
            try
            {
                if (p != null)
                {
                    if (GetBlockGridInfoStorage(p, out ModStorageGrid info))
                    {
                        var path = info.GetStoragePath();
                        if (path == null)
                        {
                            SGarage.Log.Warn($"[GetBlockState] userpath not exist.");
                            return BlockState.None;
                        }

                        if (!File.Exists(path))
                        {
                            SGarage.Log.Warn($"[GetBlockState] file {path} not exists.");
                            return BlockState.None;
                        }

                        return BlockState.Contains_ReadyForLoad;
                    }
                    else
                    {
                        return BlockState.Empty_ReadyForSave;
                    }
                }
                else
                {
                    SGarage.Log.Warn($"[GetBlockState] projector is null");
                    return BlockState.None;
                }
            }
            catch (Exception ex)
            {
                SGarage.Log.Warn($"GetBlockState exception = " + ex.ToString());
                return BlockState.Empty_ReadyForSave;
            }
        }

        private static bool SetBlockSettingsStorage(IMyProjector p, ModStorageSettings newstatus, long player_id)
        {
            if (p != null)
            {
                var TargetProjector = p as MyProjectorBase;
                if (TargetProjector.BuiltBy == player_id)
                {
                    if (TargetProjector.Storage == null)
                    {
                        TargetProjector.Storage = new MyModStorageComponent();
                        TargetProjector.Components.Add<MyModStorageComponentBase>(TargetProjector.Storage);
                    }

                    TargetProjector.SetStorageData(SETTINGS_GUID, Convert.ToBase64String(MyAPIGateway.Utilities.SerializeToBinary(newstatus)));
                    return true;
                }
            }
            return false;
        }

        private static bool GetBlockGridInfoStorage(IMyProjector p, out ModStorageGrid info)
        {
            var rawstoragedata = p.GetStorageData(STORAGE_GUID);
            if (rawstoragedata != null)
            {
                info = MyAPIGateway.Utilities.SerializeFromBinary<ModStorageGrid>(Convert.FromBase64String(rawstoragedata));
                return true;
            }
            info = new ModStorageGrid();
            return false;
        }

        /// <summary>
        /// Получить настройки блока. Если их нет вернуть дефолтные.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private static ModStorageSettings GetBlockSettingsStorage(IMyProjector p)
        {
            try
            {
                if (p != null)
                {
                    var rawstoragedata = p.GetStorageData(SETTINGS_GUID);
                    if (rawstoragedata != null)
                    {
                        var storage = MyAPIGateway.Utilities.SerializeFromBinary<ModStorageSettings>(Convert.FromBase64String(rawstoragedata));
                        return storage;
                    }
                }
                // SGarage.Log.Warn($"GetSettings failed return default.");
                return new ModStorageSettings(false, 0, 0, Vector3I.Zero, Vector3I.Zero);
            }
            catch (Exception ex)
            {
                SGarage.Log.Warn($"GetBlockSettingsStorage exception! {ex} OwnerId:[{p.OwnerId}]");
                return new ModStorageSettings(false, 0, 0, Vector3I.Zero, Vector3I.Zero);
            }
        }

        private static void TryCheckProjectionsTimers()
        {
            try
            {
                if (m_projections_times?.Count == 0) return;

                var currtime = DateTime.Now.ToUniversalTime();
                var tooff = new List<long>();

                foreach (var item in m_projections_times)
                {
                    if (item.Value < currtime)
                    {
                        tooff.Add(item.Key);
                    }
                }

                if (tooff.Any())
                {
                    MyAPIGateway.Utilities.InvokeOnGameThread(() =>
                    {
                        try
                        {
                            lock (m_projections_times)
                            {
                                foreach (var proj in tooff)
                                {
                                    var ent_p = MyAPIGateway.Entities.GetEntityById(proj);
                                    if (ent_p != null)
                                    {
                                        MyProjectorBase p = ent_p as MyProjectorBase;

                                        (p as IMyProjector).SetProjectedGrid(null);
                                        SaveProjectionSettings(p);
                                    }

                                    m_projections_times.Remove(proj);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SGarage.Log.Warn(ex, "TryCheckProjectionsTimers in-game catch!");
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                SGarage.Log.Warn(ex, "TryCheckProjectionsTimers catch!");
            }
        }
        #endregion
    }
}
