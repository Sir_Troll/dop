﻿using Torch;

namespace Foogs
{
    public class FoogsGarageConfig : ViewModel
    {
        private bool _enable;
        public bool Enabled { get => _enable; set => SetValue(ref _enable, value); }      
       
        private string _garagepath;
        public string GaragePath
        {
            get => _garagepath; set { SetValue(ref _garagepath, value); }
        }

        private int _loadcooldown;
        public int LoadCooldown
        {
            get => _loadcooldown; set { SetValue(ref _loadcooldown, value); }
        }
    }
}