﻿using System.IO;
using System.Threading;
using System.Windows.Controls;
using NLog;
using Sandbox.ModAPI;
using Torch;
using Torch.API;
using Torch.API.Managers;
using Torch.API.Plugins;
using VRageMath;

namespace Foogs
{
    public class SGarage : TorchPluginBase, IWpfPlugin
    {
        public static SGarage Instance { get; private set; }
        public static readonly Logger Log = LogManager.GetLogger("FoogsSlimGarage");
        private IChatManagerServer m_chatMngr;
        private UserControl m_control;
        private Persistent<FoogsGarageConfig> m_config;
        private bool m_init;
        public FoogsGarageConfig Config => m_config?.Data;
        public UserControl GetControl() => m_control ?? (m_control = new FoogsGarageControlForm() { DataContext = Config });

        public void Save()
        {
            m_config.Save();
        }

        /// <inheritdoc />
        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-US");
            string path = Path.Combine(StoragePath, "FoogsSlimGarage.cfg");
            Log.Warn($"Init FoogsSlimGarage plugin.");
            m_config = Persistent<FoogsGarageConfig>.Load(path);
            Instance = this;
        }

        private void Initialize()
        {
            Instance = this;
            m_init = true;
            m_control?.Dispatcher.Invoke(() =>
            {

                m_control.DataContext = Config;
            });

            if (m_config.Data.Enabled)
            {
                MainLogic.Init();
            }
            else
            {
                Log.Warn($"FoogsSlimGarage disabled");
            }
        }

        public override void Update()
        {
            if (MyAPIGateway.Session == null)
                return;

            if (!m_init)
            {
                Initialize();
            }
        }

        public void SomeLog(string msg)
        {
            Log.Warn(msg);
        }

        public void SendMsgToChat(string msg, ulong SteamId)
        {
            if (m_chatMngr == null)
            {
                m_chatMngr = SGarage.Instance.Torch.CurrentSession.Managers.GetManager<IChatManagerServer>();
            }
            if (m_chatMngr == null)
            {
                MyAPIGateway.Utilities.ShowMessage("Garage", msg);
                Log.Warn($"IChatManagerServer null!");
                return;
            }

            m_chatMngr.SendMessageAsOther("Garage", msg, Color.GreenYellow, SteamId);
        }

        public override void Dispose()
        {
            MainLogic.Dispose();
        }
    }
}
