﻿using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using VRage;
using VRage.Game;
using VRage.Game.ObjectBuilders.ComponentSystem;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.ObjectBuilders;

namespace Foogs
{
    public static class ReassemblyUtils
    {
        public static Dictionary<MyDefinitionId, MyFixedPoint> GetAllComponentsAndInventories(ref MyObjectBuilder_CubeGrid[] grids_objectbuilders)
        {
            if (grids_objectbuilders == null)
            {
                //result = "ob is null";
                return null;
            }

            int terminalBlocks = 0;
            int armorBlocks = 0;
            int gridCount = 0;

            var Grid_Components = new Dictionary<MyDefinitionId, MyFixedPoint>();
            var inventoryComponents = new Dictionary<MyDefinitionId, MyFixedPoint>();

            try
            {
                var grids = grids_objectbuilders;
                gridCount = grids.Count();
                foreach (var grid in grids)
                {
                    var blocks = new List<MyObjectBuilder_CubeBlock>();
                    blocks = grid.CubeBlocks;

                    foreach (var block in blocks)
                    {
                        MyCubeBlockDefinition blockDefintion;

                        if (block is MyObjectBuilder_TerminalBlock)
                        {
                            armorBlocks++;
                            blockDefintion = MyDefinitionManager.Static.GetCubeBlockDefinition(block);
                        }
                        else
                        {
                            terminalBlocks++;
                            blockDefintion = MyDefinitionManager.Static.GetCubeBlockDefinition(block);
                        }


                        #region Go through component List + exclude construction level.

                        foreach (var component in blockDefintion.Components)
                        {
                            if (!Grid_Components.ContainsKey(component.Definition.Id))
                            {
                                Grid_Components.Add(component.Definition.Id, 0);
                            }
                            var a = Grid_Components[component.Definition.Id];
                            var b = component.Count;
                            var c = a + b;
                            Grid_Components[component.Definition.Id] = c;

                        }

                        // This will subtract off components missing from a partially built cube.
                        // This also includes the Construction Inventory.

                        var missingComponents = GetMissingComponents(block, blockDefintion);
                        //block.GetMissingComponents(missingComponents);
                        foreach (var kvp in missingComponents)
                        {
                            var definitionid = new MyDefinitionId(typeof(MyObjectBuilder_Component), kvp.Key);
                            Grid_Components[definitionid] -= kvp.Value;
                        }

                        #endregion


                        if (block is MyObjectBuilder_TerminalBlock)
                        {
                            //GAS TANK DETECTED
                            var tank = block as Sandbox.Common.ObjectBuilders.MyObjectBuilder_OxygenTank;
                            var gasTankDefintion = blockDefintion as MyGasTankDefinition;
                            MyDefinitionId definitionid;

                            if (gasTankDefintion != null && tank != null)
                            {
                                float volumeinlitres = gasTankDefintion.Capacity * tank.FilledRatio;
                                MyObjectBuilder_Base someobject = MyObjectBuilderSerializer.CreateNewObject(gasTankDefintion.StoredGasId);
                                if (someobject is MyObjectBuilder_GasProperties)
                                {
                                    // SGarage.Log.Warn($"Gas tank detected! {tank.Name}");
                                    //SGarage.Log.Warn($"Capacity: [{gasTankDefintion.Capacity}] FilledRatio: [{tank.FilledRatio}] volumeinlitres: [{volumeinlitres}]");
                                    GasToOre((MyObjectBuilder_GasProperties)someobject, volumeinlitres, out definitionid, out MyFixedPoint kg);

                                    // SGarage.Log.Warn($"definitionid: [{definitionid.SubtypeName}] kg: [{kg}]");
                                    if (!inventoryComponents.ContainsKey(definitionid))
                                        inventoryComponents.Add(definitionid, 0);
                                    inventoryComponents[definitionid] += kg;
                                }
                            }

                            #region Go through all other Inventories for components/items.

                            // Inventory check

                            if (block.ComponentContainer != null)
                            {
                                MyObjectBuilder_ComponentContainer.ComponentData componentData = block.ComponentContainer.Components.Find((MyObjectBuilder_ComponentContainer.ComponentData s) => s.Component.TypeId == typeof(MyObjectBuilder_Inventory));
                                if (componentData != null)
                                {

                                    var inventory = (componentData.Component as MyObjectBuilder_Inventory).Items;
                                    if (inventory == null || inventory.Count == 0) continue;


                                    var list = inventory;
                                    foreach (var item in list)
                                    {

                                        var id = item.PhysicalContent.GetId();
                                        if (!inventoryComponents.ContainsKey(id))
                                            inventoryComponents.Add(id, 0);
                                        inventoryComponents[id] += item.Amount;

                                        // Go through Gas bottles.
                                        var gasContainer = item.PhysicalContent as MyObjectBuilder_GasContainerObject;
                                        if (gasContainer != null)
                                        {
                                            var defintion = (MyOxygenContainerDefinition)MyDefinitionManager.Static.GetPhysicalItemDefinition(item.PhysicalContent.GetId());

                                            float volumeinlitres = defintion.Capacity * gasContainer.GasLevel;

                                            MyObjectBuilder_Base someobject = MyObjectBuilderSerializer.CreateNewObject(defintion.StoredGasId);

                                            if (someobject is MyObjectBuilder_GasProperties)
                                            {
                                                GasToOre((MyObjectBuilder_GasProperties)someobject, volumeinlitres, out definitionid, out MyFixedPoint kg);
                                                if (!inventoryComponents.ContainsKey(definitionid))
                                                    inventoryComponents.Add(definitionid, 0);
                                                inventoryComponents[definitionid] += kg;
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex0)
            {
                SGarage.Instance.SomeLog(ex0.ToString());
            }
            // var newdict = Grid_Components.Concat(inventoryComponents).ToDictionary(x => x.Key, x => x.Value);

            foreach (var item in Grid_Components)
            {
                if (inventoryComponents.ContainsKey(item.Key))
                {
                    inventoryComponents[item.Key] += item.Value;
                }
                else
                {
                    inventoryComponents.Add(item.Key, item.Value);
                }
            }
            Grid_Components = null;
            return inventoryComponents;
        }

        public static readonly float ICE_LITER_TO_KG = 1;
        public static readonly float OIL_LITER_TO_KG = 0.05f;
        public static readonly float KERO_LITER_TO_KG = 1;
        private static void GasToOre(MyObjectBuilder_GasProperties gaspropertis, float litres, out MyDefinitionId definitionid, out MyFixedPoint kg)
        {
            float ratio;
            string name;

            if (gaspropertis.SubtypeName.Contains("Kerosene"))
            {
                ratio = KERO_LITER_TO_KG;
                name = "FrozenOil";
            }

            if (gaspropertis.SubtypeName.Contains("Oil"))
            {
                ratio = OIL_LITER_TO_KG;
                name = "FrozenOil";
            }
            else
            {
                ratio = ICE_LITER_TO_KG;
                name = "Ice";
            }
            MyObjectBuilder_Ore physicalContent = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_Ore>(name);
            definitionid = physicalContent.GetId();
            kg = (MyFixedPoint)ratio * litres;
        }

        private static Dictionary<string, int> GetMissingComponents(MyObjectBuilder_CubeBlock block, MyCubeBlockDefinition blockDefintion)
        {
            var result_Dictionary = new Dictionary<string, int>();
            var stack = new MyComponentStack(blockDefintion, block.IntegrityPercent, block.BuildPercent);
            stack.GetMissingComponents(result_Dictionary);
            return result_Dictionary;
        }
    }
}