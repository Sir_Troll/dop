﻿using System.Collections.Generic;
using System.IO;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using VRage.ModAPI;

namespace Foogs
{
    public static class SpawnCounter
    {
        public static void RenameFile(this System.IO.FileInfo fileInfo, string newFileName, FileExistBehavior fileExistBehavior = FileExistBehavior.None)
        {
            string newFileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(newFileName);
            string newFileNameExtension = System.IO.Path.GetExtension(newFileName);
            string newFilePath = System.IO.Path.Combine(fileInfo.Directory.FullName, newFileName);

            if (System.IO.File.Exists(newFilePath))
            {
                switch (fileExistBehavior)
                {
                    case FileExistBehavior.None:
                        throw new System.IO.IOException("The destination file already exists.");
                    case FileExistBehavior.Replace:
                        System.IO.File.Delete(newFilePath);
                        break;
                    case FileExistBehavior.Rename:
                        int dupplicate_count = 0;
                        string newFileNameWithDupplicateIndex;
                        string newFilePathWithDupplicateIndex;
                        do
                        {
                            dupplicate_count++;
                            newFileNameWithDupplicateIndex = newFileNameWithoutExtension + " (" + dupplicate_count + ")" + newFileNameExtension;
                            newFilePathWithDupplicateIndex = System.IO.Path.Combine(fileInfo.Directory.FullName, newFileNameWithDupplicateIndex);
                        } while (System.IO.File.Exists(newFilePathWithDupplicateIndex));
                        newFilePath = newFilePathWithDupplicateIndex;
                        break;
                    case FileExistBehavior.Skip:
                        return;
                }
            }
            System.IO.File.Move(fileInfo.FullName, newFilePath);
        }

        public class SpawnCallback
        {
            private int _counter;
            private List<IMyEntity> _entlist;
            private readonly int _maxCount;
            private readonly string _path;
            private readonly MsgInfo _info;

            public SpawnCallback(int count, string path, ref MsgInfo info)
            {
                _counter = 0;
                _entlist = new List<IMyEntity>();
                _maxCount = count;
                _path = path;
                _info = info;
            }

            public void Increment(IMyEntity ent)
            {
                _counter++;
                _entlist.Add(ent);

                if (_counter < _maxCount)
                {
                    return;
                }
                FinalSpawnCallback(_entlist, _path, _info);
            }

            private static void FinalSpawnCallback(List<IMyEntity> grids, string path, MsgInfo info)
            {
                SGarage.Log.Warn("FinalSpawnCallback!");
                MyAPIGateway.Parallel.StartBackground(() =>
                {
                    try
                    {
                        MainLogic.AfterSpawn(ref info);
        // new FileInfo(path).Rename(Path.GetFileName(path) + "_spawned", FileExistBehavior.Replace);
        File.Delete(path + VRage.ObjectBuilders.MyObjectBuilderSerializer.ProtobufferExtension);
                    }
                    catch
                    {
                        SGarage.Log.Warn("(FinalSpawnCallback) Error while AfterSpawn / rename file in garage. " + path);
                    }
                });               

                foreach (MyCubeGrid ent in grids)
                {
                    ent.DetectDisconnectsAfterFrame();
                    MyAPIGateway.Entities.AddEntity(ent, true);
                    // ent.DestructibleBlocks = false; //not used
                }
                return;
            }
        }
    }

    /// <summary>
    /// behavior when new filename is exist.
    /// </summary>
    public enum FileExistBehavior
    {
        /// <summary>
        /// None: throw IOException "The destination file already exists."
        /// </summary>
        None = 0,
        /// <summary>
        /// Replace: replace the file in the destination.
        /// </summary>
        Replace = 1,
        /// <summary>
        /// Skip: skip this file.
        /// </summary>
        Skip = 2,
        /// <summary>
        /// Rename: rename the file. (like a window behavior)
        /// </summary>
        Rename = 3
    }
}
