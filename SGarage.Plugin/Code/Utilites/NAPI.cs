﻿using System;
using System.Globalization;
using System.Text;
using Sandbox.Game.EntityComponents;
using VRage.Game.Components;
using VRage.ModAPI;

namespace Foogs
{
    public static class Serialization
    {
        public static StringBuilder Serialize(this StringBuilder sb, float f) { sb.Append(f.ToString(CultureInfo.InvariantCulture)); return sb; }
        public static StringBuilder Serialize(this StringBuilder sb, double f) { sb.Append(f.ToString(CultureInfo.InvariantCulture)); return sb; }
        public static StringBuilder Serialize(this StringBuilder sb, int f) { sb.Append(f.ToString(CultureInfo.InvariantCulture)); return sb; }
        public static StringBuilder Serialize(this StringBuilder sb, long f) { sb.Append(f.ToString(CultureInfo.InvariantCulture)); return sb; }

        public static MyModStorageComponentBase GetOrCreateStorage(this IMyEntity entity) { return entity.Storage = entity.Storage ?? new MyModStorageComponent(); }
        public static bool HasStorage(this IMyEntity entity) { return entity.Storage != null; }

        public static string GetStorageData(this IMyEntity entity, Guid guid)
        {
            if (entity.Storage == null) return null;
            string data = null;
            if (entity.Storage.TryGetValue(guid, out data))
            {
                return data;
            }
            else
            {
                return null;
            }
        }

        public static string GetAndSetStorageData(this IMyEntity entity, Guid guid, string newData)
        {
            var data = GetStorageData(entity, guid);
            SetStorageData(entity, guid, newData);
            return data;
        }

        public static void SetStorageData(this IMyEntity entity, Guid guid, String data)
        {
            if (entity.Storage == null && data == null)
            {
                return;
            }
            entity.GetOrCreateStorage().SetValue(guid, data);
        }
    }
}
