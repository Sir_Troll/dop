﻿using System.Collections.Generic;
using Sandbox.Definitions;
using Sandbox.Game.World;
using VRage.Game;

namespace Foogs
{
    public static class Limits
    {
        public static int CheckLimits_PCUCount(MyObjectBuilder_CubeGrid[] gruop)
        {
            int pcusOfGroup = 0;
            long blockCountOfGroup = 0L;

            foreach (var grid in gruop)
            {
                var blocks = grid.CubeBlocks;
                foreach (var block in blocks)
                {
                    MyDefinitionId defId = new MyDefinitionId(block.TypeId, block.SubtypeId);
                    MyCubeBlockDefinition myCubeBlockDefinition;
                    if (MyDefinitionManager.Static.TryGetCubeBlockDefinition(defId, out myCubeBlockDefinition))
                    {
                        string blockPairName = myCubeBlockDefinition.BlockPairName;
                        pcusOfGroup += myCubeBlockDefinition.PCU;
                        blockCountOfGroup++;
                    }
                }
            }
            return pcusOfGroup;
        }

        public static class VanilaLimitsAPI
        {
            public static bool CheckLimits_future(MyObjectBuilder_CubeGrid[] grids)
            {
                int FinalBlocksCount = 0;
                int FinalBlocksPCU = 0;
                bool flag = true;
                Dictionary<long, Dictionary<string, int>> BlocksAndOwnerForLimits = new Dictionary<long, Dictionary<string, int>>();
                foreach (var myCubeGrid in grids)
                {
                    foreach (var slimblock in myCubeGrid.CubeBlocks)
                    {
                        MyDefinitionId defId = new MyDefinitionId(slimblock.TypeId, slimblock.SubtypeId);
                        MyCubeBlockDefinition myCubeBlockDefinition;
                        if (MyDefinitionManager.Static.TryGetCubeBlockDefinition(defId, out myCubeBlockDefinition))
                        {
                            string blockPairName = myCubeBlockDefinition.BlockPairName;
                            long blockowner = 0;
                            blockowner = slimblock.BuiltBy;
                            // лимиты считаються по билдеру
                            if (BlocksAndOwnerForLimits.ContainsKey(blockowner))
                            {
                                var dictforuser = BlocksAndOwnerForLimits[blockowner];
                                if (dictforuser.ContainsKey(blockPairName))
                                {
                                    Dictionary<string, int> blocksPerType;
                                    string key;
                                    (blocksPerType = dictforuser)[key = blockPairName] = blocksPerType[key] + 1;
                                }
                                else
                                {
                                    dictforuser.Add(blockPairName, 1);
                                }
                                BlocksAndOwnerForLimits[blockowner] = dictforuser;
                            }
                            else
                            {
                                BlocksAndOwnerForLimits.Add(blockowner, new Dictionary<string, int>
{{ blockPairName, 1 }});
                            }
                            FinalBlocksPCU += myCubeBlockDefinition.PCU; //HACK
                        }
                    }
                    if (MySession.Static.MaxGridSize != 0)
                    {
                        flag &= (myCubeGrid.CubeBlocks.Count <= MySession.Static.MaxGridSize);
                    }
                    FinalBlocksCount += myCubeGrid.CubeBlocks.Count;
                }
                bool result = true;
                string text;
                MySession.LimitResult limresult;
                foreach (var player in BlocksAndOwnerForLimits)
                {
                    result &= CheckLimitsInternal(out text, out limresult, true, player.Key, null, FinalBlocksPCU, FinalBlocksCount, flag ? 0 : (MySession.Static.MaxGridSize + 1), player.Value);
                }
                return result;
            }

            private static bool CheckLimitsInternal(out string text, out MySession.LimitResult limitResult, bool future_spawn, long ownerID, string blockName, int pcuToBuild, int blocksToBuild = 0, int blocksCount = 0, Dictionary<string, int> blocksPerType = null)
            {

                limitResult = IsFitsWorldLimitsInternal(out text, future_spawn, ownerID, blockName, pcuToBuild, blocksToBuild, blocksCount, blocksPerType);
                if (limitResult != MySession.LimitResult.Passed)
                {
                    return false;
                }
                return true;
            }

            private static MySession.LimitResult IsFitsWorldLimitsInternal(out string failedBlockType, bool future_spawn, long ownerID, string blockName, int pcuToBuild, int blocksToBuild = 0, int blocksCount = 0, Dictionary<string, int> blocksPerType_forcheck = null)
            {
                failedBlockType = null;
                if (MySession.Static.BlockLimitsEnabled == MyBlockLimitsEnabledEnum.NONE)
                {
                    return MySession.LimitResult.Passed;
                }
                MyIdentity myIdentity = MySession.Static.Players.TryGetIdentity(ownerID);
                if (MySession.Static.MaxGridSize != 0 && blocksCount + blocksToBuild > MySession.Static.MaxGridSize)
                {
                    return MySession.LimitResult.MaxGridSize;
                }
                if (myIdentity != null)
                {
                    MyBlockLimits blockLimits = myIdentity.BlockLimits;
                    if (MySession.Static.BlockLimitsEnabled == MyBlockLimitsEnabledEnum.PER_FACTION && MySession.Static.Factions.GetPlayerFaction(myIdentity.IdentityId) == null)
                    {
                        return MySession.LimitResult.NoFaction;
                    }

                    if (blockLimits != null)
                    {
                        if (MySession.Static.MaxBlocksPerPlayer != 0 && blockLimits.BlocksBuilt + blocksToBuild > blockLimits.MaxBlocks)
                        {
                            return MySession.LimitResult.MaxBlocksPerPlayer;
                        }

                        if (MySession.Static.TotalPCU != 0 && pcuToBuild > blockLimits.PCU)
                        {
                            return MySession.LimitResult.PCU;
                        }

                        if (blocksPerType_forcheck != null)
                        {
                            using (Dictionary<string, short>.Enumerator enumerator = MySession.Static.BlockTypeLimits.GetEnumerator())
                            {
                                while (enumerator.MoveNext())
                                {
                                    KeyValuePair<string, short> keyValuePair = enumerator.Current;
                                    if (blocksPerType_forcheck.ContainsKey(keyValuePair.Key))
                                    {
                                        int КолвоБлоковОпределленногоТипаВСтруктуре = blocksPerType_forcheck[keyValuePair.Key];
                                        MyBlockLimits.MyTypeLimitData ЛимитыИгрокаНаБлок;
                                        if (blockLimits.BlockTypeBuilt.TryGetValue(keyValuePair.Key, out ЛимитыИгрокаНаБлок))
                                        {
                                            if (future_spawn)
                                            {
                                                КолвоБлоковОпределленногоТипаВСтруктуре = КолвоБлоковОпределленногоТипаВСтруктуре + ЛимитыИгрокаНаБлок.BlocksBuilt;
                                            }
                                        }

                                        if (КолвоБлоковОпределленногоТипаВСтруктуре > MySession.Static.GetBlockTypeLimit(keyValuePair.Key))
                                        {
                                            return MySession.LimitResult.BlockTypeLimit;
                                        }
                                    }
                                }
                                return MySession.LimitResult.Passed;
                            }
                        }
                        short blockTypeLimit = MySession.Static.GetBlockTypeLimit(blockName);
                        if (blockTypeLimit > 0)
                        {
                            MyBlockLimits.MyTypeLimitData myTypeLimitData2;
                            if (blockLimits.BlockTypeBuilt.TryGetValue(blockName, out myTypeLimitData2))
                            {
                                blocksToBuild += myTypeLimitData2.BlocksBuilt;
                            }

                            if (blocksToBuild > blockTypeLimit)
                            {
                                return MySession.LimitResult.BlockTypeLimit;
                            }
                        }
                    }
                }
                return MySession.LimitResult.Passed;
            }
        }
    }
}