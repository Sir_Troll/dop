﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Foogs
{
    /// <summary>
    /// Логика взаимодействия для FoogsShipsFixControlForm.xaml
    /// </summary>
    public partial class FoogsGarageControlForm : UserControl
    {
        public FoogsGarageControlForm()
        {
            InitializeComponent();
            string temp = SGarage.Instance.Config.GaragePath;
            SGarage.Instance.Config.GaragePath = temp;
        }

        private void Btn_OldGrids_Click(object sender, RoutedEventArgs e)
        {
                        
        }

        private void Btn_PCUChecker_Click(object sender, RoutedEventArgs e)
        {
           /* var editor = new PCULoopWindow()
            {
                Owner = Window.GetWindow(this),
                DataContext = DataContext,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            editor.ShowDialog();*/
        }

        private void Btn_saveRules_Click(object sender, RoutedEventArgs e)
        {
            /*var editor = new SaveRulesWindow()
            {
                Owner = Window.GetWindow(this),
                DataContext = DataContext,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            editor.ShowDialog();*/
        }

        private void Btn_loadRules_Click(object sender, RoutedEventArgs e)
        {
          /*  var editor = new LoadRulesWindow()
            {
                Owner = Window.GetWindow(this),
                DataContext = DataContext,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            editor.ShowDialog();*/
        }

        private void Btn_spelldict_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_keenecon_Click(object sender, RoutedEventArgs e)
        {
            //TODO
        }

        private void Textbox_garage_path_LostFocus(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(this.textbox_garage_path.Text))
            {
                this.textbox_garage_path.Background =  new SolidColorBrush(Colors.LightGreen);
                this.textbox_garage_path.Background.Opacity = 70;
            }
            else
            {
                this.textbox_garage_path.Background = new SolidColorBrush(Colors.Red);
                this.textbox_garage_path.Background.Opacity = 60;
            }
        }

        private void Btn_playerblacklist_Click(object sender, RoutedEventArgs e)
        {
           /* var editor = new CfgOverrides((DataContext as FoogsGarageConfig).PlayersOverrides)
            {
                Owner = Window.GetWindow(this),
                DataContext = (DataContext as FoogsGarageConfig).PlayersOverrides,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            editor.ShowDialog();

            SGarage.Instance.Config.RefreshModel();*/
        }

        private void btn_worth_Click(object sender, RoutedEventArgs e)
        {

        }

    
    }
}
