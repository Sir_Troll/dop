﻿using Sandbox.Engine.Networking;
using SJns.Code;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;

namespace SJns
{
    /// <summary>
    /// Логика взаимодействия для JumpGateControl.xaml
    /// </summary>
    public partial class JumpGateControl : UserControl
    {

        // public FoogsServerJumpPlugin PluginSettingsDataContext => (FoogsServerJumpPlugin)DataContext;
        public PluginSettings PluginSettingsLocal => (PluginSettings)DataContext;
        private SJumpTorch Plugin { get; }
        
        public JumpGateControl()
        {
            InitializeComponent();
        }

        public JumpGateControl(SJumpTorch plugin) : this()
        {
            Plugin = plugin;
            DataContext = MainLogic.PluginConfig;
        }
 
        private void Button_Click_GetPubIp(object sender, RoutedEventArgs e)
        {
            var IpAdress = MyGameService.GameServer.GetPublicIP();
            IPAddress ipaddress = IPAddressExtensions.FromIPv4NetworkOrder(IpAdress);

            MainLogic.ToLog("Result: " + ipaddress.ToString() + (ushort)Sandbox.MySandboxGame.ConfigDedicated.ServerPort);
        }
 
        private void Button_MYsqlsettings_Click(object sender, RoutedEventArgs e)
        { // ServerJumpClass.Instance._mysqlbaseconnecton.InsertFactionInfo(founderSteamID, factionId, factionTag, factionName, factionDescripti
           
        }

        private void btn_mysqlreconnect_Click(object sender, RoutedEventArgs e)
        {
           // Plugin._mysqlbaseconnecton.TryStartMySqlUpdateThreads();
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            //  FoogsServerJumpPlugin.Instance.SyncConfig.Save();
         //   Plugin.SyncConfig.RefreshModel();
           // Plugin.SyncConfig.Save();
            MainLogic.PluginConfig.RefreshModel();
        }

        [Obsolete]
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            /*
            if (FoogsServerJumpPlugin.Instance.Config.rechargemuylt != null
                && FoogsServerJumpPlugin.Instance.Config.ChargeMultiplier != 0)
            {
                FoogsServerJumpPlugin.Instance.Config.rechargemuylt.SetValue(null, FoogsServerJumpPlugin.Instance.Config.ChargeMultiplier);
            }
            else
            {
                Plugin.SomeLog("Cant connect to serverjump mod");
            }
            byte[] message = Encoding.UTF8.GetBytes(MyAPIGateway.Utilities.SerializeToXML(new Communication.ClientSettings
            {                
                depart_distance = FoogsServerJumpPlugin.Instance.Config.DepartureDistance,
                charge_distance = FoogsServerJumpPlugin.Instance.Config.ChargeDistance,
                charge_multipler = FoogsServerJumpPlugin.Instance.Config.ChargeMultiplier,
                req = false
            }));

            // ServerJumpClass.Instance.SomeLog($" send  DepartureDistance " + ServerJumpClass.Instance.Config.DepartureDistance);
           Communication.BroadcastToClients(Communication.MessageType.ClientSettings, message);*/
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var editor = new RemoteAPIForm
            {
                Owner = Window.GetWindow(this),
                DataContext = DataContext,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            editor.ShowDialog();
        }

       

        private void btn_my_cleanup_Click(object sender, RoutedEventArgs e)
        {
           
        }

     
    }
}
