﻿using Sandbox.ModAPI;
using SJump.API;
using System.IO;
using System.Threading;
using System.Windows.Controls;
using Torch;
using Torch.API;
using Torch.API.Plugins;

namespace SJump
{
    public class SJumpTorch : TorchPluginBase, IWpfPlugin
    {
        private UserControl _control;
        private static Persistent<PluginSettings> _config;
        private static MainLogic Logic = null;
        public static bool Loaded = false;
        public UserControl GetControl() => _control ?? (_control = new SJumpControl() { DataContext = _config.Data });

        public override void Init(ITorchBase torch)
        {
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.InvariantCulture;
            base.Init(torch);
            Loaded = true;
            _config = Persistent<PluginSettings>.Load(Path.Combine(StoragePath, "SJump.cfg"));
        }

        public override void Update()
        {
            if (MyAPIGateway.Session == null)
            {
                return;
            }

            if (Logic == null)
            {
                Logic = new MainLogic();
                Logic.InitLogic(_config.Data);
            }
        }

        public override void Dispose()
        {
            Logic.DisposeLogic();
        }
    }
}

