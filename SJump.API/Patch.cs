﻿using Sandbox.Engine.Multiplayer;
using Sandbox.Engine.Networking;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using SJump.API;
using SpaceEngineers.Game.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using Torch;
using Torch.Managers;
using Torch.Managers.PatchManager;
using VRage;
using VRage.Game.ModAPI;
using VRage.GameServices;
using VRage.Network;
using VRageMath;
using Torch.API.Managers;
using Torch.API.Plugins;

namespace SJump.API
{


    public static class Patch
    {
        private static readonly Random Random = new Random();
        public static Tuple<IPAddress, int> GetIPandPort()
        {

            var ip = typeof(MyDedicatedServerOverrides).GetField("IpAddress", BindingFlags.Public | BindingFlags.Static);
            if (ip == null)
            {
                throw new InvalidOperationException("Couldn't find IpAddress");
            }

            IPAddress Ip = (IPAddress)ip.GetValue(null);

            var port = typeof(MyDedicatedServerOverrides).GetField("Port", BindingFlags.Public | BindingFlags.Static);
            if (port == null)
            {
                throw new InvalidOperationException("Couldn't find Port");
            }

            int Port = (int)ip.GetValue(null);

            return new Tuple<IPAddress, int>(Ip, Port);

        }

        public static bool HookBlockLimiter(int minutes)
        {
            bool _restartPending = (bool)typeof(Torch.Commands.TorchCommands).GetField("_restartPending", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
            if (_restartPending)
            {
                return true;
            }

            PropertyInfo config = null;
            ITorchPlugin essentials = null;
            foreach (var plugin in TorchBase.Instance.Managers.GetManager<PluginManager>())
            {
                if (plugin.Id == Guid.Parse("cbfdd6ab-4cda-4544-a201-f73efa3d46c0")) //find concealment
                {
                    essentials = plugin;
                    config = plugin.GetType().GetProperty("Config", BindingFlags.Public | BindingFlags.Instance);
                    break;
                }
            }

            if (config == null)
            {
                return false;
            }

            return false;

        }
        /// <summary>
        /// If true restart happens soon, < minutes
        /// </summary>
        /// <returns></returns>
        public static bool IsRestartSoon(int minutes)
        {
            return false;
           /*
            bool _restartPending = (bool)typeof(Torch.Commands.TorchCommands).GetField("_restartPending", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
            if (_restartPending)
            {
                return true;
            }

            PropertyInfo config = null;
            ITorchPlugin essentials = null;
            foreach (var plugin in TorchBase.Instance.Managers.GetManager<PluginManager>())
            {
                if (plugin.Id == Guid.Parse("cbfdd6ab-4cda-4544-a201-f73efa3d46c0")) //find concealment
                {
                    essentials = plugin;
                    config = plugin.GetType().GetProperty("Config", BindingFlags.Public | BindingFlags.Instance);
                    break;
                }
            }

            if (config == null)
            {
                return false;
            }
            bool flag = false;

            try
            {
                dynamic configD = config.GetValue(essentials);
                dynamic autocommands = configD.AutoCommands;
                List<dynamic> PossibleRestartCommands = new List<dynamic>();
                foreach (var command in autocommands)
                {
                    string name = command.Name;
                    if (name.Contains("restart", StringComparison.InvariantCultureIgnoreCase))
                    {
                        PossibleRestartCommands.Add(command);
                    }
                }
                DateTime Timenow = DateTime.Now;
                foreach (var tmp in PossibleRestartCommands)
                {
                    TimeSpan sheduledtime = TimeSpan.Parse(tmp.ScheduledTime);
                    var steps = tmp.Steps;
                    foreach (var step in steps)
                    {
                        if (step != null)
                        {
                            if (((string)(step.Command)).Contains("restart", StringComparison.InvariantCultureIgnoreCase))
                            {

                                string comand = ((string)(step.Command)).Replace("restart ", "");
                                var sptittedcom = comand.Split(' ');
                                int seconds = 0;
                                if (sptittedcom[0] != null)
                                {
                                    seconds = Int32.Parse(sptittedcom[0]);
                                }
                                else
                                {
                                    seconds = 10;
                                }

                                TimeSpan restartdelay = TimeSpan.FromSeconds(seconds);
                                sheduledtime = sheduledtime + restartdelay;
                                var diff = (sheduledtime - Timenow.TimeOfDay);
                                if (diff <= new TimeSpan(0, minutes, 0) & diff > new TimeSpan(0, 0, 0))
                                {
                                    flag = flag || true;
                                }
                            }
                            sheduledtime = sheduledtime + TimeSpan.Parse(step.Delay);
                        }
                    }
                }
            }
            catch { }

            return flag;*/
        }
       

        private static MethodInfo Method(string name)
        {
            return typeof(Patch).GetMethod(name, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
        }

        [Obfuscation(Exclude = true, ApplyToMembers = true)]
        public static void Apply(PatchContext ctx)
        {
            
        }    
    }
}