﻿using Sandbox.Game.World;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using Torch;
using Torch.API.Managers;
using Torch.Managers.PatchManager;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.Utils;
using VRageMath;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Torch.Mod;
using Torch.Mod.Messages;
using NLog;
using Sandbox.Game;
using Sandbox.Game.Multiplayer;

namespace SJump.API
{
    public class MainLogic
    {
        private static List<long> m_lockentities = new List<long>();
        private static readonly Logger m_log = LogManager.GetLogger("SJump");
        public static PluginSettings PluginConfig;
        public static MainLogic Instanse;
        public string Storagepath;
        bool IsTorch = true;
        MyRemoteServer myRemoteServer = null;

        public void InitLogic(PluginSettings pluginconfig)
        {
            PluginConfig = pluginconfig;
            Instanse = this;

            if (IsTorch)
            {
                var torch = TorchBase.Instance;
                Storagepath = torch.Config.InstancePath;
                var mgr = torch.Managers.GetManager<PatchManager>();
                var ctx = mgr.AcquireContext();
                Patch.Apply(ctx); //apply hooks
                mgr.Commit();
                m_log.Warn("Init patch done.[normal method]");
                m_log.Warn($"ServerJumpRemoteApi initialized");
                myRemoteServer = new MyRemoteServer(PluginConfig.RemoteAPIPort, PluginConfig.RemoteApiKey);
            }
            SetupGates();
            m_log.Warn("Init SJump.API end.");
        }

        public void DisposeLogic()
        {
            myRemoteServer.Stop();
        }

        public void SendMsgToClient(string msg, ulong steamid = 0)
        {
            //TODO
            if (this.IsTorch)
            {
                // m_chatManager.SendMessageAsOther("HyperGate", msg, Color.Green, steamid, "White");
            }
            var playerId = Sync.Players.TryGetPlayerBySteamId(steamid).Identity.IdentityId;
            MyVisualScriptLogicProvider.SendChatMessage(msg, "HyperGate", playerId, "White");
        }

        public void StartJump(ulong steamId)
        {
            SendMsgToClient("Initializing jump...", steamId);
            MyAPIGateway.Parallel.StartBackground(
            () => DoJump(steamId));
        }


        private void DoJump(ulong steamId)
        {
            GateInfo targetGate = new GateInfo();
            ToLog("Jump request received by: " + steamId);
            if (!PluginConfig.Enabled)
            {
                ToLog("ServerJump is disabled in config!");
                SendMsgToClient("ServerJump disabled by admin!", steamId);
                return;
            }

            if (Patch.IsRestartSoon(10))
            {
                ToLog("Restart soon jump aborted for: " + steamId);
                SendMsgToClient("Restart will happens soon, try again later!", steamId);
                return;
            }

            IMyPlayer requestedplayer = Util.GetPlayerBySteamId(steamId);
            if (!(requestedplayer?.Controller?.ControlledEntity?.Entity is IMyCubeBlock block))
            {
                SendMsgToClient("Can't find your ship. Make sure you're seated in the ship you want to take with you.", steamId);
                return;
            }

            MyCubeGrid pilotedgGrid = (MyCubeGrid)(block?.CubeGrid);
            if (pilotedgGrid == null)
            {
                SendMsgToClient("Can't find your ship. Make sure you're seated in the ship you want to take with you.", steamId);
                return;
            }

            var pilotedgGridId = pilotedgGrid.EntityId;
            lock (m_lockentities)
            {
                if (m_lockentities.Contains(pilotedgGridId))
                {
                    ToLog("EntityId is already locked aborted! = " + pilotedgGridId);
                    SendMsgToClient("Failed to initialize jump because it is already in progress. Try later.", steamId);
                    return;
                }
                else
                {
                    ToLog("EntityId locked! = " + pilotedgGridId);
                    m_lockentities.Add(pilotedgGridId);
                }
            }

            targetGate = PluginConfig.Gates.FirstOrDefault(); // TODO
            var raw_grids = MyCubeGridGroups.Static.GetGroups(GridLinkTypeEnum.Logical).GetGroupNodes(pilotedgGrid);
            raw_grids.SortNoAlloc((x, y) => x.BlocksCount.CompareTo(y.BlocksCount));
            raw_grids.Reverse();
            raw_grids.SortNoAlloc((x, y) => x.GridSizeEnum.CompareTo(y.GridSizeEnum));
            // if (!CheckCanSaveShip(grids, target_projector, steamid, player))
            int index = 0;
            var gridsOB = new MyObjectBuilder_CubeGrid[raw_grids.Count];
            uint allPCU = 0;
            uint blockCount = 0;

            foreach (MyCubeGrid x in raw_grids)
            {
                var ob = (MyObjectBuilder_CubeGrid)x.GetObjectBuilder(true);
                gridsOB[index] = ob;
                index++;
                allPCU += (uint)x.BlocksPCU;
                blockCount += (uint)x.BlocksCount;
            }

            #region Find passengers
            //TODO union foreach's
            List<ulong> passengers = new List<ulong>();
            var blockss = new HashSet<MySlimBlock>();
            foreach (MyCubeGrid rgrid in raw_grids)
            {
                blockss.Clear();
                blockss = rgrid.GetBlocks();
                {
                    foreach (var slim in blockss)
                    {
                        var c = slim?.FatBlock as IMyCockpit;
                        if (c?.Pilot != null)
                        {
                            IMyPlayer pass_player = MyAPIGateway.Players.GetPlayerControllingEntity(c?.Pilot);
                            if (pass_player == null)
                            {
                                pass_player = MyAPIGateway.Players.GetPlayerControllingEntity(c?.Pilot.Parent);
                            }
                            if (pass_player != null)
                            {
                                passengers.Add(pass_player.SteamUserId);
                            }
                        }
                    }
                }
            }
            #endregion

            try
            {
                var gridpos = raw_grids[0].PositionComp.GetPosition();
                //List<IMySlimBlock> myblocks = new List<IMySlimBlock>();
               // var ScanBox = new BoundingSphereD(gridpos, targetGate.depart_dist).GetBoundingBox();
               // topmostallentities = MyAPIGateway.Entities.GetTopMostEntitiesInBox(ref ScanBox);
                Util.AdressAndPortFromString(targetGate.to_address, out ushort target_port, out string target_ip);
                string target_API_port = targetGate.to_apiPort;

                if (!API_requester.CheckSlots(target_ip + ":" + target_API_port))
                {
                    SendMsgToClient("No free slots on target server.", steamId);
                    UnlockGrid(pilotedgGridId);
                    return;
                }
                else
                {
                    SendMsgToClient("Target server is ready.", steamId);
                }
                /*
                }
                if (false)
                {
                if (!API_requester.CheckRestart(target_ip + ":" + target_API_port))
                {
                SendMsgToClient("Target server is restarting soon, please try later.", steamId);
                UnlockGrid(pilotedgGridId);
                return;
                }
                }*/

                if (targetGate.remote_limit_check)
                {
                    if (Util.RemoteLimitsAPI.CheckLimits_request_remote(gridsOB, "http://" + target_ip + ":" + target_API_port + "/SJump/LimitCheck/" + steamId))
                    {
                        SendMsgToClient("Remote limits check passed.", steamId);
                    }
                    else
                    {
                        SendMsgToClient("Your ship exceeds target server limits. Jump aborted.", steamId);
                        UnlockGrid(pilotedgGridId);
                        return;
                    }
                }

                if (targetGate.blocks_subtype_blacklist?.Count > 0)
                {
                    if (Util.CheckBlackList_blocks(gridsOB, targetGate.blocks_subtype_blacklist, out List<string> reason))
                    {
                        SendMsgToClient("Block blacklist check passed.Ship clear.", steamId);
                    }
                    else
                    {
                        SendMsgToClient("Your ship have blacklisted blocks. Jump aborted.", steamId);
                        UnlockGrid(pilotedgGridId);
                        return;
                    }
                }

                if (targetGate.items_subtype_blacklist?.Count > 0)
                {
                    if (Util.CheckBlackList_items(gridsOB, targetGate.items_subtype_blacklist, out List<string> reason))
                    {
                        SendMsgToClient("Items blacklist check passed.Ship inventory allowed.", steamId);
                    }
                    else
                    {
                        SendMsgToClient("Your ship have blacklisted items in inventory. Jump aborted.", steamId);
                        UnlockGrid(pilotedgGridId);
                        return;
                    }
                }

                if (targetGate.promote_lvl > MySession.Static.GetUserPromoteLevel(steamId))
                {
                    ToLog("Wrong role for this gate steamid=" + steamId);
                    SendMsgToClient("You do not have the correct rank to proceed through this gate...Jump aborted.", steamId);
                    UnlockGrid(pilotedgGridId);
                    return;
                }

                ToLog($"Gate config: IP/port[{target_ip}:{target_port}] color[{targetGate.gate_color}]");
                if (PluginConfig.LocalShipsBackup)
                {
                    Util.SaveShipsToFile(steamId, allPCU, blockCount, ref gridsOB);
                }

                if (String.IsNullOrWhiteSpace(PluginConfig.Server_alias))
                {
                    ToLog("[Jump out] Server alias not set! jump aborted.");
                    SendMsgToClient("Cant open wormhole, SJump not configured, contact admin.", steamId);
                    UnlockGrid(pilotedgGridId);
                    return;
                }

                JumpInfo jinfo = new JumpInfo()
                { PilotSteamId = steamId, TargetGate = Colors.Red, CustomData = new List<string> { "customdata kek" } };
                // PluginConfig.Server_alias, targetGate.to_address, (uint)targetGate.gate_color), steamId
                string APIAdress = target_ip + ":" + target_API_port;
                if (!Util.SendPayload(gridsOB, jinfo, APIAdress))
                {
                    SendMsgToClient("Cant open wormhole, SJump not configured, contact admin.", steamId);
                    UnlockGrid(pilotedgGridId);
                    return;
                }

                ToLog("[Jump out] Sended! ShipToJump: '" + gridsOB[0].DisplayName + "'");
                ToLog("[Jump out] Main pilot: " + Util.GetPlayerBySteamId(steamId)?.DisplayName +
                " steamId: " + steamId + " Grid: " + gridsOB[0]?.DisplayName +
                " To: " + target_ip +
                ":" + target_port);
                var timer = new System.Timers.Timer(1000);
                timer.AutoReset = false;
                timer.Elapsed += (a, b) => MyAPIGateway.Utilities.InvokeOnGameThread(() =>
                {
                    try
                    {
                        if (targetGate.jump_out_msg != null && targetGate.jump_out_msg != "")
                        {
                            MyAPIGateway.Utilities.SendMessage(targetGate.jump_out_msg);
                        }

                        if (!targetGate.copy_only)
                        {
                            foreach (var real_grid in raw_grids)
                            {
                                if (real_grid != null && !real_grid.MarkedForClose && !real_grid.Closed)
                                {
                                    real_grid.Close(); //TODO create explosion and clear depart area
                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MainLogic.ToLog(ex.ToString());
        //if fail here, grid never unlocked. anti dupe
    }
                    UnlockGrid(pilotedgGridId);
                });
                timer.Start();
                ToLog("passengers.Count: " + passengers.Count);
                foreach (var pass in passengers)
                {
                    SendPlayerToServer((target_ip +
                    ":" + target_port), pass);
                    ToLog("[Jump out] Passenger: " + Util.GetPlayerBySteamId(steamId)?.DisplayName +
                    " steamId: " + steamId + " Grid: " + gridsOB[0]?.DisplayName +
                    " To: " + target_ip +
                    ":" + target_port);
                }
            }
            catch (Exception e)
            {
                ToLog(" DoJump Exception: " + e.ToString());
                //if fail here, grid never unlocked. anti dupe
            }
            UnlockGrid(pilotedgGridId);
            return;
        }

        private void SetupGates()
        {
            if (PluginConfig.Gates == null) PluginConfig.Gates = new List<GateInfo>(); 
            if (PluginConfig.Gates.Any()) return;
            GateInfo gate_main = new GateInfo
            {
                gate_color = Colors.Black,
                to_address = "127.0.0.1:27016",
                to_apiPort = "5454",
                promote_lvl = MyPromoteLevel.None,
                jump_out_msg = "bye bye",
                jump_in_msg = "Incoming",
                depart_dist = 1000,
                minarrival_dist = 250,
                maxarrival_dist = 1250,
                remote_limit_check = true,
                reset_speed = true
            };
            PluginConfig.Gates = new List<GateInfo>() { gate_main };
        }
        private void UnlockGrid(long id)
        {
            lock (m_lockentities)
            {
                if (m_lockentities.Contains(id))
                {
                    ToLog("EntityId unlocked! = " + id);
                    m_lockentities.Remove(id);
                }
            }
        }

        private void SendPlayerToServer(string ipport, ulong psid)
        {
            if (this.IsTorch)
                ModCommunication.SendMessageTo(new JoinServerMessage(ipport), psid);
        }
        public static void ToLog(string text)
        {
            if (PluginConfig.LogEnabled)
            {
                if (PluginConfig.ColorLog)
                {
                    m_log.Warn(text);
                }
                else
                {
                    m_log.Info(text);
                }
            }
        }
    }
}
