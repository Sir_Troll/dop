﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using SJump.API;

namespace SJump.API
{
    internal static class API_requester
    {
        private static readonly HttpClient client = new HttpClient();
        /// <summary>
        /// If true , then player can jump 
        /// </summary>
        /// <param name="ipport"></param>
        /// <returns></returns>
        internal static bool CheckRestart(string ipport)
        {
            /// v101 / ServerJump / CheckRestart
            /// 

            var url = "http://" + ipport + "/api/v101/ServerJump/CheckRestart";
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Add(MainLogic.PluginConfig.RemoteApiKey, MainLogic.PluginConfig.RemoteApiKey);

            using (var client = new HttpClient())
            {
                try
                {
                    var sended_req = client.SendAsync(req);
                    sended_req.Wait();
                    var result1 = sended_req.Result.Content.ReadAsStringAsync();
                    MainLogic.ToLog("CheckRestart url = " + url + " result string =  " + result1 + sended_req.Result.StatusCode.ToString());

                    if (Int32.Parse(result1.Result) > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch { return false; }
            }
        }
        public static bool CheckAPIStatusRemoteSimple(string url = "/api/v101/ServerJump/status")
        {
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Add(MainLogic.PluginConfig.RemoteApiKey, MainLogic.PluginConfig.RemoteApiKey);

            using (var client = new HttpClient())
            {
                var sended_req = client.SendAsync(req);
                sended_req.Wait();
                var result1 = sended_req.Result.Content.ReadAsStringAsync().Result;
                MainLogic.ToLog("CheckAPIStatusRemote url = " + url + " result string =  " + result1 + sended_req.Result.StatusCode.ToString());

                if (sended_req.Result.StatusCode == HttpStatusCode.OK)
                    return true;
                return false;
            }
        }

        internal static bool CheckSlots(string ipport)
        {

            var url = "http://" + ipport + "/SJump/FreeSlots";
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Add(MainLogic.PluginConfig.RemoteApiKey, MainLogic.PluginConfig.RemoteApiKey);

            using (var client = new HttpClient())
            {
                try
                {
                    var sended_req = client.SendAsync(req);
                    sended_req.Wait();
                    var result1 = sended_req.Result.Content.ReadAsStringAsync();
                    MainLogic.ToLog("CheckSlots url = " + url + " result string =  " + result1 + sended_req.Result.StatusCode.ToString());

                    if (Int32.Parse(result1.Result) > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch { return false; }
            }
        }

        public static async Task<int> CheckAPIStatusRemote(string url = "/api/v101/ServerJump/status")
        {
            try
            {
                // await Task.Delay(TimeSpan.FromSeconds(3)).ConfigureAwait(false);
                var req = new HttpRequestMessage(HttpMethod.Get, url);

                //  req.Content = content;
                req.Headers.Add(MainLogic.PluginConfig.RemoteApiKey, MainLogic.PluginConfig.RemoteApiKey);

                using (var client = new HttpClient())
                {
                    //  var data = await client.GetByteArrayAsync(url).ConfigureAwait(false);



                    var sended_req = await client.SendAsync(req);

                    //  sended_req.Wait();
                    // var x = client.PostAsync(url, content);

                    var result1 = await sended_req.Content.ReadAsStringAsync();

                    //send req

                    //foreach (var player in BlocksAndOwnerForLimits)
                    //{
                    //   result &= CheckLimitsInternal(true, player.Key, null, FinalBlocksPCU, FinalBlocksCount, flag ? 0 : (MySession.Static.MaxGridSize + 1), player.Value);
                    //}
                    MainLogic.ToLog("CheckAPIStatusRemote url = " + url + " result string =  " + result1 + sended_req.StatusCode.ToString());
                    // return sended_req.StatusCode == HttpStatusCode.OK;
                    if (sended_req.StatusCode == HttpStatusCode.OK)
                        return 1;
                    return 0;
                }
            }
            catch
            {
                MainLogic.ToLog("FAIL!!:!::");
                return 0;
            }
        }
        public static bool SendPayloadTo(byte[] payload, string api_url)
        {
            var content = new StringContent(Convert.ToBase64String(payload));
            /* var content = new ByteArrayContent(payload);             
             var req = new HttpRequestMessage(HttpMethod.Post, api_url);
             req.Content = content;            */

            var req = new HttpRequestMessage(HttpMethod.Post, api_url);
            // var content = new ByteArrayContent(payload);
            req.Content = content;
            req.Headers.Add(MainLogic.PluginConfig.RemoteApiKey, MainLogic.PluginConfig.RemoteApiKey);
            HttpStatusCode res;
            lock (client)
            {
                var sended_req = client.SendAsync(req);
                sended_req.Wait();
                // var x = client.PostAsync(url, content);
                res = sended_req.Result.StatusCode;
            }
            MainLogic.ToLog("SendPayload URL:[" + api_url + "] Result:[" + res + "]");
            return res == HttpStatusCode.OK;
        }
    }
}
