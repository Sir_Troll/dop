﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.BZip2;
using Newtonsoft.Json;
using Sandbox;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Engine.Multiplayer;
using Sandbox.Engine.Networking;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.GameSystems.BankingAndCurrency;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using SpaceEngineers.Game.ModAPI;
using Torch;
using Torch.API.Managers;
using Torch.Utils;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.Game.ObjectBuilders.Components;
using VRage.Game.ObjectBuilders.ComponentSystem;
using VRage.ModAPI;
using VRage.Network;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRageMath;

namespace SJump.API
{
    public partial class Util
    {
        //TODO split this class
        public const string ZERO_IP = "0.0.0.0:27016";
        private static readonly List<IMyPlayer> _playerCache = new List<IMyPlayer>();
        private static readonly Random Random = new Random();

        public static bool RecievePayLoad(string req_body)
        {
            if (PayloadUnPack(Convert.FromBase64String(req_body), out ShipTransferObjectData clientData))
            {
                MyAPIGateway.Parallel.StartBackground(() =>
                {
                    //Playeer creation
                    Thread.Sleep(900);      //TODO убрать
                    var a = GridsObFromSTR_KEEEEEEN(clientData.Grids);
                    SpawnSomeGridsFromThread(a, clientData.jumpInfo.PilotSteamId, Vector3D.Zero, MainLogic.PluginConfig.Gates.Find(x => x.gate_color == clientData.jumpInfo.TargetGate));
                });
                return true;
            }
            else
            {
                return false;
            }
        }

        private static readonly HttpClient client = new HttpClient();


        public static void AdressAndPortFromString(string orig_adressstr, out ushort port, out string ip)
        {
            var adressstr = orig_adressstr.Replace("http://", "");
            adressstr = adressstr.Replace("https://", "");
            string[] array = adressstr.Trim().Split(':');
            if (array.Length < 2)
            {
                port = 27016;
            }
            else
            {
                port = ushort.Parse(array[1]);
            }
            ip = System.Net.Dns.GetHostAddresses(array[0])[0].ToString();
        }
        public static bool TryGetEntityByNameOrId(string nameOrId, out IMyEntity entity)
        {
            if (long.TryParse(nameOrId, out long id))
            {
                return MyAPIGateway.Entities.TryGetEntityById(id, out entity);
            }

            foreach (var ent in MyEntities.GetEntities())
            {
                if (ent.DisplayName == nameOrId)
                {
                    entity = ent;
                    return true;
                }
            }

            entity = null;
            return false;
        }

        /// <summary>
        /// true = passed
        /// </summary>
        /// <param name="grids_objectbuilders"></param>
        /// <param name="blacklist">list with blacklisted SubtypeName items</param>
        /// <returns></returns>
        public static bool CheckBlackList_items(MyObjectBuilder_CubeGrid[] grids_objectbuilders, List<string> blacklist, out List<string> reason)
        {
            //var blacklist = ParseBlacklist(blacklisted_subtypes);
            reason = new List<string>();
            foreach (var grid in grids_objectbuilders)
            {
                var blocks = grid.CubeBlocks;
                foreach (var block in blocks)
                {
                    if (block.ComponentContainer != null)
                    {
                        MyObjectBuilder_ComponentContainer.ComponentData componentData = block.ComponentContainer.Components.Find((MyObjectBuilder_ComponentContainer.ComponentData s) => s.Component.TypeId == typeof(MyObjectBuilder_Inventory));
                        if (componentData != null)
                        {

                            var itemsininv = (componentData.Component as MyObjectBuilder_Inventory).Items;
                            if (itemsininv == null || itemsininv.Count == 0) continue;
                            foreach (var blackitem in blacklist)
                            {

                                foreach (var item in itemsininv)
                                {
                                    if (item.PhysicalContent.SubtypeName.Equals(blackitem, StringComparison.InvariantCultureIgnoreCase))
                                        reason.Add(item.SubtypeName);
                                }
                            }
                        }
                    }
                }
            }

            if (reason.Any())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// true = passed
        /// </summary>
        /// <param name="grids_objectbuilders">where find</param>
        /// <param name="blacklist">list with blacklisted SubtypeName items</param>
        /// <returns></returns>
        public static bool CheckBlackList_blocks(MyObjectBuilder_CubeGrid[] grids_objectbuilders, List<string> blacklist, out List<string> reason)
        {
            reason = new List<string>();
            //TODO
            //var blacklist = ParseBlacklist(blacklisted_subtypes);
            foreach (var grid in grids_objectbuilders)
            {
                var result = grid.CubeBlocks.Where(x => blacklist.Contains(x.SubtypeId.String));
                foreach (var block in result)
                {
                    reason.Add(block.SubtypeName);
                }
            }

            if (reason.Any()) return false;

            return true;
        }
        public static List<string> ParseBlacklist(string stringblaccklist)
        {
            List<string> result = new List<string>();
            try
            {
                string[] separator1 = { "," };
                string[] stroki = stringblaccklist.Split(separator1, StringSplitOptions.RemoveEmptyEntries);
                int lehght = stroki.Length;
                if (lehght == 0)
                {
                    MainLogic.ToLog("Gate blacklist missconfigured!");
                    return result;
                }
                result = stroki.ToList<string>();
                return result;
            }
            catch (Exception ex)
            {
                MainLogic.ToLog("ParseBlacklist catch! = " + ex.ToString());
                return result;
            }

        }

        public static Dictionary<GateConfigEnum, string> ParseCustomData(string gatename, string customrawdata)
        {
            Dictionary<GateConfigEnum, string> result = new Dictionary<GateConfigEnum, string>(29)
{
{ GateConfigEnum.ADRESS,""},
{ GateConfigEnum.APIADRESS,""},
// { GateConfigEnum.PORT ,""},
// { GateConfigEnum.APIPORT ,""},
{ GateConfigEnum.COLOR, "" },
{ GateConfigEnum.RESET_SPEED ,""}, //gate color
{ GateConfigEnum.MINROLE,""}, //minimal role for jump (affects passagers too)
{ GateConfigEnum.COPYONLY,""}, //grid will be just copied to the target server
{ GateConfigEnum.DEPARTDISTANCE,""},
{ GateConfigEnum.WORTHTAXENABLE,""}, //enable this shit for frontier
{ GateConfigEnum.WORTHTAXPERCENT,""}, // % of grid worth
{ GateConfigEnum.PAYTOJUMPENABLE,""}, //simple pay to jump(frontier)
{ GateConfigEnum.JUMPCOST,""}, //coins/money/from frontier econmod
{GateConfigEnum.DMG_DRIVE_PERSENT,"" },
{GateConfigEnum.DISCHARGE_DRIVE_PERSENT,"" },
{ GateConfigEnum.CREATESAVEZONE,""}, //enable zafe zone created after jump
{ GateConfigEnum.SAVEZONERADIUS,""}, // gridboundingbox+100
{ GateConfigEnum.SAVEZONETIME,""}, //seconds
{ GateConfigEnum.JUMPOUT_MSG,""} ,
{ GateConfigEnum.JUMPIN_MSG,""} ,
{ GateConfigEnum.CHARGE_DIST,""},
{ GateConfigEnum.MINARRIVAL_DIST, ""} ,
{ GateConfigEnum.MAXARRIVAL_DIST, ""} ,
{ GateConfigEnum.CHARGE_MULTIPLIER, ""},
{ GateConfigEnum.LOCAL_LIMIT_CHECK, ""},
{ GateConfigEnum.REMOTE_LIMIT_CHECK, ""},
{ GateConfigEnum.BLOCKS_SUBTYPE_BLACKLIST, ""},
{ GateConfigEnum.ITEMS_SUBTYPE_BLACKLIST, ""},
{ GateConfigEnum.MINTIER, ""},
{ GateConfigEnum.TURN_OFF_DRIVE, ""}
};
            try
            {

                string[] separator1 = { ":" };
                string[] separator2 = { "\n" };
                string[] stroki = customrawdata.Split(separator2, StringSplitOptions.RemoveEmptyEntries);
                int lehght = stroki.Length;
                if (lehght != 28)
                {
                    MainLogic.ToLog("Gate on grid " + gatename + " missconfigured!");
                    result.Clear();
                    return result;
                }

                for (int i = 0; i < lehght; i++)
                {

                    if (i == 0)
                    {
                        string[] parameter = stroki[i].Split(separator1, StringSplitOptions.None);
                        if (String.IsNullOrWhiteSpace(parameter[0]) || String.IsNullOrWhiteSpace(parameter[1]) || String.IsNullOrWhiteSpace(parameter[2]))
                        {
                            result[(GateConfigEnum)i] = "";
                        }
                        result[(GateConfigEnum)i] = parameter[1] + ":" + parameter[2];
                    }
                    else
                    {
                        string[] parameter = stroki[i].Split(separator1, StringSplitOptions.None);
                        if (String.IsNullOrWhiteSpace(parameter[0]) || String.IsNullOrWhiteSpace(parameter[1]))
                        {
                            result[(GateConfigEnum)i] = "";
                        }
                        result[(GateConfigEnum)i] = parameter[1];
                    }


                }
                return result;
            }
            catch (Exception ex)
            {
                MainLogic.ToLog("ParseCustomData catch! = " + ex.ToString());
                return result;
            }
        }
        /// <summary>
        /// weird shit but only this works thx keen
        /// </summary>
        /// <param name="gridstotpob"></param>
        /// <returns></returns>
        private static string GridsObToSTR_KEEEEEEN(MyObjectBuilder_CubeGrid[] gridstotpob)
        {
            MyObjectBuilder_ShipBlueprintDefinition myObjectBuilder_ShipBlueprintDefinition = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_ShipBlueprintDefinition>();
            myObjectBuilder_ShipBlueprintDefinition.Id = new MyDefinitionId(new MyObjectBuilderType(typeof(MyObjectBuilder_ShipBlueprintDefinition)), MyUtils.StripInvalidChars("123"));
            myObjectBuilder_ShipBlueprintDefinition.DLCs = GetNecessaryDLCs(myObjectBuilder_ShipBlueprintDefinition.CubeGrids);
            myObjectBuilder_ShipBlueprintDefinition.CubeGrids = gridstotpob;
            //myObjectBuilder_ShipBlueprintDefinition.CubeGrids[0].DisplayName = blueprintDisplayName;
            MyObjectBuilder_Definitions myObjectBuilder_Definitions = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_Definitions>();
            myObjectBuilder_Definitions.ShipBlueprints = new MyObjectBuilder_ShipBlueprintDefinition[1];
            myObjectBuilder_Definitions.ShipBlueprints[0] = myObjectBuilder_ShipBlueprintDefinition;


            var memsteam = new MemoryStream();
            MyObjectBuilderSerializer.SerializeXML(memsteam, myObjectBuilder_Definitions);
            StreamReader strread = new StreamReader(memsteam);
            var finalstr = Encoding.UTF8.GetString(memsteam.ToArray());

            return finalstr;

        }

        private static MyObjectBuilder_CubeGrid[] GridsObFromSTR_KEEEEEEN(string data)
        {
            MyObjectBuilder_Base obs;
            MemoryStream stringInMemoryStream = new MemoryStream(UTF8Encoding.UTF8.GetBytes(data));

            MyObjectBuilderSerializer.DeserializeXML(stringInMemoryStream, out obs, typeof(MyObjectBuilder_Definitions));
            return (obs as MyObjectBuilder_Definitions).ShipBlueprints[0].CubeGrids;
        }

        public static bool SendPayload(MyObjectBuilder_CubeGrid[] gridob, JumpInfo jumpInfo, string API_Adress)
        {
            ShipTransferObjectData data = new ShipTransferObjectData(GridsObToSTR_KEEEEEEN(gridob), jumpInfo);
            string totalStr = JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full
            });

            bool result;
            using (MemoryStream source = new MemoryStream(Encoding.UTF8.GetBytes(totalStr)))
            {
                MemoryStream target = new MemoryStream();
                BZip2.Compress(source, target, false, 1024);
                totalStr = null;
                data = null;
                var url = "http://" + API_Adress + "/SJump/ShipDock/" + jumpInfo.PilotSteamId;
                result = API_requester.SendPayloadTo(target.ToArray(), url);
            }
            return result;
        }

        private static bool PayloadUnPack(byte[] rawdata, out ShipTransferObjectData clientData)
        {
            try
            {
                string obString;
                using (MemoryStream source = new MemoryStream(rawdata))
                {
                    using (MemoryStream target = new MemoryStream())
                    {
                        BZip2.Decompress(source, target, true);
                        obString = Encoding.UTF8.GetString(target.ToArray());
                    }
                }

                clientData = JsonConvert.DeserializeObject<ShipTransferObjectData>(obString, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto,
                    TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full
                });
                obString = null;
                return true;
            }
            catch (Exception ex)
            {
                MainLogic.ToLog("Error deserializing grid!");
                MainLogic.ToLog(ex.ToString());
                clientData = null;                
                return false;
            }
        }


        public static string PackToStrDictMembers(DictionaryReader<long, MyFactionMember> members)
        {
            if (members.Count == 1)
            {
                return Sync.Players.TryGetSteamId(members.First().Key).ToString();
            }
            string result = "";
            foreach (var mem in members)
            {
                var stimid = Sync.Players.TryGetSteamId(mem.Key);
                if (stimid == 0)
                {
                    continue;
                }

                result += stimid + ":";
            }
            if (result.Length <= 1)
            {
                return "0";
            }

            return result.Remove(result.Length - 1);

        }
        public static List<ulong> UnPackStrToMembers(string membersarray)
        {
            if (membersarray.Length < 5)
            {
                return new List<ulong>();
            }

            var members = membersarray.Split(':');
            List<ulong> listmembers = new List<ulong>();
            foreach (var membr in members)
            {
                if (membr == null || membr.Length < 1)
                {
                    continue;
                }

                listmembers.Add((ulong)Int64.Parse(membr));
            }
            return listmembers;
        }


        public static void SendOnNewPlayerSuccessToClient(ulong clientSteamId, int playerSerialId = 0)
        {//1)send in normal situation
            var onNewPlayerSuccesReflectedMethodInfoOrSth = typeof(MyPlayerCollection).GetMethod("OnNewPlayerSuccess", BindingFlags.Static | BindingFlags.NonPublic);

            MyMultiplayer.RaiseStaticEvent<ulong, int>((IMyEventOwner s) =>
            (Action<ulong, int>)Delegate.CreateDelegate(typeof(Action<ulong, int>), onNewPlayerSuccesReflectedMethodInfoOrSth),
            clientSteamId, playerSerialId, new EndpointId(clientSteamId), null);
            MainLogic.ToLog($"Send spawn request to {clientSteamId} - ok.");
        }

        /*public static void SendMedRooms(ulong steamid)
        {
        long arg = 0L;
        var ShowMedicalScreen_reflected = typeof(MySpaceRespawnComponent).GetMethod("ShowMedicalScreen_Implementation", BindingFlags.Static | BindingFlags.NonPublic);
        var delegate_showmedscreen = Delegate.CreateDelegate(typeof(Action<MOTDData, long>), ShowMedicalScreen_reflected);

        MyMultiplayer.RaiseStaticEvent<MOTDData, long>((IMyEventOwner s) =>
        (Action<MOTDData, long>)delegate_showmedscreen,
        MOTDData.Construct(), arg, new EndpointId(steamid), null);

        }*/
        /// <summary>
        /// Create player
        /// </summary>
        /// <param name="clientSteamId"></param>
        /// <param name="playerSerialId"></param>
        /// <param name="displayName"></param>
        /// <param name="myidentyyty"></param>
        /// <param name="newidentity"></param>
        public static void ConstructAndSendSpawnPlayer(ulong clientSteamId, int playerSerialId, string displayName, MyIdentity myidentyyty, bool newidentity = false)
        {
            // FoogsServerJumpPlugin.Instance.SomeLog("ConstructPlayer: start");
            MyPlayer player;
            // player = Sync.Players.TryGetPlayerBySteamId(clientSteamId);
            // if (player == null)
            // {

            //create identity,name. NOT character
            // player = Sync.Players.CreateNewPlayer(myidentyyty, new MyPlayer.PlayerId(clientSteamId, playerSerialId), displayName, realPlayer, true, true);
            // player.Identity = myidentyyty;

            //player not saved if player not spawn
            // }

            //this need call every spawn!
            player = Sync.Players.CreateNewPlayer(myidentyyty, new MyPlayer.PlayerId(clientSteamId, playerSerialId), displayName, true, true, newidentity);
            MainLogic.ToLog("(NewPlayerRequest)(ConstructPlayer): start...EntId = " + player.Identity.IdentityId);

            //if (CheckHaveIncomingShip(clientSteamId, player))
            //{
            // MainLogic.ToLog("(NewPlayerRequest)(ConstructPlayer): CheckHaveIncomingShip.......TRUE! return;");
            // return;
            //}

            MySession.Static.Factions.CompatDefaultFactions(new MyPlayer.PlayerId(clientSteamId, playerSerialId));
            SendOnNewPlayerSuccessToClient(clientSteamId, playerSerialId);

            MyAccountInfo myAccountInfo;
            if (!MyBankingSystem.Static.TryGetAccountInfo(myidentyyty.IdentityId, out myAccountInfo))
            {
                MyBankingSystem.Static.CreateAccount(myidentyyty.IdentityId);
            }
            return;
        }
        /// <summary>
        /// in game thread pls, will remove all characters in world
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public static void TryFindAndDeleteExistingCharacters(MyPlayer player)
        {
            MainLogic.ToLog("Find characters and delete. start");

            if (player == null)
            {
                return;
            }
            var temp = player.Identity.SavedCharacters.ToList();
            MainLogic.ToLog("Total bodyes: " + temp.Count);
            foreach (long entityId in temp)
            {
                MyCharacter myCharacter = MyEntities.GetEntityById(entityId, false) as MyCharacter;
                if (myCharacter != null)
                {

                    if (myCharacter.IsUsing is MyCryoChamber)
                    {
                        // FoogsServerJumpPlugin.Instance.SomeLog("Found character in Cryo");
                        // character = myCharacter;
                        // continue;
                        (myCharacter.IsUsing as MyCryoChamber).RemovePilot();
                        MainLogic.ToLog("Found body in Cryo[" + (myCharacter.IsUsing as MyCryoChamber).Name + "], erase start");
                        // (myCharacter.IsUsing as MyCryoChamber).ClearSavedpilot();

                    }
                    if (myCharacter.IsUsing is MyCockpit)
                    {

                        // FoogsServerJumpPlugin.Instance.SomeLog("Found character in Cockpit");
                        // character = myCharacter;
                        // continue;
                        (myCharacter.IsUsing as MyCockpit).RemovePilot();
                    }

                    // if (myCharacter != null)

                    //{
                    if (!myCharacter.IsDead)
                    {
                        /*
                        var inventory = myCharacter.GetInventory();
                        var tolbar = myCharacter.Toolbar;
                        bool haveinv = false;
                        bool havetoolbar = false;
                        if(inventory.Get != 0)
                        {
                        haveinv = true;
                        }*/
                    }

                    if (player.Identity.SavedCharacters.Contains(myCharacter.EntityId))
                    {
                        player.Identity.SavedCharacters.Remove(myCharacter.EntityId);
                    }

                    myCharacter.Close();
                    myCharacter = null;
                    MainLogic.ToLog("Found body erased.");
                    //}
                }
            }
            MainLogic.ToLog("Total bodyes: " + temp.Count);
            MainLogic.ToLog("Find characters and delete. end");
            // player.Identity.SavedCharacters.Clear();
            return;
        }

        public static MyCharacter CreateCharacter(MatrixD worldMatrix, Vector3 velocity,
        string characterName, string model, Vector3? colorMask, MyBotDefinition botDefinition,
        bool findNearPos = true, bool AIMode = false, MyCockpit cockpit = null, bool useInventory = true,
        long identityId = 0L, bool addDefaultItems = true)
        {
            Vector3D? vector3D = null;
            if (findNearPos)
            {
                vector3D = MyEntities.FindFreePlace(worldMatrix.Translation, 2f, 200, 5, 0.5f);
                if (vector3D == null)
                {
                    vector3D = MyEntities.FindFreePlace(worldMatrix.Translation, 2f, 200, 5, 5f);
                }
            }
            if (vector3D != null)
            {
                worldMatrix.Translation = vector3D.Value;
            }
            return CreateCharacterBase(worldMatrix, ref velocity, characterName, model, colorMask, AIMode, useInventory, botDefinition, identityId, addDefaultItems);
        }

        // Token: 0x06005B38 RID: 23352 RVA: 0x0027F170 File Offset: 0x0027D370
        private static MyCharacter CreateCharacterBase(MatrixD worldMatrix, ref Vector3 velocity,
        string characterName, string model, Vector3? colorMask, bool AIMode, bool useInventory = true,
        MyBotDefinition botDefinition = null, long identityId = 0L, bool addDefaultItems = true)
        {
            MyCharacter myCharacter = new MyCharacter();
            MyObjectBuilder_Character myObjectBuilder_Character = MyCharacter.Random();
            myObjectBuilder_Character.CharacterModel = (model ?? myObjectBuilder_Character.CharacterModel);
            if (colorMask != null)
            {
                myObjectBuilder_Character.ColorMaskHSV = colorMask.Value;
            }
            myObjectBuilder_Character.JetpackEnabled = MySession.Static.CreativeMode;
            myObjectBuilder_Character.Battery = new MyObjectBuilder_Battery
            {
                CurrentCapacity = 1f
            };
            myObjectBuilder_Character.AIMode = AIMode;
            myObjectBuilder_Character.DisplayName = characterName;
            myObjectBuilder_Character.LinearVelocity = velocity;
            myObjectBuilder_Character.PositionAndOrientation = new MyPositionAndOrientation?(new MyPositionAndOrientation(worldMatrix));
            myObjectBuilder_Character.CharacterGeneralDamageModifier = 1f;
            myObjectBuilder_Character.OwningPlayerIdentityId = new long?(identityId);
            myCharacter.Init(myObjectBuilder_Character);
            MyEntities.RaiseEntityCreated(myCharacter);
            MyEntities.Add(myCharacter, true);
            MyInventory inventory = myCharacter.GetInventory(0);
            if (useInventory)
            {
                if (inventory != null && addDefaultItems)
                {
                    MyWorldGenerator.InitInventoryWithDefaults(inventory);
                }
            }
            else if (botDefinition != null)
            {
                botDefinition.AddItems(myCharacter);
            }
            if (velocity.Length() > 0f)
            {
                Sandbox.Game.Entities.Character.Components.MyCharacterJetpackComponent jetpackComp = myCharacter.JetpackComp;
                if (jetpackComp != null)
                {
                    jetpackComp.EnableDampeners(false);
                }
            }
            return myCharacter;
        }
        private static MyCharacter ConstructCharacter(long IdentityID)
        {
            MyObjectBuilder_Character myObjectBuilder_Character = MyCharacter.Random();

            // myObjectBuilder_Character.PositionAndOrientation = this.Transform;

            myObjectBuilder_Character.JetpackEnabled = false;
            myObjectBuilder_Character.DampenersEnabled = false;
            if (myObjectBuilder_Character.Inventory == null)
            {
                myObjectBuilder_Character.Inventory = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_Inventory>();
            }
            myObjectBuilder_Character.OwningPlayerIdentityId = IdentityID;
            //MyWorldGenerator.FillInventoryWithDefaults(myObjectBuilder_Character.Inventory, MyScenarioDefinition.StartingItem);
            MyCharacter myCharacter = new MyCharacter();
            myCharacter.Name = "Player";

            myCharacter.Init(myObjectBuilder_Character);
            MyEntities.RaiseEntityCreated(myCharacter);
            MyEntities.Add(myCharacter, true);
            // this.CreateAndSetPlayerFaction();
            // MySession.Static.LocalHumanPlayer.SpawnIntoCharacter(myCharacter);
            return myCharacter;
        }
        private static void TryFindAndRemoveExistingCharacter(MyPlayer player)
        {
            if (player == null)
            {
                return;
            }
            foreach (long entityId in player.Identity.SavedCharacters)
            {
                MyCharacter myCharacter = MyEntities.GetEntityById(entityId, false) as MyCharacter;
                if (myCharacter != null && !myCharacter.IsDead)
                {
                    if (myCharacter.Parent == null)
                    {
                        myCharacter.Kill(true, new MyDamageInformation(true, 9999, MyStringHash.GetOrCompute("Deformation"), 0));
                        myCharacter.Close();
                        myCharacter = null;
                        return;
                    }
                    if (myCharacter.Parent is MyCockpit)
                    {
                        MyCockpit myCockpit = myCharacter.Parent as MyCockpit;
                        myCockpit.RemovePilot();
                        myCharacter.Kill(true, new MyDamageInformation(true, 9999, MyStringHash.GetOrCompute("Deformation"), 0));
                        myCharacter.Close();
                        myCharacter = null;
                        return;
                    }
                }
            }
            return;
        }


        public static MyIdentity AllocateIdentityForRemotePlayer(ulong clientSteamId, string displayName, string characterModel, long identityId)
        {
            MyPlayer.PlayerId playerId = new MyPlayer.PlayerId(clientSteamId, 0);
            //long identityId = MyEntityIdentifier.AllocateId(MyEntityIdentifier.ID_OBJECT_TYPE.IDENTITY, MyEntityIdentifier.ID_ALLOCATION_METHOD.RANDOM);

            if (Sync.Players.HasIdentity(identityId))
            {
                MyIdentity identitycheck = Sync.Players.TryGetIdentity(identityId);
                long identitybysteamid = Sync.Players.TryGetIdentityId(clientSteamId);
                if (identitycheck != null)
                {
                    var serversteamid = Sync.Players.TryGetSteamId(identitycheck.IdentityId);
                    if (serversteamid != clientSteamId)
                    {
                        MainLogic.ToLog
                        ("serversteamid != clientSteamId cant create player Identity in USE!!!!!!remote steam id:" + clientSteamId + "/identity steam id:" + serversteamid);
                        throw new Exception("Fatal DB desync!");
                    }
                    else
                    {

                        return identitycheck; //we create this early?
                    }
                }
                if (identitybysteamid != 0)
                {
                    MyIdentity tempidentitycheck = Sync.Players.TryGetIdentity(identityId);
                    if (tempidentitycheck.IdentityId != identityId)
                    {
                        MainLogic.ToLog
                        ("localidentitycheckbyremotesteamid.IdentityId != remote identityId!remote steam id:" + clientSteamId + "/local identity by steam id:" + tempidentitycheck.IdentityId);
                        throw new Exception("Fatal DB desync!");
                    }
                    else
                    {
                        return tempidentitycheck; //we create this early?
                    }
                }
                MainLogic.ToLog
                ("New player not in the cache,we create new EntID and they not in the server but exist in the DB. Fatal DB desync!");
                throw new Exception("Fatal DB desync!");
            }

            MyEntityIdentifier.MarkIdUsed(identityId);
            //TODO MyMultiplayer.RaiseStaticEvent<bool, long, string>((IMyEventOwner s) => new Action<bool, long, string>(MyPlayerCollection.OnIdentityCreated), addToNpcs, identity.IdentityId, identity.DisplayName, default(EndpointId), null);
            return Sync.Players.CreateNewIdentity(displayName, identityId, characterModel, new Vector3(255, 255, 255));
        }
        /// <summary>
        ///
        /// Create fake playrer on server save. return new indentity.
        /// working at 03.04.18
        /// not working for already joined!!!!
        /// </summary>
        /// <param name="steamid"></param>
        /// <param name="playerName"></param>
        /// <param name="characterModel"></param>
        /// <returns></returns>
        private static long CreateRemotePlayerLocaly_internal(ulong steamid, long identityID, string playerName, string characterModel)
        {
            var steamfakeclient = new MyNetworkClient(steamid, playerName);
            MyPlayer.PlayerId playerId = new MyPlayer.PlayerId(steamid, 0);
            MyIdentity myIdentity = Sync.Players.TryGetPlayerIdentity(playerId);

            if (myIdentity == null)
            {
                // myIdentity = Sync.Players.RespawnComponent.CreateNewIdentity(playerName, playerId, characterModel);
                // myIdentity = MySession.Static.Players.CreateNewIdentity(playerName, identityID, characterModel, Vector3.Zero);
                myIdentity = AllocateIdentityForRemotePlayer(steamid, playerName, characterModel, identityID);
            }

            var olo = Sync.Players.CreateNewPlayer(myIdentity, steamfakeclient, myIdentity.DisplayName, true);
            Sync.Players.RemovePlayer(olo, true);
            //Thread.Sleep(20); //TODO REMOVE

            // long id = MyAPIGateway.Players.TryGetIdentityId(steamid);
            return myIdentity.IdentityId;
        }

        public static long CreateNewPlayer(ulong steamid, string playerName, string characterModel)
        {
            var steamfakeclient = new MyNetworkClient(steamid, playerName);
            MyPlayer.PlayerId playerId = new MyPlayer.PlayerId(steamid, 0);
            var myIdentity = Sync.Players.RespawnComponent.CreateNewIdentity(playerName, playerId, characterModel);
            var fakeplayer = Sync.Players.CreateNewPlayer(myIdentity, steamfakeclient, myIdentity.DisplayName, true);
            Sync.Players.RemovePlayer(fakeplayer, true);
            return myIdentity.IdentityId;
        }

        public static IMyPlayer GetPlayerBySteamId(ulong steamId)
        {
            _playerCache.Clear();
            MyAPIGateway.Players.GetPlayers(_playerCache);
            return _playerCache.FirstOrDefault(p => p.SteamUserId == steamId);
        }

        public static IMyPlayer GetPlayerById(long identityId)
        {
            _playerCache.Clear();
            MyAPIGateway.Players.GetPlayers(_playerCache);
            return _playerCache.FirstOrDefault(p => p.IdentityId == identityId);
        }

        /// <summary>
        /// Randomizes a vector by the given amount
        /// </summary>
        /// <param name="start"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static Vector3D RandomPositionFromPoint(Vector3D start, double distance)
        {
            double z = Random.NextDouble() * 2 - 1;
            double piVal = Random.NextDouble() * 2 * Math.PI;
            double zSqrt = Math.Sqrt(1 - z * z);
            var direction = new Vector3D(zSqrt * Math.Cos(piVal), zSqrt * Math.Sin(piVal), z);

            //var mat = MatrixD.CreateFromYawPitchRoll(RandomRadian, RandomRadian, RandomRadian);
            //Vector3D direction = Vector3D.Transform(start, mat);
            direction.Normalize();
            start += direction * -2;
            return start + direction * distance;
        }

        public static void CollectGatesInWord()
        {
            /* HashSet<IMyEntity> entities = new HashSet<IMyEntity>();
            List<IMySlimBlock> blocks = new List<IMySlimBlock>();
            Dictionary<long, GateInfo> gatesEntId = new Dictionary<long, GateInfo>();
            MyAPIGateway.Entities.GetEntities(entities, x => x.Closed != true);

            foreach (var item in entities)
            {
            blocks.Clear();
            var cubegrid = (item as IMyCubeGrid);
            if (cubegrid != null)
            {
            cubegrid.GetBlocks(blocks);
            foreach (var item2 in blocks)
            {
            if (item2.BlockDefinition.Id.SubtypeId == TorchSJump.APIPlugin.gate_subtype)
            {
            if (ParseGateInfo(item2, out GateInfo newgate))
            {
            gatesEntId.Add(newgate.gate_entityid, newgate);
            }
            }
            }
            }
            }
            MainLogic.Instance.PluginConfig.Gates = gatesEntId;
            MainLogic.ToLog("CollectGatesInWord: Active gates count =" + gatesEntId.Count);
            Setupgates();
            MainLogic.ToLog("CollectGatesInWord setup gates fin");
            return;*/
        }

        public static bool SaveShipsToFile(ulong steamId, uint totalpcu, uint totalblocks, ref MyObjectBuilder_CubeGrid[] grids_objectbuilders)
        {
            string gridname = "";
            if (grids_objectbuilders[0].DisplayName.Length > 30)
            {
                gridname = grids_objectbuilders[0].DisplayName.Substring(0, 30);
            }
            else
            {
                gridname = grids_objectbuilders[0].DisplayName;
            }
            string filenameexported = DateTime.Now.ToShortDateString() + "_" + DateTime.Now.ToLongTimeString() + "_" + totalpcu + "_" + totalblocks + "_" + gridname + "_" + Random.Next();
            var a1 = Path.GetInvalidPathChars();
            var b1 = Path.GetInvalidFileNameChars();

            var arraychar = a1.Concat(b1);
            foreach (char c in arraychar)
            {
                filenameexported = filenameexported.Replace(c.ToString(), ".");
            }

            return SaveToFile(ref grids_objectbuilders, steamId, filenameexported); //backup grid gruop to folder
        }

        private static bool SaveToFile(ref MyObjectBuilder_CubeGrid[] gridstotpob, ulong steamid, string filenameexported)
        {
            MyObjectBuilder_ShipBlueprintDefinition myObjectBuilder_ShipBlueprintDefinition = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_ShipBlueprintDefinition>();
            myObjectBuilder_ShipBlueprintDefinition.Id = new MyDefinitionId(new MyObjectBuilderType(typeof(MyObjectBuilder_ShipBlueprintDefinition)), MyUtils.StripInvalidChars(filenameexported));
            myObjectBuilder_ShipBlueprintDefinition.DLCs = GetNecessaryDLCs(myObjectBuilder_ShipBlueprintDefinition.CubeGrids);
            myObjectBuilder_ShipBlueprintDefinition.CubeGrids = gridstotpob;
            myObjectBuilder_ShipBlueprintDefinition.RespawnShip = false;
            myObjectBuilder_ShipBlueprintDefinition.DisplayName = filenameexported;
            myObjectBuilder_ShipBlueprintDefinition.OwnerSteamId = Sync.MyId;
            MyObjectBuilder_Definitions myObjectBuilder_Definitions = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_Definitions>();
            myObjectBuilder_Definitions.ShipBlueprints = new MyObjectBuilder_ShipBlueprintDefinition[1];
            myObjectBuilder_Definitions.ShipBlueprints[0] = myObjectBuilder_ShipBlueprintDefinition;
            string mypathdir = Path.Combine(MainLogic.PluginConfig.BackupPath, steamid.ToString());
            if (!Directory.Exists(mypathdir))
            {
                Directory.CreateDirectory(mypathdir);
            }

            string finalpath = Path.Combine(mypathdir, filenameexported + ".sbc");
            var flag = MyObjectBuilderSerializer.SerializeXML(finalpath, false, myObjectBuilder_Definitions, null);
            if (flag)
                MyObjectBuilderSerializer.SerializePB(finalpath + MyObjectBuilderSerializer.ProtobufferExtension, true, myObjectBuilder_Definitions);

            return flag;
        }
        private static string[] GetNecessaryDLCs(MyObjectBuilder_CubeGrid[] cubeGrids)
        {
            if (cubeGrids.IsNullOrEmpty<MyObjectBuilder_CubeGrid>())
            {
                return null;
            }
            HashSet<string> hashSet = new HashSet<string>();
            for (int i = 0; i < cubeGrids.Length; i++)
            {
                foreach (MyObjectBuilder_CubeBlock builder in cubeGrids[i].CubeBlocks)
                {
                    MyCubeBlockDefinition cubeBlockDefinition = MyDefinitionManager.Static.GetCubeBlockDefinition(builder);
                    if (cubeBlockDefinition != null && cubeBlockDefinition.DLCs != null && cubeBlockDefinition.DLCs.Length != 0)
                    {
                        for (int j = 0; j < cubeBlockDefinition.DLCs.Length; j++)
                        {
                            hashSet.Add(cubeBlockDefinition.DLCs[j]);
                        }
                    }
                }
            }
            return hashSet.ToArray<string>();
        }
        private static void RecreateGrid(List<MyObjectBuilder_CubeGrid> oGrids)
        {
            Vector3D basePos = Vector3D.Zero;
            var grids = new HashSet<IMyEntity>();
            var blocks = new List<IMySlimBlock>();

            Console.WriteLine("RecreateGrid");
            MyAPIGateway.Entities.GetEntities(grids, e => e is IMyCubeGrid && (e as IMyCubeGrid).GridSizeEnum == MyCubeSize.Large);

            foreach (IMyCubeGrid grid in grids)
            {
                Console.WriteLine(grid.DisplayName);
                Console.WriteLine(grid.GetPosition().ToString());
                blocks.Clear();
                grid.GetBlocks(blocks, e => e.FatBlock != null && e.FatBlock.BlockDefinition.SubtypeId.StartsWith("Supergate")); //only our Subtypes

                foreach (var block in blocks)
                {
                    Console.WriteLine("found gate");
                    basePos = grid.GetPosition();
                }
            }

            MyAPIGateway.Entities.RemapObjectBuilderCollection(oGrids); // ERRRRM i found something EEERRRMM
            var baseGrid = oGrids[0];

            if (basePos.Equals(Vector3D.Zero))
            {
                basePos = baseGrid.PositionAndOrientation.Value.Position;
            }

            Vector3D translation = Vector3D.Zero - basePos;

            oGrids.ForEach((x) =>
            {
                IMyEntity ent = MyAPIGateway.Entities.CreateFromObjectBuilderParallel(x, false);

                if (x == oGrids[0])
                {
                    translation = (Vector3D)MyAPIGateway.Entities.FindFreePlace(Vector3D.Zero, (float)ent.WorldVolume.Radius) - basePos;
                }

                //because whatever
                Matrix fromQuaternion = Matrix.CreateFromQuaternion(x.PositionAndOrientation.Value.Orientation);
                Vector3 forward = fromQuaternion.Forward;
                Vector3 up = fromQuaternion.Up;

                MatrixD myMatrixD = MatrixD.CreateWorld(x.PositionAndOrientation.Value.Position, forward, up);
                myMatrixD.Translation = myMatrixD.Translation + translation;
                ent.PositionComp.SetWorldMatrix(myMatrixD);

                ent.Physics.LinearVelocity = Vector3D.Zero;
                ent.Physics.AngularVelocity = Vector3D.Zero;
                MyAPIGateway.Entities.AddEntity(ent);
            });
        }
        /// <summary>
        /// Just read file and call spawn function
        /// </summary>
        /// <param name="targetent"></param>
        /// <param name="path"></param>
        public static void ImportGridsFromFile(IMyEntity targetent, string path)
        {//TODO FIX THIS COPY TO GARAGE!
            string xmlstring = System.IO.File.ReadAllText(path);

            MyObjectBuilder_Definitions myObjectBuilder_Definitions = MyObjectBuilderSerializer.CreateNewObject<MyObjectBuilder_Definitions>();

            myObjectBuilder_Definitions = MyAPIGateway.Utilities.SerializeFromXML<MyObjectBuilder_Definitions>(xmlstring);
            if (myObjectBuilder_Definitions != null)
            {
                var cubeGrids = myObjectBuilder_Definitions.ShipBlueprints[0].CubeGrids;
                var newOwnerId = (targetent as MyCubeGrid).BigOwners.FirstOrDefault<long>(); //for test
                var steamid = Sync.Players.TryGetSteamId(newOwnerId);
                var position = targetent.GetPosition();

                // SpawnSomeGridsFromThread_old(cubeGrids, steamid, position, false);
            }
            else
            {
                // MainLogic.Instanse.SendMsgToClient("Server", "Error",;
            }
        }
        private static MyObjectBuilder_CubeGrid RemovePilots(MyObjectBuilder_CubeGrid grid)
        {
            foreach (MyObjectBuilder_CubeBlock myObjectBuilder_CubeBlock in grid.CubeBlocks)
            {
                MyObjectBuilder_Cockpit myObjectBuilder_Cockpit = myObjectBuilder_CubeBlock as MyObjectBuilder_Cockpit;
                if (myObjectBuilder_Cockpit != null)
                {
                    myObjectBuilder_Cockpit.ClearPilotAndAutopilot();
                    if (myObjectBuilder_Cockpit.ComponentContainer != null && myObjectBuilder_Cockpit.ComponentContainer.Components != null)
                    {
                        foreach (MyObjectBuilder_ComponentContainer.ComponentData componentData in myObjectBuilder_Cockpit.ComponentContainer.Components)
                        {
                            if (componentData.TypeId == typeof(MyHierarchyComponentBase).Name)
                            {
                                MyObjectBuilder_HierarchyComponentBase myObjectBuilder_HierarchyComponentBase = (MyObjectBuilder_HierarchyComponentBase)componentData.Component;
                                myObjectBuilder_HierarchyComponentBase.Children.RemoveAll((MyObjectBuilder_EntityBase x) => x is MyObjectBuilder_Character);
                                break;
                            }
                        }
                    }
                    MyObjectBuilder_CryoChamber myObjectBuilder_CryoChamber = myObjectBuilder_Cockpit as MyObjectBuilder_CryoChamber;
                    if (myObjectBuilder_CryoChamber != null)
                    {
                        myObjectBuilder_CryoChamber.Clear();
                    }
                }
                else
                {
                    MyObjectBuilder_LandingGear myObjectBuilder_LandingGear = myObjectBuilder_CubeBlock as MyObjectBuilder_LandingGear;
                    if (myObjectBuilder_LandingGear != null)
                    {
                        myObjectBuilder_LandingGear.IsLocked = false;
                        myObjectBuilder_LandingGear.MasterToSlave = null;
                        myObjectBuilder_LandingGear.AttachedEntityId = null;
                        myObjectBuilder_LandingGear.LockMode = SpaceEngineers.Game.ModAPI.Ingame.LandingGearMode.Unlocked;
                    }
                }
            }
            return grid;
        }

        /// <summary>
        /// Remap, make ownership and spawn given grids gruop.And set correct position
        /// </summary>
        public static void SpawnSomeGridsFromThread(MyObjectBuilder_CubeGrid[] cubeGrids, ulong steamid,
        Vector3D pos, GateInfo gateinfo)
        {
            SpawnCounter counter = null;
            Vector3D targetposition = pos;
            MyObjectBuilder_CubeGrid baseGrid = cubeGrids[0];
            BoundingSphereD sphere = new BoundingSphereD(Vector3D.Zero, 0);//BoundingSphere 0/0
            double randomdistance = Random.Next(gateinfo.minarrival_dist, gateinfo.maxarrival_dist);
            targetposition = Util.RandomPositionFromPoint(targetposition, randomdistance);
            MyAPIGateway.Entities.RemapObjectBuilderCollection(cubeGrids);
            counter = new SpawnCounter(cubeGrids, targetposition, steamid, cubeGrids.Count(), gateinfo);

            for (int i = 0; i < cubeGrids.Length; i++)
            {
                var ob = cubeGrids[i];
                foreach (MyObjectBuilder_CubeBlock block in ob.CubeBlocks)
                {
                    if (!(block is MyObjectBuilder_Cockpit c))
                    {
                        continue;
                    }
                    if (c.Pilot != null)
                    {//TODO
                        /*
                        var pilotSteamId = c.Pilot.PlayerSteamId;
                        var pilotIdentityId = MyAPIGateway.Multiplayer.Players.TryGetIdentityId(pilotSteamId);
                        if (pilotIdentityId == -1)
                        {
                        Log.Info("cannot find player, removing character from cockpit, steamid: " + pilotSteamId);
                        cockpit.Pilot = null;
                        continue;
                        }
                        cockpit.Pilot.OwningPlayerIdentityId = pilotIdentityId;

                        var pilotIdentity = MySession.Static.Players.TryGetIdentity(pilotIdentityId);
                        if (pilotIdentity.Character != null)
                        {
                        if (Config.ThisIp != null && Config.ThisIp != "")
                        {
                        ModCommunication.SendMessageTo(new JoinServerMessage(Config.ThisIp), pilotSteamId);
                        }
                        KillCharacter(pilotSteamId);
                        }
                        pilotIdentity.PerformFirstSpawn();
                        pilotIdentity.SavedCharacters.Clear();
                        pilotIdentity.SavedCharacters.Add(cockpit.Pilot.EntityId);
                        MyAPIGateway.Multiplayer.Players.SetControlledEntity(pilotSteamId, cockpit.Pilot as VRage.ModAPI.IMyEntity);*/
                    }
                }
                ob.IsStatic = false;
               // cubeGrids[i] = RemovePilots(ob);

                if (gateinfo.reset_speed)
                {
                    cubeGrids[i].LinearVelocity = new SerializableVector3(0, 0, 0);
                    cubeGrids[i].AngularVelocity = new SerializableVector3(0, 0, 0);

                }

                if (i == 0)
                {
                    sphere = cubeGrids[i].CalculateBoundingSphere();
                }
                else
                {
                    sphere = BoundingSphereD.CreateMerged(sphere, (cubeGrids[i].CalculateBoundingSphere())); //TODO add this ti shipfix
                }
            }

            MainLogic.ToLog("(SpawnSomeGrids) Sphere radius = " + sphere.Radius.ToString());

            MyAPIGateway.Utilities.InvokeOnGameThread(() =>
            {

                MainLogic.ToLog("(SpawnSomeGrids) Spawn target pos = " + targetposition.ToString());
                Vector3D newcleanpos = targetposition;
                Vector3D? newcleanpostemp = MyAPIGateway.Entities.FindFreePlace(targetposition, (float)sphere.Radius + 100); // - basePos
                if (newcleanpostemp != null)
                {
                    // newcleanpos = (Vector3D)newcleanpostemp - targetposition; //some booms im think this is wrong
                    newcleanpos = (Vector3D)newcleanpostemp;//more safe
                }
                else
                {
                    MainLogic.ToLog("(SpawnSomeGrids) Cant find free place,chosen new rendom(1000)point");
                    newcleanpos = Util.RandomPositionFromPoint(newcleanpos, 1000);
                }
                MainLogic.ToLog("(SpawnSomeGrids) Final pos for spawn = " + newcleanpos.ToString());

                Vector3D oldpos = cubeGrids[0].PositionAndOrientation.GetValueOrDefault().Position + Vector3D.Zero;

                for (int i = 0; i < cubeGrids.Length; i++) // set position
                {
                    var ob = cubeGrids[i];

                    if (i == 0)
                    {

                        if (ob.PositionAndOrientation.HasValue)
                        {
                            var posiyto = ob.PositionAndOrientation.GetValueOrDefault();
                            //FoogsServerJumpPlugin.Instance.SomeLog("old pos main grid = " + (posiyto.Position - Vector3D.Zero).ToString());
                            posiyto.Position = newcleanpos;
                            ob.PositionAndOrientation = posiyto;
                            MainLogic.ToLog("(SpawnSomeGrids) new pos main grid = " + (ob.PositionAndOrientation.GetValueOrDefault().Position - Vector3D.Zero).ToString());

                        }
                    }
                    else
                    {
                        // FoogsServerJumpPlugin.Instance.SomeLog("1)ob before set i= " + i + " pos " + ((ob.PositionAndOrientation.GetValueOrDefault().Position - Vector3D.Zero).ToString()));


                        var o = ob.PositionAndOrientation.GetValueOrDefault();
                        o.Position = newcleanpos + o.Position - oldpos;
                        ob.PositionAndOrientation = o;
                        // FoogsServerJumpPlugin.Instance.SomeLog("2)ob after magic i= " + i + " pos " + ((ob.PositionAndOrientation.GetValueOrDefault().Position - Vector3D.Zero).ToString()));
                    }
                    counter._targetGatePosition = newcleanpos;
                    MyAPIGateway.Entities.CreateFromObjectBuilderParallel(ob, false, counter.Increment);
                }
            });
        }

        public static void Explode(Vector3D position, float damage, double radius, IMyEntity owner, MyExplosionTypeEnum type, bool affectVoxels = true)
        {
            var exp = new MyExplosionInfo(damage, damage, new BoundingSphereD(position, radius), type, true)
            {
                Direction = Vector3D.Up,
                ExplosionFlags = MyExplosionFlags.APPLY_FORCE_AND_DAMAGE | MyExplosionFlags.CREATE_PARTICLE_EFFECT | MyExplosionFlags.APPLY_DEFORMATION,
                OwnerEntity = owner as MyEntity,
                VoxelExplosionCenter = position
            };
            if (affectVoxels)
            {
                exp.AffectVoxels = true;
            }

            MyExplosions.AddExplosion(ref exp);
        }
    }





}
