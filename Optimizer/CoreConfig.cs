using Slime.GUI;
using System;
using System.Runtime.CompilerServices;
using Torch;

namespace Slime.Config
{
    public class CoreConfig : ViewModel
    {
        private bool _enabledSlowdownSafezoneEnabled = false;
        private int _enabledSlowdownSafezoneEnabledTimes = 3;

        private bool _slowPBUpdateEnable = false;
        private int _slowPBUpdate1 = 10;
        private int _slowPBUpdate10 = 4;
        private int _slowPBUpdate100 = 2;
        private string _slowPBIgnored = "";

        private string _disabledMethods = "";
        private string _implementations = "";
        private string _experimental = "";
        private string _customOptimizations = "";

        [DisplayTab(Name = "DisabledMethods", GroupName = "Optimizations", Tab = "Optimizations", Order = 0)]
        public string DisabledMethods { get => _disabledMethods; set => SetValue(ref _disabledMethods, value); }

        [DisplayTab(Name = "Implementations", GroupName = "Optimizations", Tab = "Optimizations", Order = 0)]
        public string Implementations { get => _implementations; set => SetValue(ref _implementations, value); }

        //=================================================================================================

        [DisplayTab(Name = "Enabled", GroupName = "ProgramBlock", Tab = "Optimizations", Order = 1)]
        public bool SlowPBEnabled { get => _slowPBUpdateEnable; set => SetValue(ref _slowPBUpdateEnable, value); }

        [DisplayTab(Name = "Update 1 Slow", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "Description 1-> vanilla, 5-> 4 frames idle, 1 update")]
        public int SlowPBUpdate1 { get => _slowPBUpdate1; set => SetValue(ref _slowPBUpdate1, Math.Max(1, value)); }

        [DisplayTab(Name = "Update 10 Slow", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "Description 1-> vanilla, 5-> 49 frames idle, 1 update")]
        public int SlowPBUpdate10 { get => _slowPBUpdate10; set => SetValue(ref _slowPBUpdate10, Math.Max(1, value)); }

        [DisplayTab(Name = "Update 100 Slow", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "Description 1-> vanilla, 5-> 499 frames idle, 1 update")]
        public int SlowPBUpdate100 { get => _slowPBUpdate100; set => SetValue(ref _slowPBUpdate100, Math.Max(1, value)); }

        [DisplayTab(Name = "Ignored subtypes", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "This subtypeIds will be ignored. use ' ' or ',' as a separator")]
        public string SlowPBIgnored { get => _slowPBIgnored; set => SetValue(ref _slowPBIgnored, value); }

        //=================================================================================================

        [DisplayTab(Name = "On/Off", GroupName = "Safezones", Tab = "Optimizations", Order = 1)]
        public bool EnabledSlowdownSafezoneEnabled { get => _enabledSlowdownSafezoneEnabled; set => SetValue(ref _enabledSlowdownSafezoneEnabled, value); }

        [DisplayTab(Name = "Slowdown times", GroupName = "Safezones", Tab = "Optimizations", Order = 2, Description = "-1 Disables at all pulling out from safezones")]
        public int EnabledSlowdownSafezoneEnabledTimes { get => _enabledSlowdownSafezoneEnabledTimes; set => SetValue(ref _enabledSlowdownSafezoneEnabledTimes, Math.Max(-1, value)); }
    }
}