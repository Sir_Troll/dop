﻿using Slime.Config;
using Slime.Features;
using Slime.GUI;
using Slime.Slow;
using System.IO;
using Torch;
using Torch.API;
using Torch.Managers.PatchManager;
using TorchPlugin;

namespace Slime
{
    public class Optimizer : CommonPlugin
    {
        public static CoreConfig Config => _config.Data;
        private static Persistent<CoreConfig> _config;

        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            _config = Persistent<CoreConfig>.Load(Path.Combine(StoragePath, "MIG-Optimizer.cfg"));
        }

        public override void Dispose()
        {
            _config.Save(Path.Combine(StoragePath, "MIG-Optimizer.cfg"));
            base.Dispose();
        }

        public override void Patch(PatchContext context)
        {
            base.Patch(context);
            Optimizations.InitHack(context);
            OptimizationSafezone.InitHack(context);
            ColdProgramBlock.InitHack(context);
        }

        public override System.Windows.Controls.UserControl CreateControl()
        {
            return new MainControl();
        }
    }
}
