﻿using System.Collections.Generic;
using Sandbox.Game.Entities;
using Slime.Slow;
using Torch.Managers.PatchManager;
using NAPI;

namespace Slime
{
    internal class OptimizationSafezone
    {

        private static readonly Dictionary<long, int> dictionary = new Dictionary<long, int>();
        public static void InitHack(PatchContext patchContext)
        {
            if (Optimizer.Config.EnabledSlowdownSafezoneEnabledTimes > 0) return;
            patchContext.Prefix(typeof(MySafeZone), "UpdateBeforeSimulation", typeof(OptimizationSafezone), new[] { "__instance" });
        }

        private static bool UpdateBeforeSimulation(ref MySafeZone __instance)
        {
            if (Optimizer.Config.EnabledSlowdownSafezoneEnabledTimes <= -1) return false;
            return Optimizations.Slow(__instance.EntityId, dictionary, Optimizer.Config.EnabledSlowdownSafezoneEnabledTimes);
        }
    }
}