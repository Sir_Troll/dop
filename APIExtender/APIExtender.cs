﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.Entities;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Torch.Managers.PatchManager;
using VRage.Collections;
using VRage.Game.ModAPI;
using VRage.Game.ObjectBuilders.Components;
using VRageMath;
using NAPI;
using Sandbox.Game.GameSystems;
using Sandbox.Game.Entities.Character;
using System.Windows.Controls;
using TorchPlugin;
using Torch.API;
using Slime.Config;
using Torch;
using System.IO;
using Slime.GUI;
using Features;

namespace Slime
{
    public class APIExtender : CommonPlugin
    {

        public static CoreConfig Config => _config.Data;
        private static Persistent<CoreConfig> _config;

        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            _config = Persistent<CoreConfig>.Load(Path.Combine(StoragePath, "MIG-APIExtender.cfg"));
        }

        public override void Patch(PatchContext patchContext)
        {
            APIExtensions.Init(patchContext);
            SafeZoneFix.Patch(patchContext);
            ProjectorFixPatch.Patch(patchContext);

        }

        public override void Dispose()
        {
            _config.Save(Path.Combine(StoragePath, "MIG-APIExtender.cfg"));
            base.Dispose();
        }

        public override UserControl CreateControl()
        {
            return new MainControl();
        }
    }
}