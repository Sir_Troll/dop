﻿using System;
using NAPI;
using Sandbox.Engine.Multiplayer;
using Sandbox.Game.Entities;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using Slime;
using Torch.Managers.PatchManager;

namespace Features
{
    public static class SafeZoneFix
    {
        public static void Patch(PatchContext patchContext)
        {
            if (APIExtender.Config.SkipSafezoneValidation)
            {
                patchContext.Prefix(typeof(MySessionComponentSafeZones), typeof(SafeZoneFix), "IsPlayerValidationFailed");
            }
        }

        private static bool IsPlayerValidationFailed(ref bool __result, ulong steamId, long safeZoneBlockId, out MyIdentity playerIdentity, out MyCubeBlock beaconBlock)
        {
            //Log.Info("Hit: IsPlayerValidationFailedFix");
            playerIdentity = null;
            beaconBlock = null;
            MyCubeBlock myCubeBlock;
            if (!MyEntities.TryGetEntityById<MyCubeBlock>(safeZoneBlockId, out myCubeBlock, false))
            {
                MyMultiplayerServerBase myMultiplayerServerBase = MyMultiplayer.Static as MyMultiplayerServerBase;
                if (myMultiplayerServerBase != null)
                {
                    myMultiplayerServerBase.ValidationFailed(steamId, true, null, true);
                }
                __result = true;
                return false;
            }

            //TODO add check for server steamid
            beaconBlock = myCubeBlock;
            playerIdentity = Sync.Players.TryGetIdentity(beaconBlock.OwnerId);
           //Log.Info("Hit: IsPlayerValidationFailedFix __result = false;");
            __result = false;
            return false;
        }
    }
}
