﻿using System.Collections.Generic;
using Sandbox.Game.Entities.Blocks;
using Torch.Managers.PatchManager;
using VRage.Game;
using NAPI;
using Slime;

namespace Features
{
    public static class ProjectorFixPatch
    {
        public static void Patch(PatchContext ctx)
        {
            if (APIExtender.Config.EnableBlueprintProtection)
            {
                ctx.Redirect(typeof(MyProjectorBase), typeof(ProjectorFixPatch), "OnNewBlueprintSuccess");
                Log.Warn ("PATCHING: MyProjectorBase->OnNewBlueprintSuccess");
            }
        }

        private static void OnNewBlueprintSuccess(MyProjectorBase __instance, ref List<MyObjectBuilder_CubeGrid> projectedGrids)
        {
            var proj = __instance;
            if (proj == null)
            {
                Log.Warn("No projector?");
            }

            var grid = projectedGrids[0];
            RemoveStockPile(grid);
        }

        private static void RemoveStockPile(MyObjectBuilder_CubeGrid grid)
        {
            if (grid == null)
            {
                return;
            }

            var projectedBlocks = grid.CubeBlocks;

            foreach (var block in projectedBlocks)
            {
                if (block is MyObjectBuilder_ProjectorBase)
                {
                    ((MyObjectBuilder_ProjectorBase)block).ProjectedGrids = null;
                }

                if (block.ConstructionStockpile == null || block.ConstructionStockpile.Items.Length == 0)
                {
                    continue;
                }

                block.ConstructionStockpile = null;
            }
        }
    }
}
