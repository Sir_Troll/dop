using Slime.GUI;
using System;
using System.Runtime.CompilerServices;
using Torch;

namespace Slime.Config
{
    public class CoreConfig : ViewModel
    {
        private bool _skipSafezoneValidation = false;
        private bool _enableSafezonesAPI = false;
        private bool _enableJumpDriveAPI = false;
        private bool _enableBlueprintProtection = false;
        private bool _enableTurretsNotShootingFix = false;

        [DisplayTab(Name = "SkipSafezoneValidation", GroupName = "Features", Tab = "Features", Order = 0, Description = "Used for mods, being able to manipulate safezones")]
        public bool SkipSafezoneValidation { get => _skipSafezoneValidation; set => SetValue(ref _skipSafezoneValidation, value); }

        [DisplayTab(Name = "EnableSafezonesAPI", GroupName = "Features", Tab = "Features", Order = 0)]
        public bool EnableSafezonesAPI { get => _enableSafezonesAPI; set => SetValue(ref _enableSafezonesAPI, value); }

        [DisplayTab(Name = "EnableJumpDriveAPI", GroupName = "Features", Tab = "Features", Order = 0)]
        public bool EnableJumpDriveAPI { get => _enableJumpDriveAPI; set => SetValue(ref _enableJumpDriveAPI, value); }

        [DisplayTab(Name = "EnableBlueprintProtection", GroupName = "Features", Tab = "Features", Order = 0, Description = "Preventing getting Items from blueprint stockpile")]
        public bool EnableBlueprintProtection { get => _enableBlueprintProtection; set => SetValue(ref _enableBlueprintProtection, value); }

        [DisplayTab(Name = "EnableTurretsNotShootingFix", GroupName = "Fixes", Tab = "Features", Order = 0, Description = "With this fix, turrets will always see targets")]
        public bool EnableTurretsNotShootingFix { get => _enableTurretsNotShootingFix; set => SetValue(ref _enableTurretsNotShootingFix, value); }
    }
}