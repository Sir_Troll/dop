﻿using Torch.API;
using Torch.Managers.PatchManager;
using NAPI;
using Slime.Features;
using Slime;
using System.Windows.Controls;
using Slime.GUI;
using System.Collections.Generic;
using System;

namespace TorchPlugin
{
	public class FreezerPlugin : CommonPlugin
	{
		public static Freezer freezer;
		public static FreezerPlugin instance;
		public static SimTracker simTracker = new SimTracker();

		public override void Init(ITorchBase torch)
		{
			base.Init(torch);

			instance = this;

			freezer = new Freezer ();
			freezer.Init (torch, StoragePath);

			SlowLogic.Init();
			FrameExecutor.addFrameLogic (freezer);
			FrameExecutor.addFrameLogic (simTracker);
			FrameExecutor.addFrameLogic(new SlowLogic());

		}

		public override void Dispose()
		{
			freezer.Dispose(StoragePath);
			base.Dispose();
		}

		public override void Patch(PatchContext patchContext)
		{
			Ext2.RegisterGuids(new List<Guid>() { FreezerExtra.ConvertGuid });
			FreezerEnergyFix.Init();
            FixFreezer.InitHack (patchContext);
			PhysicsTracker.Init(patchContext);
			MemoryTracker.Init();
		}

		public override UserControl CreateControl()
		{
			return new FreezerGUI() { DataContext = freezer };
		}
	}
}