﻿using System;
using System.Collections.Generic;
using System.Linq;
using NAPI;
using Sandbox.Definitions;
using Sandbox.Engine.Multiplayer;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using VRage.Game.ModAPI;
using VRage.Network;
using VRageMath;


namespace Slime
{

    public static class SSS
    {
        public static bool IsOnlyParts(this MyCubeGrid Grid)
        {

            int Blockcount = Grid.CubeBlocks.Count;
            var blocks = Grid.CubeBlocks.Where
                (x => x is IMyMotorAdvancedRotor | x is IMyMotorAdvancedStator
            | x is IMyMotorBase | x is IMyMotorRotor | x is IMyMotorStator | x is IMyMotorSuspension
            | x is IMyWheel | x is IMyPistonBase | x is IMyPistonTop
            | x is IMyExtendedPistonBase);

            if (Blockcount == blocks.Count())
                return true;
            return false;
        }
    }

    public class FreezerExtra
    {
        public static Guid ConvertGuid = new Guid("202b2c90-65b7-4c70-93fe-d255ae546d3d");

        public static void LockGridGroup(MyCubeGrid main)
        {
            //FindAndOffSafeZone(main);
            var grids = main.GetConnectedGrids(GridLinkTypeEnum.Physical);

            foreach (var x in grids)
            {
                if (!x.IsStatic && x.GetStorageData(ConvertGuid) == null)
                {
                    var _v1 = Vector3D.Zero;
                    var _v2 = Vector3D.Zero;
                    if (x.Physics != null)
                    {
                        _v1 = x.Physics.LinearVelocity;
                        _v2 = x.Physics.AngularVelocity;
                    }

                    // x.SetStorageData(ConvertGuid, "d");                   
                    x.SetStorageData(ConvertGuid, new SimpleData() { s1 = "d", v1 = _v1, v2 = _v2 }.Serialize());
                }
            }

            /*
            if (!main.IsStatic)
            {
                main?.Physics?.SetSpeeds(Vector3.Zero, Vector3.Zero);
                Torch.TorchBase.Instance.Invoke(() =>
                {
                    MyMultiplayer.RaiseEvent<MyCubeGrid>(main, (MyCubeGrid x) => new Action(x.ConvertToStatic), default(EndpointId));

                });
            }*/

            foreach (var grid in grids)
            {
                (grid as MyCubeGrid).OverFatBlocks((x) =>
                {
                    var suspension = x as IMyMotorSuspension;
                    if (suspension != null)
                    {
                        suspension.SetStorageData(ConvertGuid, new SimpleData { d1 = suspension.Height, d2 = suspension.Strength }.Serialize());
                        suspension.Height = (suspension.SlimBlock.BlockDefinition as MyMotorSuspensionDefinition).MaxHeight;
                        suspension.Strength = 100;
                        return;
                    }

                    var rotor = x as IMyMotorStator;
                    if (rotor != null)
                    {
                        rotor.SetStorageData(ConvertGuid, new SimpleData { d1 = rotor.TargetVelocityRPM, d2 = rotor.BrakingTorque, l1 = rotor.RotorLock ? 1 : 0 }.Serialize());
                        // (rotor as MyMotorStator).Physics?.SetSpeeds(Vector3.Zero, Vector3.Zero);
                        // (rotor as MyMotorStator)?.TopGrid?.Physics?.SetSpeeds(Vector3.Zero, Vector3.Zero);
                        rotor.RotorLock = true;
                        rotor.TargetVelocityRPM = 0;
                        rotor.BrakingTorque = (rotor.SlimBlock.BlockDefinition as MyMotorStatorDefinition).UnsafeTorqueThreshold;
                        return;
                    }

                    var piston = x as IMyPistonBase;
                    if (piston != null)
                    {
                        piston.SetStorageData(ConvertGuid, new SimpleData { d1 = piston.Velocity }.Serialize());
                        piston.Velocity = 0;
                        return;
                    }
                });
            }

            foreach (MyCubeGrid grid in grids)
            {
                Torch.TorchBase.Instance.Invoke(() =>
                {
                    grid?.Physics?.SetSpeeds(Vector3.Zero, Vector3.Zero);
                    MyMultiplayer.RaiseEvent<MyCubeGrid>(grid, (MyCubeGrid x) => new Action(x.ConvertToStatic), default(EndpointId));
                });
            }
        }

        public static void UnlockGrid(IMyCubeGrid main)
        {
            try
            {
                var grids = main.GetConnectedGrids(GridLinkTypeEnum.Mechanical);
                var grids_to_unstatic = new List<IMyCubeGrid>(); //make them dynamic
                var mains = new List<IMyCubeGrid>();
                var grids_to_unfreeze = new List<Tuple<IMyCubeGrid, Vector3D, Vector3D>>(); //revert all settings on rotor pistons wheels


                foreach (var x in grids)
                {
                    bool flag = false;
                    SimpleData _data = new SimpleData() { v1 = Vector3D.Zero, v2 = Vector3D.Zero };
                    var d = x.GetAndSetStorageData(ConvertGuid, null);
                    try
                    {


                        _data = SimpleData.Deserialize(d);
                        flag = _data.s1 == "d";

                    }
                    catch { flag = d == "d"; }


                    var _v1 = Vector3D.Zero;
                    var _v2 = Vector3D.Zero;
                    if (!(_data == null && _data?.v1 == null && _data?.v2 == null))
                    {
                        _v1 = _data.v1;
                        _v2 = _data.v2;
                    }


                    if (flag)
                    {
                        grids_to_unstatic.Add(x);
                        grids_to_unfreeze.Add(new Tuple<IMyCubeGrid, Vector3D, Vector3D>(x, _v1, _v2));
                    }
                    else
                    {
                        if ((x as MyCubeGrid).CubeBlocks.Count == 1)
                        {//if wheels ot piston/rotor tops
                            if ((x as MyCubeGrid).IsOnlyParts())
                            {
                                grids_to_unstatic.Add(x);
                            }
                        }
                        grids_to_unfreeze.Add(new Tuple<IMyCubeGrid, Vector3D, Vector3D>(x, _v1, _v2));
                    }
                }

                Torch.TorchBase.Instance.Invoke(() =>
                {
                    foreach (var x in grids_to_unstatic)
                    {
                        if (x == null || x.Closed || x.MarkedForClose) return;
                        MyMultiplayer.RaiseEvent<MyCubeGrid>((x as MyCubeGrid), (MyCubeGrid y)
                            => new Action(y.OnConvertToDynamic), default(EndpointId));

                        Log.Fatal("RaiseEvent turn to dyn: " + x.DisplayName + " result " + x.IsStatic.ToString());

                        (x as MyCubeGrid).OnConvertToDynamic();
                        //  MyMultiplayer.ReplicateImmediatelly(MyExternalReplicable.FindByObject((x as MyCubeGrid)));
                    }
                });

                foreach (var g in grids_to_unfreeze)
                {
                    g.Item1.OverFatBlocks((x) =>
                    {
                        var suspension = x as IMyMotorSuspension;
                        if (suspension != null)
                        {
                            var d = suspension.GetAndSetStorageData(ConvertGuid, null);
                            if (d != null)
                            {
                                var sd = SimpleData.Deserialize(d);
                                suspension.Height = (float)sd.d1;
                                suspension.Strength = (float)sd.d2;
                            }
                            return;
                        }

                        var rotor = x as IMyMotorStator;
                        if (rotor != null)
                        {
                            var d = rotor.GetAndSetStorageData(ConvertGuid, null);
                            if (d != null)
                            {
                                var sd = SimpleData.Deserialize(d);
                                rotor.RotorLock = sd.l1 == 1;
                                rotor.TargetVelocityRPM = (float)sd.d1;
                                rotor.BrakingTorque = (float)sd.d2;
                            }
                            return;
                        }

                        var piston = x as IMyPistonBase;
                        if (piston != null)
                        {
                            var d = piston.GetAndSetStorageData(ConvertGuid, null);
                            if (d != null)
                            {
                                var sd = SimpleData.Deserialize(d);
                                piston.Velocity = (float)sd.d1;
                            }
                            return;
                        }
                    });
                }

                Torch.TorchBase.Instance.Invoke(() =>
                {
                    foreach (var x in grids_to_unfreeze)
                    {
                        if (x == null || x.Item1.Closed || x.Item1.MarkedForClose || x.Item1.IsStatic) return;

                        x.Item1.Physics?.SetSpeeds(x.Item2, x.Item3);
                    }
                });
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}