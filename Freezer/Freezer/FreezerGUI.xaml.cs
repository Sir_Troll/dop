﻿#region

using System;
using System.Windows;
using System.Windows.Controls;
using TorchPlugin;

#endregion

namespace Slime.GUI
{
	public partial class FreezerGUI : UserControl {
        public FreezerGUI() { InitializeComponent(); }

        private Freezer Plugin => (Freezer) DataContext;
        private void Run(Action a) { FreezerPlugin.InvokeTorch(a); }

        private void Reveal_OnClick(object sender, RoutedEventArgs e) {
            var p = Plugin;
            Run(delegate { p.RevealGrids(p.Settings.Data.UnFreezeDistance); });
        }

        private void Conceal_OnClick(object sender, RoutedEventArgs e) {
            var p = Plugin;
            Run(delegate { p.ConcealGrids(p.Settings.Data.FreezeDistance, p.Settings.Data.BatchAmount); });
        }

        private void RevealAll_OnClick(object sender, RoutedEventArgs e) {
            var p = Plugin;
            Run(delegate { p.RevealAll(); });
        }

        private void FreezeAll_OnClick(object sender, RoutedEventArgs e) {
            var p = Plugin;
            Run(delegate { p.ConcealGrids(p.Settings.Data.FreezeDistance, 0); });
        }


        private void GetMod_Click(object sender, RoutedEventArgs e) { System.Diagnostics.Process.Start("https://steamcommunity.com/sharedfiles/filedetails/?id=1981073813"); }

        private void EditExclusion_OnClick(object sender, RoutedEventArgs e) {
            //var editor = new CollectionEditor() {Owner = Window.GetWindow(this)};
            //editor.Edit<string>(Plugin.Settings.Data.ExcludedSubtypes, "Excluded Subtypes");
        }
    }
}