﻿using System;
using System.Collections.Generic;
using TorchPlugin;

namespace Slime
{
    public static class FreezerCache
    {
        private static HashSet<String> _excludedSubtypesSet = null;
        private static HashSet<String> _excludedWorkingSubtypesSet = null;

        public static HashSet<String> ExcludedSubtypesSet
        {
            get
            {
                if (_excludedSubtypesSet == null)
                {
                    _excludedSubtypesSet = new HashSet<String>();
                    var d = FreezerPlugin.freezer.Config.ExcludedSubtypes.Split(new string[] { " ", "\r\n", "\n", "," }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var s in d) { _excludedSubtypesSet.Add(s.ToString()); }
                }
                return _excludedSubtypesSet;
            }
        }

        public static HashSet<String> ExcludedWorkingSubtypesSet
        {
            get
            {
                if (_excludedWorkingSubtypesSet == null)
                {
                    _excludedWorkingSubtypesSet = new HashSet<String>();
                    var d = FreezerPlugin.freezer.Config.ExcludedWorkingSubtypes.Split(new string[] { " ", "\r\n", "\n", "," }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var s in d)
                    {
                        _excludedWorkingSubtypesSet.Add(s.ToString());
                    }
                }
                return _excludedWorkingSubtypesSet;
            }
        }
    }
}
