﻿using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Slime.Features;
using SpaceEngineers.Game.Entities.Blocks;
using System;
using System.Collections.Generic;
using System.Reflection;
using TorchPlugin;

namespace Slime
{
    class FreezerEnergyFix
    {
        public static Guid FREEZE_TIME_GUID = new Guid("8baaff08-432d-44e5-ab95-f7524a46a634");
        public static MethodInfo StorePower;

        public static void Init()
        {
            if (FreezerPlugin.freezer.Config.OfflinePowerProduction)
            {
                Ext2.RegisterGuids(new List<Guid>() { FREEZE_TIME_GUID });
                StorePower = typeof(MyBatteryBlock).easyMethod("StorePower");
            }
        }

        public static void OnFreeze(List<MyCubeGrid> grids)
        {
            if (!FreezerPlugin.freezer.Config.OfflinePowerProduction) return;
            var ts = SharpUtils.timeStamp();

            foreach (var x in grids)
            {
                x.GetStorage().SetValue(FREEZE_TIME_GUID, "" + ts);
            }
        }

        public static void OnUnFreeze(List<MyCubeGrid> grids)
        {
            try
            {
                if (!FreezerPlugin.freezer.Config.OfflinePowerProduction) return;

                var solarMlt = FreezerPlugin.freezer.Settings.Data.SolarPanelMlt;
                var windMlt = FreezerPlugin.freezer.Settings.Data.WindTurbineMlt;
                var generated = 0f;
                var timepassed = 0f;
                var batts = new List<MyBatteryBlock>();
                foreach (var x in grids)
                {
                    var storage = x.GetAndSetStorageData(FREEZE_TIME_GUID, null);
                    int freezedAt = 0;
                    if (!int.TryParse(storage, out freezedAt))
                    {
                        Log.Error($"Couldn't parse time {storage}");
                        continue;
                    }

                    var gi = x.GridInfo();
                    if (gi == null)
                    {
                        continue;
                    }



                    timepassed = (SharpUtils.timeStamp() - freezedAt) * 1000f;

                    if (gi.blocks.ContainsKey(ShipSubpart.WIND_TURBINE))
                    {
                        foreach (var y in gi.blocks[ShipSubpart.WIND_TURBINE])
                        {
                            var w = y as MyWindTurbine;
                            generated += w.SourceComp.CurrentOutput * windMlt * timepassed;
                        }
                    }


                    if (gi.blocks.ContainsKey(ShipSubpart.SOLAR_PANEL))
                    {
                        foreach (var y in gi.blocks[ShipSubpart.SOLAR_PANEL])
                        {
                            var w = y as MySolarPanel;
                            generated += w.SourceComp.CurrentOutput * solarMlt * timepassed;
                        }
                    }



                    if (gi.blocks.ContainsKey(ShipSubpart.BATTERY))
                    {
                        foreach (var y in gi.blocks[ShipSubpart.BATTERY])
                        {
                            batts.Add(y as MyBatteryBlock);
                        }
                    }


                }

                batts.Sort((a, b) =>
                {
                    var d = (b.MaxStoredPower - b.CurrentStoredPower) - (a.MaxStoredPower - a.CurrentStoredPower);
                    return d > 0 ? 1 : d < 0 ? -1 : 0;
                });

                foreach (var x in batts)
                {
                    if (generated > 0)
                    {
                        var canStore = (x.MaxStoredPower - x.CurrentStoredPower) * 3600 * 1.25f;
                        var charge = Math.Min(canStore, generated);
                        generated -= charge;

                        StorePower.Invoke(x, new object[] { 1000, charge });
                    }
                }
            }
            catch (Exception e)
            {
                Log.Fatal(e.ToString());
            }

        }
    }
}
