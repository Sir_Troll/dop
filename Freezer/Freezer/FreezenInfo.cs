using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.World;
using Slime.Features;
using TorchPlugin;
using VRage.Game.Entity;
using VRage.Game.Entity.EntityComponents.Interfaces;
using VRageMath;

namespace Slime
{
    public class FrozenInfo
    {
        private static FieldInfo cryoOccupied = typeof(MyCryoChamber).GetField("m_currentPlayerId", BindingFlags.NonPublic | BindingFlags.Instance);
        public static double CalculteRating(List<MyCubeGrid> Grids)
        {
            var r = 0.0d;
            foreach (var x in Grids)
            {
                r += x.BlocksPCU + (x.IsStatic ? 1000 : 0);
            }
            //Rating = Rating * (0.8+(Grids.Count * 0.2f));
            return r;
        }

        public long Id { get; }
        public bool IsConcealed { get; private set; }
        public BoundingBoxD WorldAABB { get; private set; }
        public List<MyCubeGrid> Grids { get; }


        public readonly int Rating = 0;
        public event Action<FrozenInfo> Closing;
        internal volatile int ProxyId = -1;
        public HashSet<long> disabledProjectors;

        public FrozenInfo(long Id, List<MyCubeGrid> Grids, BoundingBoxD worldAABB)
        {
            this.Id = Id;
            this.Grids = Grids;
            this.WorldAABB = worldAABB;
            this.Rating = (int)CalculteRating(Grids);
        }


        public void StopGrids()
        {
            foreach (var x in Grids)
            {
                if (!x.IsStatic)
                {
                    x.Physics?.SetSpeeds(Vector3.Zero, Vector3.Zero);
                }
            }
        }

        public bool ShouldSkipFreeze(bool ConcealPirates, bool ConcealProduction, float dontFreezeGravity, int dontFreezeProductionMaximumPCU)
        {

            if (dontFreezeGravity > 0 && Grids[0].Physics != null)
            {
                var g = Grids[0].Physics.Gravity.Length();
                if (g > 0 && g < dontFreezeGravity)
                {
                    return true;
                }
            }

            if (!ConcealPirates)
            {
                var pirateId = MyPirateAntennas.GetPiratesId();
                foreach (var grid in Grids)
                {
                    if (grid.BigOwners.Contains(pirateId)) { return true; }
                }
            }

            bool hasProduction = false;
            foreach (var grid in Grids)
            {
                var tt = grid.GridInfo();
                if (tt == null)
                {
                    Log.LogSpamming((27345723), (builder, i) => builder.Append("GridInfo is null:").Append(grid));
                    return true; // WRONG STATE?
                }

                if (tt.IsExcluded())
                {
                    return true;
                }
                //if (tt.HasWorkingProduction())
                if (!hasProduction)
                {
                    hasProduction = tt.HasWorkingProduction();
                    if (hasProduction && !ConcealProduction) { return true; }
                }
            }

            if (hasProduction && Rating <= dontFreezeProductionMaximumPCU)
            {
                return true;
            }
            return false;
        }

        public void UpdatePostConceal()
        {
            IsConcealed = true;
            UpdateAABB();
            HookOnClosing();
        }

        public void UpdatePostReveal()
        {
            IsConcealed = false;
            UnhookOnClosing();
        }

        private void HookOnClosing()
        {
            foreach (var grid in Grids) grid.OnMarkForClose += Grid_OnMarkForClose;
        }

        private void UnhookOnClosing()
        {
            foreach (var grid in Grids) grid.OnMarkForClose -= Grid_OnMarkForClose;
        }

        private void Grid_OnMarkForClose(MyEntity obj)
        {
            EnableProjectors();
            UnhookOnClosing();
            Closing?.Invoke(this);
        }

        public void UpdateAABB()
        {
            var startPos = Grids.First().PositionComp.GetPosition();
            var box = new BoundingBoxD(startPos, startPos);

            foreach (var aabb in Grids.Select(g => g.PositionComp.WorldAABB)) box.Include(aabb);

            WorldAABB = box;
        }

        public bool IsCryoOccupied(ulong steamId)
        {
            try
            {
                foreach (var g in Grids)
                {
                    var gi = g.GridInfo();
                    foreach (var c in gi.blocks[ShipSubpart.CRYO])
                    {
                        if (c == null) continue;
                        var value = (MyPlayer.PlayerId?)cryoOccupied.GetValue(c);
                        if (value?.SteamId == steamId) return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                Log.LogSpamming(472766168, (x, a) => x.Append("IsCryoOccupiedError:" + e));
                return false;
            }

        }

        private void DisableProjectors(MyCubeGrid grid)
        {
            foreach (var projector in grid.GetFatBlocks<MyProjectorBase>())
            {
                if (projector.ProjectedGrid == null) continue;

                projector.Enabled = false;
                if (disabledProjectors == null)
                {
                    disabledProjectors = new HashSet<long>();
                }
                disabledProjectors.Add(projector.EntityId);
            }
        }

        public void EnableProjectors()
        {
            if (disabledProjectors == null) return;
            foreach (var projector in disabledProjectors.Select(x => MyEntities.GetEntityById(x) as MyProjectorBase))
            {
                if (projector == null) continue;
                projector.Enabled = true;
            }

            disabledProjectors.Clear();
        }

        public void Conceal()
        {
            foreach (var grid in Grids)
            {
                DisableProjectors(grid);
                if (grid.Parent == null) UnregisterRecursive(grid);
            }

            FreezerEnergyFix.OnFreeze(Grids);

            if (FreezerPlugin.freezer.Config.ConvertToStations)
            {
                Grids.Sort((a, b) => b.BlocksCount - a.BlocksCount);
                FreezerExtra.LockGridGroup(Grids.First());
            }
            else if (FreezerPlugin.freezer.Config.StopGrids)
            {
                StopGrids();
            }
        }

        public void Reveal()
        {
            foreach (var grid in Grids)
            {
                if (grid.Parent == null) RegisterRecursive(grid);
            }
            FreezerEnergyFix.OnUnFreeze(Grids);
            

            if (FreezerPlugin.freezer.Config.ConvertToStations)
            {
                FreezerExtra.UnlockGrid(Grids.First());
            }

            EnableProjectors();
        }


        private void UnregisterRecursive(MyEntity e)
        {
            if (e.IsPreview) return;

            MyEntities.UnregisterForUpdate(e);
            (e.GameLogic as IMyGameLogicComponent)?.UnregisterForUpdate();
            e.setFrozen();
            if (e.Hierarchy == null) return;

            foreach (var child in e.Hierarchy.Children) UnregisterRecursive((MyEntity)child.Container.Entity);
        }

        private void RegisterRecursive(MyEntity e)
        {
            if (e.IsPreview) return;

            MyEntities.RegisterForUpdate(e);
            (e.GameLogic as IMyGameLogicComponent)?.RegisterForUpdate();
            e.setUnFrozen();
            if (e.Hierarchy == null) return;

            foreach (var child in e.Hierarchy.Children) RegisterRecursive((MyEntity)child.Container.Entity);
        }
    }
}

/*foreach (var block in Grids.SelectMany(x => x.GetFatBlocks())) {
    //var sn = block.BlockDefinition.Id.SubtypeName;
    //if (!string.IsNullOrEmpty(sn)) {
    //    if (exSubs.Length > 0 && exSubs.Contains(sn)) return true;
    //    if (exWorkingSubs.Length > 0 && exWorkingSubs.Contains(sn)) {
    //        if (block is IMyFunctionalBlock && block.IsWorking) {
    //            return true;
    //        }
    //    }
    //}
    
    if (block is IMyProductionBlock prod) {
        hasProduction = true;
        if (!ConcealProduction && prod.IsProducing) return true;
    } else if (prepareForConceal) {
        //if (block is MyCryoChamber cryo) CryoChambers.Add(cryo);
        //else if (block is MyMotorBase) {
        //    if (block is MyMotorSuspension) {
        //        Wheels.Add(block as MyMotorSuspension);
        //    } else {
        //        Rotors.Add(block as MyMotorBase);
        //    }
        //} else if (block is MyPistonBase) {
        //    Pistons.Add(block as MyPistonBase);
        //}
    }
}*/