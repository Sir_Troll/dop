﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NAPI;
using NLog;
using Sandbox.Engine.Multiplayer;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.Multiplayer;
using Sandbox.ModAPI;
using Slime.GUI;
using Slime.Z;
using Torch;
using Torch.API;
using Torch.Managers;
using TorchPlugin;
using VRage.Game;
using VRage.Game.Components;
using VRage.ModAPI;
using VRage.Utils;
using VRageMath;

namespace Slime
{
    public sealed class Freezer : Action1<long>
    {
        public Persistent<FreezerConfig> Settings { get; private set; }
        public FreezerConfig Config { get => Settings.Data; }
        public Dictionary<long, FrozenInfo> Concealed { get; } = new Dictionary<long, FrozenInfo>();

        private ulong _counter;
        private bool _init;
        private readonly List<FrozenInfo> buffer = new List<FrozenInfo>();
        private MyDynamicAABBTreeD _concealedAabbTree;
        private bool _settingsChanged;
        private bool _ready;
        private bool _wasready = false;

        private ITorchBase Torch;

        public void Init(ITorchBase torch, string StoragePath)
        {
            this.Torch = torch;
            try
            {
                Settings = Persistent<FreezerConfig>.Load(Path.Combine(StoragePath, "MIG-Freezer.cfg"));
            }
            catch (Exception e)
            {
                Log.Error(e);
            }

            if (Settings?.Data == null) Settings = new Persistent<FreezerConfig>(Path.Combine(StoragePath, "MIG-Freezer.cfg"), new FreezerConfig());
            Settings.Data.PropertyChanged += Data_PropertyChanged;
            _concealedAabbTree = new MyDynamicAABBTreeD(MyConstants.GAME_PRUNING_STRUCTURE_AABB_EXTENSION);
        }

        public void Dispose(string StoragePath)
        {
            Settings.Save(Path.Combine(StoragePath, "MIG-Freezer.cfg"));
        }

        private void Data_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            _settingsChanged = true;
        }

        int target = 0;
        int targetStep = 0;

        //TODO: divide conceal/reveal runs over several ticks to avoid stuttering.

        public void Target(int target, int targetStep)
        {
            this.target = target;
            this.targetStep = targetStep;
        }



        public void run(long frame)
        {
            if (MyAPIGateway.Session == null || !Config.Enabled) return;

            Init();

            if (_ready && !_wasready)
            {
                _wasready = true;
                if (Config.FreezeAllOnStart)
                {
                    ConcealGrids();
                }
            }

            if (_ready)
            {
                if (Config.LoadBalancer)
                {
                    if (_counter % (ulong)Config.UnFreezeInterval == 0) RevealGrids(Config.UnFreezeDistance);

                    UpdateTarget();

                    if (target < 0)
                    {
                        target = Math.Min(target + targetStep, 0);
                        ConcealGrids(Config.FreezeDistance, targetStep);
                    }
                    else if (target > 0)
                    {
                        target = Math.Max(target - targetStep, 0);
                        RevealAll(targetStep);
                    }
                }
                else
                {
                    if (_counter % (ulong)Config.FreezeInterval == 0) ConcealGrids(Config.FreezeDistance, Config.BatchAmount);
                    if (_counter % (ulong)Config.UnFreezeInterval == 1) RevealGrids(Config.UnFreezeDistance);
                }

                _counter += 1;

                UpdateGui();
            }
        }


		public void UpdateGui ()
		{
			if (_counter % 300 == 0)
			{
				try
				{
					var frozen = Concealed.Count;
					var total = MyCubeGridGroups.Static.Physical.Groups.Count;
					FreezerPlugin.instance.UpdateUI((x) => {
						var gui = x as FreezerGUI;
						gui.FreezerInfo.Text = $"Frozen: {frozen} / {total} | {FreezerPlugin.simTracker.avrg:N2}% | Quality={FreezerPlugin.simTracker.Quality}) | Phys: {FreezerPlugin.simTracker.avrgPhysics:N2} | NET Memory: {MemoryTracker.lastGC / 1024 / 1024} / {MemoryTracker.now/1024/1024} mb, GCCounter: {MemoryTracker.GCCounter}, Mb/S = {60*MemoryTracker.memoryPerTick/1024} kb";
					});
				}
				catch (Exception e) { Log.Error(e, "WTF?"); }
			}
		}

        public void UpdateTarget()
        {
            if (FreezerPlugin.simTracker.refreshed)
            {
                switch (FreezerPlugin.simTracker.Quality)
                {
                    case 5:
                        Target(28, 28);
                        break;
                    case 4:
                        Target(20, 20);
                        break;
                    case 3:
                        Target(12, 12);
                        break;
                    case 2:
                        Target(8, 8);
                        break;
                    case 1:
                        Target(4, 4);
                        break;
                    case 0:
                        Target(0, 0);
                        break;
                    case -1:
                        Target(-8, 8);
                        break;
                    case -2:
                        Target(-20, 20);
                        break;
                    case -3:
                        Target(-30, 30);
                        break;
                    case -4:
                        Target(-50, 25);
                        break;
                    case -5:
                        Target(-80, 99999);
                        break;
                }
            }
        }

        public void Init()
        {
            if (_init || MyAPIGateway.TerminalControls == null) return;
            Log.Warn("Freezer will work in " + Settings.Data.WorkDelayMs + " ms");
            var delayTimer = new System.Timers.Timer { AutoReset = false, Interval = Settings.Data.WorkDelayMs };
            delayTimer.Elapsed += (sender, args) =>
            {
                Log.Warn("Freezer started work");
                _ready = true;
            };
            delayTimer.Start();

            //MyMultiplayer.Static.ClientJoined += RevealCryoPod;

            _init = true;
        }

        private void RevealCryoPod(ulong steamId)
        {
            try
            {
                Torch.Invoke(() =>
                {
                    try
                    {
                        var list = new List<FrozenInfo>(Concealed.Values);
                        for (var i = list.Count - 1; i >= 0; i--)
                        {
                            var group = list[i];
                            if (group.IsCryoOccupied(steamId))
                            {
                                RevealGroup(group);
                                return;
                            }
                        }
                    }
                    catch (Exception e) { Log.LogSpamming(27743994, (x, i) => x.Append("RevealCryoPod Exception:").Append(e)); }
                });
            }
            catch (Exception e)
            {
                Log.LogSpamming(27742994, (x, i) => x.Append("RevealCryoPod2 Exception:").Append(e));
            }
        }


        private int Freeze(FrozenInfo group)
        {
            if (Concealed.ContainsKey(group.Id)) return 0;
            group.Conceal();
            //group.UpdateAABB();
            var aabb = group.WorldAABB;

            group.ProxyId = _concealedAabbTree.AddProxy(ref aabb, group, 0);
            group.Closing += Group_Closing;
            group.UpdatePostConceal();

            Concealed.Add(group.Id, group);
            return group.Grids.Count;
        }

        private void Group_Closing(FrozenInfo group) { RevealGroup(group); }

        public int RevealGroup(FrozenInfo group)
        {
            if (!group.IsConcealed) { return 0; }

            var count = group.Grids.Count;
            group.Reveal();

            Concealed.Remove(group.Id);
            _concealedAabbTree.RemoveProxy(group.ProxyId);
            group.UpdatePostReveal();
            return count;
        }

        public int RevealGrids(double distanceFromPlayers)
        {
            var revealed = 0;
            var playerSpheres = GetPlayerViewSpheres(distanceFromPlayers);
            foreach (var sphere in playerSpheres)
            {
                revealed += RevealGridsInSphere(sphere);
            }

            if (_settingsChanged)
            {
                var list = new List<FrozenInfo>(Concealed.Values);
                var ss = Settings.Data;
                for (var i = list.Count - 1; i >= 0; i--)
                {
                    if (list[i].ShouldSkipFreeze(ss.FreezePirates, ss.FreezeProduction, ss.DontFreezeInGravity, ss.DontFreezeProductionMaximumPCU)) revealed += RevealGroup(list[i]);
                }
                _settingsChanged = false;
            }

            //if (revealed != 0) Log.Info($"Revealed {revealed} grids near players.");
            return revealed;
        }

        public int RevealGridsInSphere(BoundingSphereD sphere)
        {
            var revealed = 0;
            _concealedAabbTree.OverlapAllBoundingSphere(ref sphere, buffer);
            foreach (var group in buffer) revealed += RevealGroup(group);
            buffer.Clear();
            return revealed;
        }

        public int ConcealGrids(double distanceFromPlayers = 0, int maxAmount = 0)
        {

            var groups = TargetConcealGrids(distanceFromPlayers, maxAmount);
            int concealed = 0;

            var x = new Stopwatch();
            x.Start();
            foreach (var group in groups) { concealed += Freeze(group); }
            //DOP.Log.Error("Freezer conceal took:" + x.Elapsed.TotalMilliseconds);
            x.Stop();
            //if (concealed > 0) Log.Warn("Freezed grids : " + concealed);
            return concealed;
        }

        public int RevealAll(int maxAmount = 9999)
        {
            var revealed = 0;
            var list = new List<FrozenInfo>(Concealed.Values);

            var max = Math.Min(list.Count, maxAmount);

            if (Settings.Data.UseUnfreezePriority)
            {
                list.Sort((a, b) => (a.Rating > b.Rating ? 1 : a.Rating == b.Rating ? 0 : -1));
            }
            else
            {
                list.ShuffleList(); //Randomize
            }

            for (var i = 0; i < max; i++)
            {
                revealed += RevealGroup(list[i]);
            }
            //if (revealed > 0) Log.Warn("Unfreezed grid-groups: " + revealed);
            return revealed;
        }

        public IEnumerable<FrozenInfo> TargetConcealGrids(double distanceFromPlayers = 0, int maxAmount = 0)
        {
            if (Settings.Data.Profiling)
            {
                var sw = Stopwatch.StartNew();
                var result = TargetConcealGridsInternal(distanceFromPlayers, maxAmount);
                var end = sw.Elapsed.TotalMilliseconds;
                if (end > 1d) Log.Error("Find targets to Freeze : [UsePriority:" + Settings.Data.UseFreezePriority + " MaxAmount:" + maxAmount + "] Took:" + end);
                return result;
            }
            return TargetConcealGridsInternal(distanceFromPlayers, maxAmount);
        }

        private IEnumerable<FrozenInfo> TargetConcealGridsInternal(double distanceFromPlayers = 0, int maxAmount = 0)
        {
            //Measure(distanceFromPlayers, maxAmount);
            var playerSpheres = GetPlayerViewSpheres(distanceFromPlayers);
            if (!Settings.Data.UseFreezePriority)
            {
                ConcurrentBag<FrozenInfo> groups = new ConcurrentBag<FrozenInfo>();
                Parallel.ForEach(MyCubeGridGroups.Static.Physical.Groups, group =>
                {
                    if (maxAmount != 0 && groups.Count > maxAmount) return;

                    var Grids = group.Nodes.Select(n => n.NodeData).ToList();
                    var Id = Grids.First().EntityId;
                    if (Concealed.ContainsKey(Id)) return; //already consealed
                    var aabb = group.GetWorldAABB();
                    if (playerSpheres.Any(s => s.Contains(aabb) != ContainmentType.Disjoint)) return;

                    var concealGroup = new FrozenInfo(Id, Grids, aabb);
                    if (concealGroup.ShouldSkipFreeze(Settings.Data.FreezePirates, Settings.Data.FreezeProduction, Config.DontFreezeInGravity, Config.DontFreezeProductionMaximumPCU)) return;
                    groups.Add(concealGroup);
                });
                return groups;
            }
            else
            {
                var query = MyCubeGridGroups.Static.Physical.Groups.AsParallel()
                                            .Select((x) =>
                                            {
                                                var Grids = x.Nodes.Select(n => n.NodeData).ToList();
                                                var Id = Grids.First().EntityId;
                                                if (Concealed.ContainsKey(Id)) return null;
                                                var aabb = x.GetWorldAABB();
                                                if (playerSpheres.Any(s => s.Contains(aabb) != ContainmentType.Disjoint)) return null;
                                                return new FrozenInfo(Id, Grids, aabb);
                                            })
                                            .Where((x) => x != null)
                                            .OrderBy((x) => x.Rating)
                                            .Where((x) => !x.ShouldSkipFreeze(Settings.Data.FreezePirates, Settings.Data.FreezeProduction, Config.DontFreezeInGravity, Config.DontFreezeProductionMaximumPCU));

                if (maxAmount != 0) { query = query.Take(maxAmount); }
                return query.ToList();
            }
        }


        private List<BoundingSphereD> GetPlayerViewSpheres(double distance) { return ((MyPlayerCollection)MyAPIGateway.Multiplayer.Players).GetOnlinePlayers().Select(p => new BoundingSphereD(p.GetPosition(), distance)).ToList(); }
    }

    public static class Extensions
    {
        public static MyModStorageComponentBase GetStorage(this IMyEntity entity) { return entity.Storage = entity.Storage ?? new MyModStorageComponent(); }
    }
}