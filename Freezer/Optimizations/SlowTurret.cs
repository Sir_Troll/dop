﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using NAPI;
using ParallelTasks;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Weapons;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Ingame;
using SpaceEngineers.Game.ModAPI;
using TorchPlugin;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRageMath;
using IMyBlockGroup = Sandbox.ModAPI.IMyBlockGroup;
using IMyFunctionalBlock = Sandbox.ModAPI.IMyFunctionalBlock;
using IMyGridTerminalSystem = Sandbox.ModAPI.IMyGridTerminalSystem;
using IMyProgrammableBlock = Sandbox.ModAPI.IMyProgrammableBlock;

namespace Slime.Features {
    public class SlowTurrets {
        private static Guid guid = new Guid("0c5aacc4-0024-4fad-adc9-57ed8c27a343");
        private static Regex extractRegex = new Regex("\\[ARG:([\\w]*)\\]");

        public static void Init ()
        {
            Ext2.RegisterGuids(new List<Guid>() { guid });
        }

        private ShipSubpart gridInfo;
        private BoundingSphereD box = new BoundingSphereD(new Vector3D(), 3000);
        public MyCubeGrid grid => gridInfo.grid;
        public static Freezer Plugin => FreezerPlugin.freezer;
        private static readonly Random Random = new Random();
        
        private AutoTimer timer = new AutoTimer(Plugin.Settings.Data.EnabledSlowdownTurretTurnOffInterval, Random.Next(Plugin.Settings.Data.EnabledSlowdownTurretTurnOffInterval));

        public SlowTurrets(ShipSubpart gridInfo) { this.gridInfo = gridInfo; }
        
        public void Tick() {
            if (gridInfo.grid.isFrozen()) return;
            if (!Plugin.Settings.Data.EnabledSlowdownTurretTurnOff) return;
            if (gridInfo.blocks.Count == 0) return;
            
            if (timer.tick()) {
                scanEnemies();
            }
        }
        
        public void scanEnemies() {
            box.Center = (gridInfo.grid as IMyCubeGrid).WorldAABB.Center;
            box.Radius = Plugin.Settings.Data.EnabledSlowdownTurretTurnOffRadius;

            var connected = MyAPIGateway.GridGroups.GetGroup(gridInfo.grid, GridLinkTypeEnum.Physical);
            foreach (var x in connected) {
                var tt = (x as MyCubeGrid).GridInfo();
                if (tt != null) {
                    tt.slowTurrets.timer.reset();
                }
            }
            timer.reset();
            MyAPIGateway.Parallel.Start(Searcher.DoScan, AfterScan, new ScanWorkData(gridInfo.grid, box, Plugin.Settings.Data.EnabledSlowdownTurretTurnOff));
        }

        static class Searcher {
            public static void DoScan(WorkData workData) {
                try {
                    var data = workData as ScanWorkData;
                    if (data == null || data.grid == null || data.grid.MarkedForClose || data.grid.Closed) return;
                    if (data.sphere != null) {
                        var sp = (BoundingSphereD) data.sphere;
                        data.targets = new List<MyEntity>();
                        MyGamePruningStructure.GetAllEntitiesInSphere(ref sp, data.targets);
                    }

                    var filtered = GetEnemies(data);
                    lock (data) { data.filtered = filtered; }
                    if (data.activate) {
                        var blocksToActivate = Search(data, filtered.Count > 0);
                        lock (data) { data.blocksToActivate = blocksToActivate; }
                    }
                } catch (Exception e) {
                    Log.Fatal(e, "DoScanTurrets");
                }
            }
        
            private static List<MyEntity> GetEnemies(ScanWorkData data) {
                var filtered = new List<MyEntity>();
                foreach (var x in data.targets) {
                    var block = x as MyCubeBlock;
                    if (block != null) {
                        if (block.OwnerId != 0) {
                            try {
                                if (data.grid.GetUserRelation(block.OwnerId).AsNumber() < 1) {
                                    filtered.Add(block);
                                    break;
                                }
                            } catch (Exception e) {
                                Log.LogSpamming(266326463, (builder, i) => builder.Append("Error at GetEnemies: " + x.EntityId + " : " + x +" : "+ e));
                            }
                        }
                    } else {
                        var ch = x as MyCharacter;
                        if (ch != null) {
                            try {
                                if (data.grid.isEnemy(ch.GetPlayerIdentityId())) {
                                    filtered.Add(ch);
                                    break;
                                }
                            } catch (Exception e) {
                                Log.LogSpamming(266326462, (builder, i) => builder.Append("Error at GetEnemies: " + x.EntityId + " : " + x +" : "+ e));
                            }
                            
                        }
                    }
                }

                return filtered;
            }
        
            private static List<IMyFunctionalBlock> Search(ScanWorkData data, bool found) {
                var blockGroups = new List<IMyBlockGroup>();
                var blocksToActivate = new List<IMyFunctionalBlock>();
                
                var terminalSystem = (data.grid.GridSystems.TerminalSystem as IMyGridTerminalSystem);
                if (terminalSystem == null) {
                    Log.LogSpamming(266326461, (builder, i) => builder.Append("TerminalSystem is null: "+ data.grid));
                }

                try {
                    terminalSystem.GetBlockGroups(blockGroups);
                    foreach (var x in blockGroups) {
                        if (!found && x.Name == "[SAFE]") {
                            x.GetBlocksOfType(blocksToActivate);
                        } else if (found && x.Name == "[DANGER]") {
                            x.GetBlocksOfType(blocksToActivate);
                        }
                    }
                } catch (Exception e) {
                    Log.LogSpamming(266326460, (builder, i) => builder.Append("TerminalSystem is null: "+ data.grid));
                }

                return blocksToActivate;
            }
        }

        public static void AfterScan(WorkData workData) {
            try {
                var data = workData as ScanWorkData;
                if (data == null) return;

                lock (data) {
                    var tt = SlowLogic.refsShips.GetValueOrDefault(data.grid, null) as ShipSubpart;
                    tt?.slowTurrets.onScanComplete(data);
                }
            } catch (Exception e) {
                Log.LogSpamming(34266772, (builder, i) => builder.Append("Slow turrets:").Append(e));
            }
        }
        
        public void onScanComplete(ScanWorkData data) {
            if (data.filtered == null) { return; }
            //DOP.Log.Error("onScanComplete");
            
            var enemies = data.filtered;
            var danger = enemies.Count > 0;

            if (Plugin.Settings.Data.Profiling) {
                var sw = new Stopwatch();
                sw.Start();
                ReactOnEnemiesChanged(danger);
                var passed = sw.Elapsed;
                sw.Stop();
                if (passed.TotalMilliseconds > 1d) {
                    Log.Error("TurretTurnOn/Off time:" + passed.TotalMilliseconds + " ms: Grid:" + grid.DisplayName + " at:" + grid.WorldMatrix.Translation);
                }
            } else {
                ReactOnEnemiesChanged(danger);
            }
            
            if (data.blocksToActivate != null && data.blocksToActivate.Count > 0) {
                if (Plugin.Settings.Data.EnabledSlowdownTurretSendSignal) {
                    if (Plugin.Settings.Data.Profiling) {
                        var sw =new Stopwatch();
                        sw.Start();
                        foreach (var x in data.blocksToActivate) {
                            if (danger) Arm(x); else Disarm(x);
                        }
                        var passed = sw.Elapsed;
                        if (passed.TotalMilliseconds > 1d) {
                            Log.Error("TurretActivation:" + passed.TotalMilliseconds + " ms Grid:" + grid.DisplayName + " at:" + grid.WorldMatrix.Translation);
                        }
                        sw.Stop();
                        sw.Reset();
                    } else {
                        foreach (var x in data.blocksToActivate) {
                            if (danger) Arm(x); else Disarm(x);
                        }
                    }
                }
            }
        }

        public void ReactOnEnemiesChanged(bool haveEnemies) {
            var connected = MyAPIGateway.GridGroups.GetGroup(grid, GridLinkTypeEnum.Physical);
            //TODO Check if ME here
            foreach (var x in connected) {
                var tt = (x as MyCubeGrid).GridInfo();
                tt?.slowTurrets.turnTurrets(haveEnemies);
            }
        }

        public void Arm(IMyFunctionalBlock y) {
            if (!y.Enabled) {
                y.Enabled = true;
                var timer = y as IMyTimerBlock;
                if (timer != null) {
                    timer.Trigger();
                } else {
                    var prog = y as IMyProgrammableBlock;
                    if (prog != null) {
                        var data = ExtractArg(y.CustomName, extractRegex) ?? "danger";
                        prog.Run(data, UpdateType.Terminal);
                    }
                }
            }
        }
        
        public void Disarm(IMyFunctionalBlock y) {
            if (y.Enabled) {
                var timer = y as IMyTimerBlock;
                if (timer != null) {
                    timer.Trigger();
                } else {
                    var prog = y as IMyProgrammableBlock;
                    if (prog != null) {
                        var data = ExtractArg(y.CustomName, extractRegex) ?? "safe";
                        prog.Run(data, UpdateType.Terminal);
                    }
                }

                y.Enabled = false;
            }
        }
        
        public static String ExtractArg (String str, Regex r) {
            foreach (Match m in r.Matches (str)) {
                if (m.Groups.Count > 2) { return m.Groups[1].Value; }
            }
            return null;
        }
        
        
        private void XOnMarkForClose(MyEntity obj) {
            //DOP.Log.Error("XOnMarkForClose:");
            (obj as MyFunctionalBlock).EnabledChanged -= XOnEnabledChanged;
            obj.OnMarkForClose -= XOnMarkForClose;
        }

        
        private void XOnEnabledChanged(MyTerminalBlock obj) {
            //DOP.Log.Error("OnEnabledChanged:" + obj.isFrozen());
            if (obj.isFrozen()) return;
            //DOP.Log.Error("OnEnabledChanged: by user");
            obj.SetStorageData(guid, null);
        }

        private static void setSilentEnabled(MyLargeTurretBase turr, bool enabled) {
            turr.setFrozen();
            //DOP.Log.Error("setSilentEnabled:"+enabled);
            turr.Enabled = enabled;
            turr.setUnFrozen();
        }

        
        public void turnTurrets(bool t) {
            if (Plugin.Settings.Data.EnabledSlowdownTurretSaveState) {
                foreach (var turr in gridInfo.turrets) {
                    if (t == turr.Enabled) continue;
                    if (!t) {
                        turr.SetStorageData(guid, "1");
                        setSilentEnabled(turr, false);
                    } else {
                        var d = turr.GetAndSetStorageData(guid, null);
                        if (d == "1") { setSilentEnabled(turr, true); }
                    }
                }
            } else {
                foreach (var turr in gridInfo.turrets) { turr.Enabled = t; }
            }
        }

        public void OnTurretAdded(MyLargeTurretBase bb) {
            bb.EnabledChanged += XOnEnabledChanged;
            bb.OnMarkForClose += XOnMarkForClose;
            
        }
    }
    
    public class ScanWorkData : WorkData {
        public MyCubeGrid grid;
        public BoundingSphereD? sphere;
        public bool activate;
            
        public List<MyEntity> targets;
        public List<MyEntity> filtered;
        public List<IMyFunctionalBlock> blocksToActivate;

        public ScanWorkData(MyCubeGrid grid, BoundingSphereD? sphere, bool activate) {
            this.sphere = sphere;
            this.grid = grid;
            this.activate = activate;
        }
    }
}