using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Weapons;
using Sandbox.ModAPI;
using SpaceEngineers.Game.Entities.Blocks;
using SpaceEngineers.Game.Weapons.Guns;

namespace Slime.Features
{
    //9e333c09-e4c3-4aef-bd7b-36cfdf983ce2
    public class ShipSubpart : SlowdownShip
    {
        public SlowTurrets slowTurrets;
        public HashSet<MyLargeTurretBase> turrets = new HashSet<MyLargeTurretBase>();
        public static Type ASSEMBLER = typeof(MyAssembler);
        public static Type REFINERY = typeof(MyRefinery);

        public static Type WIND_TURBINE = typeof(MyWindTurbine);
        public static Type SOLAR_PANEL = typeof(MySolarPanel);
        public static Type BATTERY = typeof(MyBatteryBlock);

        public static Type GATLING_TURRET = typeof(MyLargeGatlingTurret);
        public static Type MISSLE_TURRET = typeof(MyLargeMissileTurret);

        public static Type CRYO = typeof(MyCryoChamber);


        private static Stopwatch sw = new Stopwatch();

        public HashSet<MyCubeBlock> excluded = new HashSet<MyCubeBlock>();
        public HashSet<IMyFunctionalBlock> excludedWorking = new HashSet<IMyFunctionalBlock>();


        public Dictionary<Type, HashSet<MyCubeBlock>> blocks = new Dictionary<Type, HashSet<MyCubeBlock>>();

        public ShipSubpart(MyCubeGrid grid) : base(grid)
        {

            slowTurrets = new SlowTurrets(this);
            HashSet<MyCubeBlock> _excluded = new HashSet<MyCubeBlock>();
            HashSet<IMyFunctionalBlock> _excludedWorking = new HashSet<IMyFunctionalBlock>();
            Dictionary<Type, HashSet<MyCubeBlock>> _dict = new Dictionary<Type, HashSet<MyCubeBlock>>();

            Parallel.ForEach(grid.GetBlocks(), xx =>
            {
                try
                {
                    var fat = xx.FatBlock;
                    var sn = xx.BlockDefinition.Id.SubtypeName;
                    if (fat != null)
                    {
                        lock (_dict)
                        {
                            var t = fat.GetType();
                            HashSet<MyCubeBlock> set;
                            if (!_dict.ContainsKey(t))
                            {
                                set = new HashSet<MyCubeBlock>();
                                _dict.Add(t, set);
                            }
                            else
                            {
                                set = _dict[t];
                            }
                            set.Add(fat);
                        }

                        if (FreezerCache.ExcludedSubtypesSet.Contains(sn))
                        {
                            lock (excluded)
                            {
                                excluded.Add(fat);
                            }
                        }

                        var fu = fat as IMyFunctionalBlock;
                        if (fu != null && FreezerCache.ExcludedWorkingSubtypesSet.Contains(sn))
                        {
                            lock (excludedWorking)
                            {
                                excludedWorking.Add(fu);
                            }
                        }
                    }
                }
                catch (Exception e) { Log.Fatal(e, "SlowGrid" + xx + " " + xx.BlockDefinition.Id.SubtypeName); }
            });

            this.excludedWorking = _excludedWorking;
            this.excluded = _excluded;
            this.blocks = _dict;
        }


        public override void Hack() { /** DO NOTHING **/ }

        public void BlockAddedOrRemoved(MyCubeBlock block, bool added)
        {

            if (block is MyLargeTurretBase)
            {
                TurretAddedOrRemoved((MyLargeTurretBase) block, added);
            }
            
            var t = block.GetType();
            if (blocks.ContainsKey(t))
            {
                var set = blocks[t];
                if (added)
                {
                    set.Add(block);
                }
                else
                {
                    set.Remove(block);
                }
            }
            else
            {
                if (added)
                {
                    var set = new HashSet<MyCubeBlock>();
                    set.Add(block);
                    blocks.Add(t, set);
                }
            }

            var sn = block.BlockDefinition.Id.SubtypeName;
            if (FreezerCache.ExcludedSubtypesSet.Contains(sn))
            {
                lock (excluded)
                {
                    excluded.AddOrRemove(block, added);
                }
            }

            var fu = block as IMyFunctionalBlock;
            if (fu != null && FreezerCache.ExcludedWorkingSubtypesSet.Contains(sn))
            {
                lock (excludedWorking)
                {
                    excludedWorking.AddOrRemove(fu, added);
                }
            }
        }

        private void TurretAddedOrRemoved(MyLargeTurretBase block, bool added)
        {
            if (added)
            {
                slowTurrets.OnTurretAdded( block);
                turrets.Add(block);
                return;
            }
            turrets.Remove(block);
        }


        public bool HasWorkingProduction()
        {
            if (blocks.ContainsKey(ASSEMBLER))
            {
                foreach (var pp in blocks[ASSEMBLER])
                {
                    if ((pp as MyAssembler).IsProducing) { return true; }
                }
            }

            if (blocks.ContainsKey(REFINERY)
            {
                foreach (var pp in blocks[REFINERY])
                {
                    if ((pp as MyRefinery).IsProducing) { return true; }
                }
            }

            return false;
        }

        public bool IsExcluded()
        {
            if (excluded.Count > 0) return true;
            foreach (var x in excludedWorking)
            {
                if (x.IsWorking) { return true; }
            }

            return false;
        }

        public override void Tick()
        {
            slowTurrets.Tick();
        }
    }
}
