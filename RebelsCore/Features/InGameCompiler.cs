﻿using System;
using System.Linq;
using System.Reflection;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.IO;
using System.Text;
using System.Runtime.CompilerServices;
using NAPI;

namespace Slime.Features.Commands
{
	class InGameCompiler {
        static string LIBS = @"

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities;
using Sandbox.Game.World;
using Sandbox.Game.EntityComponents;
using Sandbox.Game;
using Sandbox.ModAPI;
using Sandbox.ModAPI;
using Sandbox.Game.Entities.Character;

using VRage.Game;
using VRage.Game.ModAPI;
using VRageMath;
using VRage.Game.ModAPI.Ingame;
using NLog;

using Sandbox.Definitions;
using VRage.Game;
using VRage.Game.Entity;

";

        static string CODE_TEMPLATE = LIBS + @"
namespace Compiled {
    public class Program {
        public static Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public static void Main() {
            MyAPIGateway.Utilities.InvokeOnGameThread (()=>Execute());
        }

        public static void Execute () { 
            try {
                <CodeHere>
            } catch (Exception eee) {
                Log.Error(eee);
            }
        }
    }
}";

        static string CODE_TEMPLATE2 = LIBS + @"

namespace Compiled {
    public class Program {
        public static Logger Log = NLog.LogManager.GetCurrentClassLogger();
        public static void Main () {
            
        }

        public static void Filter (MyProjectorBase __instance, List<MyObjectBuilder_CubeGrid> grids) {
            try {
               <CodeHere>
            } catch (Exception eee) {
               Log.Error(eee);
            }
        }
    }
}";


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void CompileAndExecute(string code) {
            var assembly = Assemble(CODE_TEMPLATE.Replace("<CodeHere>", code));
            Type program = assembly.GetType("Compiled.Program");
            MethodInfo main = program.GetMethod("Main");
            main.Invoke(null, null);
        }

        public static MethodInfo filter;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Compile(string code) {
            var assembly = Assemble(CODE_TEMPLATE2.Replace("<CodeHere>", code));
            Type program = assembly.GetType("Compiled.Program");
            filter = program.easyMethod("Filter");
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Assembly Assemble(string code) {
            CSharpCodeProvider provider = new CSharpCodeProvider();
            CompilerParameters parameters = new CompilerParameters();
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            Func<string, bool> where = (x) => x.EndsWith(".dll") && !x.Contains("Havok.dll") && !x.Contains("steam") && !x.Contains("tier0") && !x.Contains("vstdlib_s") && !x.Contains("RecastDetour.dll") && !x.Contains("VRage.Native.dll"); //

            foreach (var y in Directory.GetFiles(Directory.GetCurrentDirectory() + "\\DedicatedServer64").Where(where)) {
                //if (y.IsDynamic) { DOP.Log.Error ("Skipping:" + y.Location); continue; }
                try {
                    parameters.ReferencedAssemblies.Add(y);
                    //DOP.Log.Error ("Added:" + y);
                } catch (Exception e) {
                    //DOP.Log.Error ("Skipping:" + y);
                }
            }

            //parameters.ReferencedAssemblies.Add (@"C:\Windows\Microsoft.NET\Framework64\v4.0.30319\netstandard.dll");

            parameters.GenerateInMemory = true;
            parameters.GenerateExecutable = true;
            CompilerResults results = provider.CompileAssemblyFromSource(parameters, code);

            if (results.Errors.HasErrors) {
                StringBuilder sb = new StringBuilder();

                foreach (CompilerError error in results.Errors) { DOP.Log.Error($"Error ({error.ErrorNumber}): {error.ErrorText}"); }

                throw new InvalidOperationException(sb.ToString());
            }

            return results.CompiledAssembly;
        }
    }
}