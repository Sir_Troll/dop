﻿using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.Weapons;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Torch.Managers.PatchManager;
using VRage.Game.Entity;

namespace Slime.Features
{
	// DOP.Instance.Config.
	class RemoteDeleting { // : HackStatic
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void InitHack(PatchContext patchContext) { patchContext.Prefix(typeof(MyCubeGrid), "RemoveBlocksBuiltByID", typeof(RemoteDeleting), new[] {"__instance", "identityID"}); }

        public static bool RemoveBlocksBuiltByID(MyCubeGrid __instance, long identityID) {
            if (!DOP.Instance.Config.RemoteDeleting) return true;
            DOP.Log.Warn("RemoteDeleting : " + identityID + " " + __instance + " " + __instance.PositionComp.GetPosition());
            var sphere = new VRageMath.BoundingSphereD(__instance.PositionComp.GetPosition(), DOP.Instance.Config.RemoteDeletingRange);
            var ent = MyEntities.GetEntitiesInSphere(ref sphere);


            var can = checkCanDelete(identityID, ent);

            if (!can) return false;
            switch (DOP.Instance.Config.RemoteDeletingStrategy) {
                case Config.FixRemoteDeleteStrategy.DISABLE_DRILLS: {
                    foreach (var x in __instance.GetFatBlocks()) {
                        var drill = x as MyShipDrill;
                        if (drill != null) { drill.Enabled = false; }
                    }

                    break;
                }
                case Config.FixRemoteDeleteStrategy.BLOCK_DELETE_GRIDS_WITH_DRILLS: {
                    foreach (var x in __instance.GetFatBlocks()) {
                        var drill = x as MyShipDrill;
                        if (drill != null) {
                            Common.ShowNotification("RemoteDeleting : Blocked. Please remove drills from grid.", 15000, "Red", identityID);
                            return false;
                        }
                    }

                    break;
                }
                case Config.FixRemoteDeleteStrategy.BLOCK_DELETE_GRIDS_WITH_WORKING_DRILLS: {
                    foreach (var x in __instance.GetFatBlocks()) {
                        var drill = x as MyShipDrill;
                        if (drill != null && drill.Enabled) {
                            Common.ShowNotification("RemoteDeleting : Blocked. Disable all drills on grid.", 15000, "Red", identityID);
                            return false;
                        }
                    }

                    break;
                }
            }

            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool checkCanDelete(long identityID, List<MyEntity> ent) {
            foreach (var x in ent) {
                var bl = x as MyCubeBlock;
                if (DOP.Instance.Config.RemoteDeletingStrategy == Config.FixRemoteDeleteStrategy.BLOCK_DELETE_GRIDS_WITH_DRILLS_AROUND) {
                    if (x is MyShipDrill) {
                        return false;
                    }
                }
                
                
                if (bl != null && bl.OwnerId != 0) {
                    var cant = Relations.isEnemy(identityID, bl.OwnerId);
                    if (cant) {
                        var mess = DOP.Instance.Config.RemoteDeletingText;
                        if (mess != "") { Common.ShowNotification(mess, 15000, "Red", identityID); }

                        DOP.Log.Warn("RemoteDeleting : Blocked : Near is block :" + bl + " " + identityID + " " + bl.OwnerId);
                        return false;
                    }
                }

                var pl = x as MyCharacter;
                if (pl != null) {
                    var cant = MyIDModule.GetRelationPlayerPlayer(identityID, pl.GetPlayerIdentityId()).AsNumber() == -1;
                    if (cant) {
                        var mess = DOP.Instance.Config.RemoteDeletingText;
                        if (mess != "") { Common.ShowNotification(mess, 15000, "Red", identityID); }

                        DOP.Log.Warn("RemoteDeleting : Blocked : Near is player :" + pl.GetPlayerIdentityId() + " " + pl.DisplayName);
                        return false;
                    }
                }
            }


            DOP.Log.Warn("RemoteDeleting : Deleted : " + identityID);
            return true;
        }
    }
}