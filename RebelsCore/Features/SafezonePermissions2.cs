﻿using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.World;
using Torch.Managers.PatchManager;
using VRage.Game.Entity;
using VRage.Game.ObjectBuilders.Components;
using VRageMath;
using NAPI;

namespace Slime.Features
{
	internal class SafezonePermissions2 {
	    public void InitHack(PatchContext patchContext) {
            P3.IsSafeMethod = typeof(MySafeZone).getMethod("IsSafe", flags: Ext2.all);
            P3.ContainedEntities = typeof(MySafeZone).GetField("m_containedEntities", Ext2.all);

            patchContext.Prefix2(typeof(MySessionComponentSafeZones), "IsActionAllowed", typeof(SafezonePermissions2), new string[] { "__result", "aabb", "action", "sourceEntityId", "user" }, types: new[] {typeof(BoundingBoxD), typeof(MySafeZoneAction), typeof(long), typeof(ulong)});
            patchContext.Prefix2(typeof(MySessionComponentSafeZones), "IsActionAllowed", typeof(SafezonePermissions2), new string[] { "__result", "point", "action", "sourceEntityId", "user" }, types: new[] {typeof(Vector3D), typeof(MySafeZoneAction), typeof(long), typeof(ulong)});
            patchContext.Prefix2(typeof(MySessionComponentSafeZones), "IsActionAllowed", typeof(SafezonePermissions2), new string[] { "__result", "entity", "action", "sourceEntityId", "user" }, types: new[] {typeof(MyEntity), typeof(MySafeZoneAction), typeof(long), typeof(ulong)});
	    }

	    //private static bool UpdateSafeZone(MyObjectBuilder_SafeZone ob, bool sync = false) {
		//    if (ob.SafeZoneBlockId != 0) {
		//	    ob.Entities = new long[0];
		//	    ob.Factions = new long[0];
		//	    ob.Players = new long[0];
		//	    ob.AccessTypeFactions = MySafeZoneAccess.Blacklist;
		//	    ob.AccessTypePlayers = MySafeZoneAccess.Blacklist;
		//	    ob.AccessTypeGrids = MySafeZoneAccess.Blacklist;
		//	    ob.AccessTypeFloatingObjects = MySafeZoneAccess.Blacklist;
		//	    ob.AllowedActions = MySafeZoneAction.All;
		//    }
		//    return true;
	    //}
        
	    
	    //DOP: RESULT FALSE:MySafeZone {1D5891465592363} Grid_S_Large_5 {1C7FAA5F42F27AD} Building 75033634328666813 0
	    private static bool IsActionAllowed(ref bool __result, MyEntity entity, MySafeZoneAction action, long sourceEntityId = 0L, ulong user = 0UL) {
			MyCharacter myCharacter;
			if (user != 0UL) {
				if (MySession.Static.IsUserAdmin(user) && MySafeZone.CheckAdminIgnoreSafezones(user)) {
					__result = true;
					return false;
				}
			} else if ((myCharacter = (entity as MyCharacter)) != null && myCharacter.ControllerInfo != null && myCharacter.ControllerInfo.Controller != null && myCharacter.ControllerInfo.Controller.Player != null) {
				ulong steamId = myCharacter.ControllerInfo.Controller.Player.Id.SteamId;
				if (MySession.Static.IsUserAdmin(steamId) && MySafeZone.CheckAdminIgnoreSafezones(steamId)) {
					__result = true;
					return false;
				}
			}
			if (user != 0UL && MySession.Static.IsUserAdmin(user) && MySafeZone.CheckAdminIgnoreSafezones(user)) {
				__result = true;
				return false;
			}
			if (!MySessionComponentSafeZones.AllowedActions.HasFlag(action)) {
				__result = false;
				return false;
			}
			
			foreach (var x in MySessionComponentSafeZones.SafeZones) {
				if (!x.Enabled) continue;
				if (!P3.ContainsEntityCopy(x, entity.EntityId)) continue;
				__result = P3.IsActionAllowedEntity(x, entity, action, sourceEntityId, user);
				if (__result == false) {
					//DOP.Log.Error("RESULT FALSE:"+x+" "+entity+" "+action+" "+sourceEntityId+" "+user);
				}
				return false;
			}
			
			__result = true;
			return false;
		}

	    private static bool IsActionAllowed(ref bool __result, BoundingBoxD aabb, MySafeZoneAction action, long sourceEntityId = 0L, ulong user = 0UL) {
		    if (action == 0) { //piston extend.
			    __result = true;
			    return false;
		    }
		    
			if (user != 0UL && MySession.Static.IsUserAdmin(user) && MySafeZone.CheckAdminIgnoreSafezones(user)) {
				__result = true;
				return false;
			}
			if (!MySessionComponentSafeZones.AllowedActions.HasFlag(action)) {
				__result = false;
				return false;
			}
			foreach (var x in MySessionComponentSafeZones.SafeZones) {
				if (!x.Enabled) continue;
				if (P3.IsOutsideCopy(x, aabb)) continue;
				__result = P3.IsActionAllowedAABB(x, aabb, action, sourceEntityId, user); // WARNING
				if (__result == false) {
					MyEntity ent;
					MyEntities.TryGetEntityById(sourceEntityId, out ent);
					
					//DOP.Log.Error("RESULT FALSE:"+x+" "+aabb+" "+action+" "+sourceEntityId+" "+user + " / ENT=" + ent);
				}
				return false;
			}
			__result = true;
			return false;
		}
		
	    //DOP: RESULT FALSE:MySafeZone {12DBED71FF1FC7E} Grid_S_Large_5 {15DBEC0FB815496} Building 80843053890806056 0
	    //DOP: RESULT FALSE:MySafeZone {12DBED71FF1FC7E} {Min:{X:-8.64724401320328 Y:-22.199392074894 Z:-22.6308129426217} Max:{X:18.0428094567097 Y:8.7780002049115 Z:12.0082148485039}} Building 0 90133406723833861
	    //DOP: RESULT FALSE:MySafeZone {12DBED71FF1FC7E} Grid_S_Large_5 {15DBEC0FB815496} Damage 0 76561198313708845

	    private static bool IsActionAllowed(ref bool __result, Vector3D point, MySafeZoneAction action, long sourceEntityId = 0L, ulong user = 0UL) {
			if (user != 0UL && MySession.Static.IsUserAdmin(user) && MySafeZone.CheckAdminIgnoreSafezones(user)) {
				__result = true;
				return false;
			}
			if (!MySessionComponentSafeZones.AllowedActions.HasFlag(action)) {
				__result = false;
				return false;
			}
			foreach (var x in MySessionComponentSafeZones.SafeZones) {
				if (!x.Enabled) continue;
				if (!P3.IsInside(x, point)) continue;
				__result = P3.IsActionAllowedPoint(x, point, action, sourceEntityId, user);
				if (__result == false) {
					MyEntity ent;
					MyEntities.TryGetEntityById(sourceEntityId, out ent);
					//DOP.Log.Error("RESULT FALSE:"+x+" "+point+" "+action+" "+sourceEntityId+" "+user + " / ENT=" + ent);
				}
				return false;
			}
			
			__result = true;
			return false;
		}
    }
}