﻿using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Character;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.Game.ObjectBuilders.Components;

namespace Slime.Features {
    static class PermissionLogic {
        public static bool CanDoInSafezone(MySafeZone __instance, MyEntity entity, MySafeZoneAction action) {
            MyEntity safezoneBlock;
            if (!MyEntities.TryGetEntityById(__instance.SafeZoneBlockId, out safezoneBlock, false)) {
                return true; //Must be default safezone;
            }

            if (entity is MyCharacter) {
                var plId = (entity as MyCharacter).GetPlayerIdentityId();

                var sfBlock = (safezoneBlock as MyCubeBlock);
                if (sfBlock != null) {
                    //if (sfBlock.BlockDefinition.Id.SubtypeName.Contains ("Faction")) {
                    //
                    //    faction = MyAPIGateway.Session.Factions.TryGetPlayerFaction (sfBlock.BuiltBy);
                    //    if (faction) {
                    //
                    //    }
                    //} 
                    return true;
                }


                //if (tryGetPlayerAndFaction (__instance, out ownerId, out ownerFaction, out isFactionSf)) {
                //    if (isFactionSf) {
                //        return (plId == ownerFaction.FactionId || ownerFaction.IsLeader(plId));
                //    } else {
                //        return plId == ownerId;
                //    }
                //}
            }

            return false;
        }


        public static MyRelationsBetweenPlayerAndBlock GetRelationPlayerBlock(long owner, long user, MyOwnershipShareModeEnum share, MyRelationsBetweenPlayerAndBlock noFactionResult, MyRelationsBetweenFactions defaultFactionRelations, MyRelationsBetweenPlayerAndBlock defaultShareWithAllRelations) {
            if (owner == user) return MyRelationsBetweenPlayerAndBlock.Owner;
            if (owner == 0L || user == 0L) return MyRelationsBetweenPlayerAndBlock.NoOwnership;

            if (share == MyOwnershipShareModeEnum.Faction) { //FACTION: only faction trusted players can use. If player leave faction - he still can use, if faction relations are Friendship
                if (PermissionsStorage.isFactionFriends(owner, user)) { return MyRelationsBetweenPlayerAndBlock.FactionShare; } else {
                    IMyFaction myFaction = MySession.Static.Factions.TryGetPlayerFaction(user);
                    IMyFaction myFaction2 = MySession.Static.Factions.TryGetPlayerFaction(owner);
                    if (myFaction == myFaction2) { return MyRelationsBetweenPlayerAndBlock.Neutral; } else { return MyRelationsBetweenPlayerAndBlock.Enemies; }
                }
            }

            if (share == MyOwnershipShareModeEnum.All) {
                var b1 = Common2.getPlayer(owner);
                var b2 = Common2.getPlayer(user);
                var bots = b1 == null ? false : b1.IsBot || b2 == null ? false : b2.IsBot; //TODO FIX IT

                if (bots) { //VANILLA SHARE WITH ALL
                    return MyRelationsBetweenPlayerAndBlock.FactionShare;
                } else {
                    if (PermissionsStorage.isFriend(owner, user)) { return MyRelationsBetweenPlayerAndBlock.FactionShare; } else { return MyRelationsBetweenPlayerAndBlock.Enemies; }
                }
            }

            return MyRelationsBetweenPlayerAndBlock.Enemies;
        }


        private static bool tryGetPlayerAndFaction(MySafeZone __instance, out long playerId, out IMyFaction faction, out bool isFactionSf) {
            playerId = 0;
            faction = null;
            isFactionSf = false;
            if (__instance.SafeZoneBlockId != 0) {
                MyEntity safezoneBlock;
                if (MyEntities.TryGetEntityById(__instance.SafeZoneBlockId, out safezoneBlock, false)) {
                    var sfBlock = (safezoneBlock as MyCubeBlock);
                    if (sfBlock != null) {
                        if (sfBlock.BlockDefinition.Id.SubtypeName.Contains("Faction")) {
                            faction = MyAPIGateway.Session.Factions.TryGetPlayerFaction(sfBlock.BuiltBy);
                            isFactionSf = true;
                        }

                        return true;
                    }
                }
            }

            return false;
        }


        private static void Debug(long owner, long user, MyOwnershipShareModeEnum share) {
            var bb1 = Common2.getPlayer(owner);
            var bb2 = Common2.getPlayer(user);
            var bbots = Common2.isBot(owner) || Common2.isBot(user);

            var ff = (PermissionsStorage.isFriend(owner, user) ? "friend" : "enemy") + "/" + (PermissionsStorage.isFactionFriends(owner, user) ? "friend" : "enemy");
            var sameguild = owner.PlayerFaction() == user.PlayerFaction() ? "g_allies" : "g_enemy";


            Log.LogSpamming(61123, (x, y) => x.AppendMany("RelationPB: ", owner.PlayerName(), "/", user.PlayerName(), ":", share, " ", ff, " ", sameguild, " ", (bbots ? "bot" : "players")));
        }
    }
}