﻿using System.Collections.Generic;
using System.IO;

namespace Slime.Features {
    class PermissionsSerializer {
        Dictionary<long, HashSet<long>> friends = new Dictionary<long, HashSet<long>>();
        Dictionary<long, HashSet<long>> factionFriends = new Dictionary<long, HashSet<long>>();
        Dictionary<long, ZoneRights> rights = new Dictionary<long, ZoneRights>(); //key - blockId

        public byte[] Serialize() {
            using (var strm = new MemoryStream()) {
                using (var bw = new BinaryWriter(strm)) {
                    Serialize(bw, friends);
                    Serialize(bw, factionFriends);

                    bw.Write(rights.Count);
                    foreach (var x in rights) {
                        bw.Write(x.Key);
                        x.Value.Serialize(bw);
                    }

                    bw.Flush();
                    return strm.ToArray();
                }
            }
        }

        public void Deserialize(byte[] data) {
            using (var strm = new MemoryStream(data)) {
                using (var bw = new BinaryReader(strm)) {
                    friends = Deserialize(bw);
                    factionFriends = Deserialize(bw);

                    var count = bw.ReadInt32();
                    for (var x = 0; x < count; x++) {
                        var l = bw.ReadInt64();
                        var zr = new ZoneRights();
                        zr.Deserialize(bw);
                    }
                }
            }
        }

        private static void Serialize(BinaryWriter bw, Dictionary<long, HashSet<long>> d) {
            bw.Write(d.Count);
            foreach (var x in d) {
                bw.Write(x.Key);
                bw.Write(x.Value.Count);
                foreach (var y in x.Value) { bw.Write(y); }
            }
        }

        private static Dictionary<long, HashSet<long>> Deserialize(BinaryReader bw) {
            var d = new Dictionary<long, HashSet<long>>();
            var count = bw.ReadInt32();
            for (var x = 0; x < count; x++) {
                var key = bw.ReadInt64();
                var count2 = bw.ReadInt32();
                var h = new HashSet<long>();
                for (var y = 0; y < count2; y++) {
                    var key2 = bw.ReadInt64();
                    h.Add(key2);
                }

                d.Add(key, h);
            }

            return d;
        }
    }
}