﻿using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.Entities;
using System.Reflection;
using Torch.Managers.PatchManager;
using VRage.Collections;
using VRage.Game.Entity;
using VRage.Game.ObjectBuilders.Components;
using VRageMath;
using NAPI;

namespace Slime.Features
{
	class SafezonePermissions {
        static MethodInfo IsSafeMethod;
        static FieldInfo ContainedEntities;


        public void InitHack(PatchContext patchContext) {
            IsSafeMethod = typeof(MySafeZone).getMethod("IsSafe", flags: Ext2.all);
            ContainedEntities = typeof(MySafeZone).GetField("m_containedEntities", Ext2.all);

            patchContext.Redirect2(typeof(MySafeZone), "IsActionAllowed", typeof(SafezonePermissions), "IsActionAllowedAABB", types: new[] {typeof(BoundingBoxD), typeof(MySafeZoneAction), typeof(long)}, types2: null);
            patchContext.Redirect2(typeof(MySafeZone), "IsActionAllowed", typeof(SafezonePermissions), "IsActionAllowedPoint", types: new[] {typeof(Vector3D), typeof(MySafeZoneAction), typeof(long)}, types2: null);
            patchContext.Redirect2(typeof(MySafeZone), "IsActionAllowed", typeof(SafezonePermissions), "IsActionAllowedEntity", types: new[] {typeof(MyEntity), typeof(MySafeZoneAction), typeof(long)}, types2: null);
        }

        public static bool IsActionAllowedAABB(MySafeZone __instance, ref bool __result, BoundingBoxD aabb, MySafeZoneAction action, long sourceEntityId = 0L) {
            DOP.Log.Error("IsActionAllowedEntity" + __instance + " " + __result + " " + aabb + " " + action + " " + sourceEntityId);
            __result = IsActionAllowedCopy(__instance, aabb, action, sourceEntityId);
            return false;
        }

        public static bool IsActionAllowedPoint(MySafeZone __instance, ref bool __result, Vector3D point, MySafeZoneAction action, long sourceEntityId = 0L) {
            DOP.Log.Error("IsActionAllowedEntity" + __instance + " " + __result + " " + point + " " + action + " " + sourceEntityId);
            __result = IsActionAllowedCopy(__instance, point, action, sourceEntityId);
            return false;
        }

        public static bool IsActionAllowedEntity(MySafeZone __instance, ref bool __result, MyEntity entity, MySafeZoneAction action, long sourceEntityId = 0L) {
            DOP.Log.Error("IsActionAllowedEntity" + __instance + " " + __result + " " + entity + " " + action + " " + sourceEntityId);

            __result = IsActionAllowedCopy(__instance, entity, action, sourceEntityId);
            return false;
        }

        public static bool IsSafeCopy(MySafeZone __instance, MyEntity ent) { return (IsSafeMethod.Invoke(__instance, new object[] {ent}) as bool?) ?? false; }

        public static bool ContainsEntityCopy(MySafeZone __instance, long id) { return (ContainedEntities.GetValue(__instance) as MyConcurrentHashSet<long>).Contains(id); }

        private static bool IsOutsideCopy(MySafeZone __instance, BoundingBoxD aabb) {
            bool result;
            if (__instance.Shape == MySafeZoneShape.Sphere) {
                BoundingSphereD boundingSphereD = new BoundingSphereD(__instance.PositionComp.GetPosition(), (double) __instance.Radius);
                result = !boundingSphereD.Intersects(aabb);
            } else {
                MyOrientedBoundingBoxD myOrientedBoundingBoxD = new MyOrientedBoundingBoxD(__instance.PositionComp.LocalAABB, __instance.PositionComp.WorldMatrix);
                result = !myOrientedBoundingBoxD.Intersects(ref aabb);
            }

            return result;
        }

        public static bool IsActionAllowedCopy(MySafeZone __instance, MyEntity entity, MySafeZoneAction action, long sourceEntityId = 0L) {
            if (!__instance.Enabled) return true;
            if (entity == null) return false;
            if (!ContainsEntityCopy(__instance, entity.EntityId)) return true;
            if (!__instance.AllowedActions.HasFlag(action)) return false;

            if (action == MySafeZoneAction.Welding || action == MySafeZoneAction.Grinding || action == MySafeZoneAction.Building || action == MySafeZoneAction.Drilling) {
                if (!PermissionLogic.CanDoInSafezone(__instance, entity, action)) { return false; }
            }

            MyEntity myEntity;
            return (sourceEntityId == 0L || !MyEntities.TryGetEntityById(sourceEntityId, out myEntity, false) || IsSafeCopy(__instance, myEntity.GetTopMostParent(null)));
        }

        public static bool IsActionAllowedCopy(MySafeZone __instance, BoundingBoxD aabb, MySafeZoneAction action, long sourceEntityId = 0L) {
            if (!__instance.Enabled || IsOutsideCopy(__instance, aabb)) return true;
            if (!__instance.AllowedActions.HasFlag(action)) return false;
            MyEntity myEntity;
            return sourceEntityId == 0L || !MyEntities.TryGetEntityById(sourceEntityId, out myEntity, false) || IsSafeCopy(__instance, myEntity.GetTopMostParent(null));
        }

        public static bool IsActionAllowedCopy(MySafeZone __instance, Vector3D point, MySafeZoneAction action, long sourceEntityId = 0L) {
            if (!__instance.Enabled) { return true; }

            bool flag;
            if (__instance.Shape == MySafeZoneShape.Sphere) {
                BoundingSphereD boundingSphereD = new BoundingSphereD(__instance.PositionComp.GetPosition(), (double) __instance.Radius);
                flag = (boundingSphereD.Contains(point) != ContainmentType.Contains);
            } else {
                MyOrientedBoundingBoxD myOrientedBoundingBoxD = new MyOrientedBoundingBoxD(__instance.PositionComp.LocalAABB, __instance.PositionComp.WorldMatrix);
                flag = !myOrientedBoundingBoxD.Contains(ref point);
            }

            if (!flag) { return true; }

            if (!__instance.AllowedActions.HasFlag(action)) { return false; }


            MyEntity myEntity;
            return (sourceEntityId == 0L || !MyEntities.TryGetEntityById(sourceEntityId, out myEntity, false) || IsSafeCopy(__instance, myEntity.GetTopMostParent(null)));
        }
    }
}