﻿using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;
using VRageMath;
using NAPI;


namespace Slime.Features
{
	class FreindsListCommands : CommandModule {
        [Command("tst", "A", null)]
        [Permission(MyPromoteLevel.None)]
        public void tst() {
            var sp = new BoundingSphereD(Context.Player.GetPosition(), 30f);
            var e = MyEntities.GetEntitiesInSphere(ref sp);
            foreach (var x in e) {
                if (x is MyTerminalBlock) { (x as MyTerminalBlock).CustomNameChanged += FreindsListCommands_CustomNameChanged; }
            }
        }

        public static string GetAllFootprints(Exception x) {
            var st = new StackTrace(x, true);
            var frames = st.GetFrames();
            var traceString = new StringBuilder();

            foreach (var frame in frames) {
                if (frame.GetFileLineNumber() < 1) continue;

                traceString.Append("File: " + frame.GetFileName());
                traceString.Append(", Method:" + frame.GetMethod().Name);
                traceString.Append(", LineNumber: " + frame.GetFileLineNumber());
                traceString.Append("  -->  ");
            }

            return traceString.ToString();
        }

        private void FreindsListCommands_CustomNameChanged(MyTerminalBlock obj) {
            try { throw new Exception(); } catch (Exception e) { DOP.Log.Error(GetAllFootprints(e)); }

            //RebelsCorePlugin.Log.Error (" SetCustomNameEvent:"+new Exception().StackTrace);
        }

        [Command("af", "Add friend", null)]
        [Permission(MyPromoteLevel.None)]
        public void addfriend() {
            var pl = getPlayer();
            if (pl != 0) {
                PermissionsStorage.AddFriend(base.Context.Player.IdentityId, pl);
                base.Context.Respond("Friend added!", null, null);
            }
        }


        [Command("rf", "Remove friend", null)]
        [Permission(MyPromoteLevel.None)]
        public void remfriend() {
            var pl = getPlayer();
            if (pl != 0) {
                PermissionsStorage.RemFriend(base.Context.Player.IdentityId, pl);
                base.Context.Respond("Friend removed!", null, null);
            }
        }

        [Command("aff", "Add faction ally", null)]
        [Permission(MyPromoteLevel.None)]
        public void addfacfriend() {
            var pl = getPlayer();
            if (pl != 0) {
                PermissionsStorage.AddFactionFriend(base.Context.Player.IdentityId, pl);
                base.Context.Respond("Faction friend added!", null, null);
            }
        }

        [Command("rff", "Remove faction ally", null)]
        [Permission(MyPromoteLevel.None)]
        public void remfacfriend() {
            var pl = getPlayer();
            if (pl != 0) {
                PermissionsStorage.RemFactionFriend(base.Context.Player.IdentityId, pl);
                base.Context.Respond("Faction friend removed!", null, null);
            }
        }


        [Command("f", "Get friends", null)]
        [Permission(MyPromoteLevel.None)]
        public void getfriends() { base.Context.Respond(PermissionsStorage.GetFriends(base.Context.Player.IdentityId), null, null); }


        [Command("ff", "Get fac friends", null)]
        [Permission(MyPromoteLevel.None)]
        public void getfacfriends() { base.Context.Respond("Faction:" + PermissionsStorage.GetFactionFriends(base.Context.Player.IdentityId), null, null); }


        [Command("all", "Get fac friends", null)]
        [Permission(MyPromoteLevel.None)]
        public void getAllIdentity() { base.Context.Respond(Common2.allIdentity(), null, null); }

        [Command("allp", "Get fac friends", null)]
        [Permission(MyPromoteLevel.None)]
        public void getAllPlayers() { base.Context.Respond(Common2.allIdentity2(), null, null); }

        [Command("allp2", "Get fac friends", null)]
        [Permission(MyPromoteLevel.None)]
        public void getAllPlayers2() { base.Context.Respond(Common2.allIdentity2(), null, null); }


        public long getPlayer() {
            var args = Context.Args;

            var list = new List<IMyPlayer>();
            if (args.Count == 0) base.Context.Respond("Error:Add player name!");

            MyAPIGateway.Multiplayer.Players.GetPlayers(list, (x) => { return x.DisplayName.StartsWith(args[0]); });
            if (list.Count == 0) { base.Context.Respond("Nobody found with nickname : " + args[0], null, null); } else if (list.Count > 1) { base.Context.Respond("Nobody found several with nickname : " + args[0], null, null); } else { return list[0].IdentityId; }

            return 0;
        }
    }
}