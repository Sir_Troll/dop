﻿using NAPI;
using Slime.Mod;
using System;
using System.Collections.Generic;

namespace Slime.Features {
    class PermissionsStorage {
        public static Dictionary<long, HashSet<long>> friends = new Dictionary<long, HashSet<long>>();
        public static Dictionary<long, HashSet<long>> factionFriends = new Dictionary<long, HashSet<long>>();
        public static Dictionary<long, ZoneRights> rights = new Dictionary<long, ZoneRights>(); //key - blockId

        public static ZoneRights GetSafezoneRights(long blockId) {
            if (!rights.ContainsKey(blockId)) { rights.Add(blockId, new ZoneRights()); }

            return rights[blockId];
        }

        public static void AddFriend(long owner, long user) {
            if (!friends.ContainsKey(owner)) { friends.Add(owner, new HashSet<long>()); }

            friends[owner].Add(user);
        }

        public static void AddFactionFriend(long owner, long user) {
            if (!factionFriends.ContainsKey(owner)) { factionFriends.Add(owner, new HashSet<long>()); }

            factionFriends[owner].Add(user);
        }

        public static void RemFriend(long owner, long user) {
            if (friends.ContainsKey(owner)) { friends.Remove(user); }
        }

        public static void RemFactionFriend(long owner, long user) {
            if (factionFriends.ContainsKey(owner)) { factionFriends.Remove(user); }
        }

        public static String GetFactionFriends(long owner) {
            if (factionFriends.ContainsKey(owner) && factionFriends[owner].Count != 0) {
                var s = "Friends:";
                foreach (var x in factionFriends[owner]) { s += Common.getPlayerName(owner) + " "; }

                return s;
            } else { return "No friends"; }
        }

        public static String GetFriends(long owner) {
            if (friends.ContainsKey(owner) && friends[owner].Count != 0) {
                var s = "Friends:";
                foreach (var x in friends[owner]) { s += Common.getPlayerName(owner) + " "; }

                return s;
            } else { return "No friends"; }
        }

        public static bool isFriend(long owner, long user) { return friends.ContainsKey(owner) && friends[owner].Contains(user); }

        public static bool isFactionFriends(long owner, long user) { return factionFriends.ContainsKey(owner) && factionFriends[owner].Contains(user); }
    }
}