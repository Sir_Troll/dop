﻿using Torch.Managers.PatchManager;
using Sandbox.Game;
using System.Runtime.CompilerServices;
using System;
using NAPI;

namespace Slime.Features
{
	class ExplosionFix {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void InitHack(PatchContext patchContext) { patchContext.Prefix(typeof(MyExplosions), "AddExplosion", typeof(ExplosionFix), new[] {"explosionInfo", "updateSync"}); }

        public static bool AddExplosion(ref MyExplosionInfo explosionInfo, bool updateSync = true) {
            explosionInfo.ExplosionSphere.Radius = Math.Min(explosionInfo.ExplosionSphere.Radius, DOP.Instance.Config.ExplosionProtectionMeters);
            return true;
        }
    }
}