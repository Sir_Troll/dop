﻿using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.Entities;
using System.Reflection;
using Sandbox.Game.Entities.Character;
using Sandbox.ModAPI;
using VRage.Collections;
using VRage.Game.Entity;
using VRage.Game.ObjectBuilders.Components;
using VRageMath;
using NAPI;
using SpaceEngineers.Game.Entities.Blocks.SafeZone;

namespace Slime.Features
{
	internal class P3 {
        public static MethodInfo IsSafeMethod;
        public static FieldInfo ContainedEntities;
        
        /*
         * Grinding
         * 		Tool->MySessionComponentSafeZones.IsActionAllowed(targetBlock.WorldAABB, MySafeZoneAction.Grinding, 0L, user) -> MySessionComponentSafeZones.IsActionAllowed((MyEntity)targetDestroyable, MySafeZoneAction.Grinding, 0L, 0UL)
         * 		Ship->MySessionComponentSafeZones.IsActionAllowed(base.CubeGrid, MySafeZoneAction.Grinding, shooter (OwnerId), 0UL))
         * Welding
         * 		Ship->MySessionComponentSafeZones.IsActionAllowed(base.CubeGrid, MySafeZoneAction.Welding, shooter, 0UL))
         * 		Tool->if (!MySessionComponentSafeZones.IsActionAllowed(this.Owner, MySafeZoneAction.Welding, 0L, 0UL)) & MySessionComponentSafeZones.IsActionAllowed(block.WorldAABB, MySafeZoneAction.Welding, 0L, user)
         * 
         */
        
        internal static bool IsSafeCopy(MySafeZone __instance, MyEntity ent) { return (IsSafeMethod.Invoke(__instance, new object[] {ent}) as bool?) ?? false; }

        internal static bool ContainsEntityCopy(MySafeZone __instance, long id) { return (ContainedEntities.GetValue(__instance) as MyConcurrentHashSet<long>).Contains(id); }

        internal static bool IsOutsideCopy(MySafeZone __instance, BoundingBoxD aabb) {
            bool result;
            if (__instance.Shape == MySafeZoneShape.Sphere) {
                BoundingSphereD boundingSphereD = new BoundingSphereD(__instance.PositionComp.GetPosition(), (double) __instance.Radius);
                result = !boundingSphereD.Intersects(aabb);
            } else {
                MyOrientedBoundingBoxD myOrientedBoundingBoxD = new MyOrientedBoundingBoxD(__instance.PositionComp.LocalAABB, __instance.PositionComp.WorldMatrix);
                result = !myOrientedBoundingBoxD.Intersects(ref aabb);
            }

            return result;
        }

        internal static bool IsActionAllowedEntity(MySafeZone __instance, MyEntity entity, MySafeZoneAction action, long sourceEntityId, ulong user) {
	        if (MyEntities.TryGetEntityById(__instance.SafeZoneBlockId, out var safezoneBlock, false)) {
	            switch (action) {
		            case MySafeZoneAction.Damage:
		            case MySafeZoneAction.VoxelHand:
		            case MySafeZoneAction.Drilling: return false;
		            
		            case MySafeZoneAction.ConvertToStation:
		            case MySafeZoneAction.Shooting:
		            case MySafeZoneAction.LandingGearLock: return true;
	            }

	            var sfb = safezoneBlock as MySafeZoneBlock;
	            var pl = MyAPIGateway.Players.TryGetIdentityId(user);
	            if (action == MySafeZoneAction.Grinding) {
		            if (entity is MyCubeGrid) {
			            return sfb.GetUserRelationToOwner(sourceEntityId).AsNumber() >= 1;
		            } else {
			            return true;
		            }
	            }
	            if (action == MySafeZoneAction.Welding) {
		            var owner = (entity as MyCharacter);
		            if (owner != null) {
			            return sfb.GetUserRelationToOwner(owner.GetPlayerIdentityId()).AsNumber() >= 1;
		            } else {
			            return sfb.GetUserRelationToOwner(sourceEntityId).AsNumber() >= 1;
		            }
	            }

	            if (action == MySafeZoneAction.Building) {
		            var character = entity as MyCharacter;
			        if (character != null) { return sfb.GetUserRelationToOwner(character.GetPlayerIdentityId()).AsNumber() >= 1; }
			        
			        MyEntity ent;
			        if (sourceEntityId != 0 && MyEntities.TryGetEntityById(sourceEntityId, out ent)) {
				        character = ent as MyCharacter;
				        if (character != null) {
					        return sfb.GetUserRelationToOwner(character.GetPlayerIdentityId()).AsNumber() >= 1;
				        } else {
					        DOP.Log.Error("BUILDING:"+ent);
				        }
			        }
			        
			        return sfb.GetUserRelationToOwner(pl).AsNumber() >= 1;
	            }
	            return false;
            }

	        MyEntity myEntity;
            return (sourceEntityId == 0L || !MyEntities.TryGetEntityById(sourceEntityId, out myEntity, false) || IsSafeCopy(__instance, myEntity.GetTopMostParent(null)));
        }
        
        internal static bool IsActionAllowedAABB(MySafeZone __instance, BoundingBoxD aabb, MySafeZoneAction action, long sourceEntityId, ulong user) {
	        if (MyEntities.TryGetEntityById(__instance.SafeZoneBlockId, out var safezoneBlock, false)) {
	            
	            switch (action) {
		            case MySafeZoneAction.Damage:
		            case MySafeZoneAction.VoxelHand:
		            case MySafeZoneAction.Drilling: return false;
		            case MySafeZoneAction.Shooting:
		            case MySafeZoneAction.ConvertToStation:
		            case MySafeZoneAction.LandingGearLock: return true;
	            }
	            
	            var sfb = safezoneBlock as MySafeZoneBlock;
	            var pl = MyAPIGateway.Players.TryGetIdentityId(user);
	            
	            if (action == MySafeZoneAction.Grinding) {
		            return sfb.GetUserRelationToOwner(pl).AsNumber() >= 1;
	            }
	            
	            if (action == MySafeZoneAction.Building) {
		            if (sourceEntityId == 0 && user == 0) return true; // For welding projections
		            return sfb.GetUserRelationToOwner(pl).AsNumber() >= 1;
	            }
	            
	            return sfb.GetUserRelationToOwner(pl).AsNumber() >= 1;
            } 
            
            if (!__instance.AllowedActions.HasFlag(action)) return false;
            
            MyEntity myEntity;
            return sourceEntityId == 0L || !MyEntities.TryGetEntityById(sourceEntityId, out myEntity, false) || IsSafeCopy(__instance, myEntity.GetTopMostParent(null));
        }

        internal static bool IsInside(MySafeZone __instance, Vector3D point) {
	        if (__instance.Shape == MySafeZoneShape.Sphere) {
		        BoundingSphereD boundingSphereD = new BoundingSphereD(__instance.PositionComp.GetPosition(), (double) __instance.Radius);
		        return (boundingSphereD.Contains(point) != ContainmentType.Contains);
	        } else {
		        MyOrientedBoundingBoxD myOrientedBoundingBoxD = new MyOrientedBoundingBoxD(__instance.PositionComp.LocalAABB, __instance.PositionComp.WorldMatrix);
		        return !myOrientedBoundingBoxD.Contains(ref point);
	        }
        }

        internal static bool IsActionAllowedPoint(MySafeZone __instance, Vector3D point, MySafeZoneAction action, long sourceEntityId, ulong user) {
	        if (MyEntities.TryGetEntityById(__instance.SafeZoneBlockId, out var safezoneBlock, false)) {
	            
	            switch (action) {
		            case MySafeZoneAction.Damage:
		            case MySafeZoneAction.VoxelHand:
		            case MySafeZoneAction.Drilling: return false;
		            case MySafeZoneAction.Shooting:
		            case MySafeZoneAction.ConvertToStation:
		            case MySafeZoneAction.Building: 
		            case MySafeZoneAction.LandingGearLock: return true;
	            }
	            
	            var sfb = safezoneBlock as MySafeZoneBlock;
	            var pl = MyAPIGateway.Players.TryGetIdentityId(user);
	            //DOP.LogSpamming(3429, (x,y)=>x.Append("AllowedPOINT " + action + ": (" +sourceEntityId +"/"+ user +":"+ pl +" / "+sfb.OwnerId+") | " + sfb.GetUserRelationToOwner(sourceEntityId) + "/" +sfb.GetUserRelationToOwner(pl)));
	            return sfb.GetUserRelationToOwner(pl).AsNumber() >= 1;
	        }
            

            if (!__instance.AllowedActions.HasFlag(action)) { return false; }


            MyEntity myEntity;
            return (sourceEntityId == 0L || !MyEntities.TryGetEntityById(sourceEntityId, out myEntity, false) || IsSafeCopy(__instance, myEntity.GetTopMostParent(null)));
        }
    }
    
    
    
}

//patchContext.Prefix2(typeof(MySafeZone), "IsActionAllowed", typeof(SafezonePermissions2), new string[] { "__instance", "__result", "aabb", "action", "sourceEntityId", }, types: new[] {typeof(BoundingBoxD), typeof(MySafeZoneAction), typeof(long)});
//patchContext.Prefix2(typeof(MySafeZone), "IsActionAllowed", typeof(SafezonePermissions2), new string[] { "__instance", "__result", "point", "action", "sourceEntityId", }, types: new[] {typeof(Vector3D), typeof(MySafeZoneAction), typeof(long)});
//patchContext.Prefix2(typeof(MySafeZone), "IsActionAllowed", typeof(SafezonePermissions2), new string[] { "__instance", "__result", "entity", "action", "sourceEntityId", }, types: new[] {typeof(MyEntity), typeof(MySafeZoneAction), typeof(long)});


//public static bool IsActionAllowedAABB(MySafeZone __instance, ref bool __result, BoundingBoxD aabb, MySafeZoneAction action, long sourceEntityId = 0L) {
//    __result = P3.IsActionAllowedCopy(__instance, aabb, action, sourceEntityId);
//    return false;
//}
//
//public static bool IsActionAllowedPoint(MySafeZone __instance, ref bool __result, Vector3D point, MySafeZoneAction action, long sourceEntityId = 0L) {
//    __result = P3.IsActionAllowedCopy(__instance, point, action, sourceEntityId);
//    return false;
//}
//
//public static bool IsActionAllowedEntity(MySafeZone __instance, ref bool __result, MyEntity entity, MySafeZoneAction action, long sourceEntityId = 0L) {
//    __result = P3.IsActionAllowedCopy(__instance, entity, action, sourceEntityId);
//    return false;
//}
