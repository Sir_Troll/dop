using Slime.GUI;
using System;
using System.Runtime.CompilerServices;

namespace Slime.Config
{
	partial class CoreConfig {
        private bool _enabledSlowdownAssembler = false;
        private bool _enabledSlowdownRefinery = true;
        
        private bool _enabledSlowdownGrinder = true;
        private int _enabledSlowdownGrinderInterval = 10;
        private float _enabledSlowdownGrinderBoost = 10;
        
        private bool _enabledSlowdownThruster = true;
        private bool _enabledSlowdownTurretTurnOff = true;
        private bool _enabledSlowdownTurretMod = true;
        private bool _enabledSlowdownTurretActivation = true;
        private int _enabledSlowdownTurretTurnOffRadius = 3000;
        private int _enabledSlowdownTurretTurnOffInterval = 500;

        private bool _enabledSlowdownSafezoneEnabled = false;
        private int _enabledSlowdownSafezoneEnabledTimes = 3;
        
        private bool _slowPBUpdateEnable = false;
        private int _slowPBUpdate1 = 10;
        private int _slowPBUpdate10 = 4;
        private int _slowPBUpdate100 = 2;
        private string _slowPBIgnored = "";

        private bool _enabledSlowdownWelder = true;
        private int _enabledSlowdownWelderInterval = 20;
        private float _enabledSlowdownWelderBoost = 20;
        
        private string _optimizations = "Parachute Ship2 Char Mass Inv Other ProgBlock GasGenerator1 GasGenerator2 GasGenerator3 Projector1 Block1 Thrust1 Ship1 Tank";
        private bool _enabledSlowdownCockpit = false;
        private bool _enabledSlowdownGasTank = false;
        private bool _enabledThrusterDamage = false;
        private bool _enabledSlowdownGasGen = false;
        private bool _enabledSlowdownLandingGear = false;
        private bool _enabledSlowdownParachute = false;
        private bool _enabledSlowdownSensor = false;
		
        [DisplayTab(Name = "Optimizations", GroupName = "Optimizations", Tab = "Optimizations", Order = 0)]
        public string Optimizations { get => _optimizations; set => SetValue(ref _optimizations, value); }


        [DisplayTab(Name = "Refinery", GroupName = "Slowdown", Tab = "Optimizations", Order = 2)]
        public bool EnabledSlowdownRefinery { get => _enabledSlowdownRefinery; set => SetValue(ref _enabledSlowdownRefinery, value); }

       

        //=================================================================================================
        
        [DisplayTab(Name = "Grinder", GroupName = "Grinder", Tab = "Optimizations", Order = 3)]
        public bool EnabledSlowdownGrinder { get => _enabledSlowdownGrinder; set => SetValue(ref _enabledSlowdownGrinder, value); }
        
        [DisplayTab(Name = "GrinderInterval", GroupName = "Grinder", Tab = "Optimizations", Order = 3)]
        public int EnabledSlowdownGrinderInterval { get => _enabledSlowdownGrinderInterval; set => SetValue(ref _enabledSlowdownGrinderInterval, value); }
        
        [DisplayTab(Name = "GrinderBoost", GroupName = "Grinder", Tab = "Optimizations", Order = 3)]
        public float EnabledSlowdownGrinderBoost { get => _enabledSlowdownGrinderBoost; set => SetValue(ref _enabledSlowdownGrinderBoost, value); }
        
        //=================================================================================================

        [DisplayTab(Name = "Welder", GroupName = "Welder", Tab = "Optimizations", Order = 4)]
        public bool EnabledSlowdownWelder { get => _enabledSlowdownWelder; set => SetValue(ref _enabledSlowdownWelder, value); }

        [DisplayTab(Name = "WelderInterval", GroupName = "Welder", Tab = "Optimizations", Order = 4)]
        public int EnabledSlowdownWelderInterval { get => _enabledSlowdownWelderInterval; set => SetValue(ref _enabledSlowdownWelderInterval, value); }

        [DisplayTab(Name = "WelderBoost", GroupName = "Welder", Tab = "Optimizations", Order = 4)]
        public float EnabledSlowdownWelderBoost { get => _enabledSlowdownWelderBoost; set => SetValue(ref _enabledSlowdownWelderBoost, value); }
        
        //=================================================================================================

        [DisplayTab(Name = "GasGen", GroupName = "Slowdown", Tab = "Optimizations", Order = 6)]
        public bool EnabledSlowdownGasGen { get => _enabledSlowdownGasGen; set => SetValue(ref _enabledSlowdownGasGen, value); }

        [DisplayTab(Name = "Thruster", GroupName = "Slowdown", Tab = "Optimizations", Order = 7)]
        public bool EnabledSlowdownThruster { get => _enabledSlowdownThruster; set => SetValue(ref _enabledSlowdownThruster, value); }

        [DisplayTab(Name = "ThrusterDamage", GroupName = "Slowdown", Tab = "Optimizations", Order = 8)]
        public bool EnabledThrusterDamage { get => _enabledThrusterDamage; set => SetValue(ref _enabledThrusterDamage, value); }

        //=================================================================================================
        
        [DisplayTab(Name = "On/Off", GroupName = "Turrets", Tab = "Optimizations", Order = 1)]
        public bool EnabledSlowdownTurretTurnOff { get => _enabledSlowdownTurretTurnOff; set => SetValue(ref _enabledSlowdownTurretTurnOff, value); }
        
        [DisplayTab(Name = "Save turrets states (required mod)", GroupName = "Turrets", Tab = "Optimizations", Order = 1)]
        public bool EnabledSlowdownTurretSaveState { get => _enabledSlowdownTurretMod; set => SetValue(ref _enabledSlowdownTurretMod, value); }

        [DisplayTab(Name = "Send Signal [ENEMIES-DETECTED]/[ENEMIES-LOST]", GroupName = "Turrets", Tab = "Optimizations", Description = "1)Finds BlockGroup [ENEMIES-DETECTED]/[ENEMIES-LOST] 2) For each block makes is Enabled/Disabled 3) For each timer - TriggerNow 4) For each PB, TryRun with argument in name of PB [DETECTED:xxx]/[LOST:yyy] (xxx/yyy-arguments)", Order = 1)]
        public bool EnabledSlowdownTurretSendSignal { get => _enabledSlowdownTurretActivation; set => SetValue(ref _enabledSlowdownTurretActivation, value); }
    
        [DisplayTab(Name = "Activation radius (meters)", GroupName = "Turrets", Tab = "Optimizations", Order = 2, Description = "MaxSpeed * 2 * (INTERVAL / 60) + MaxTurretRange")]
        public int EnabledSlowdownTurretTurnOffRadius { get => _enabledSlowdownTurretTurnOffRadius; set => SetValue(ref _enabledSlowdownTurretTurnOffRadius, value); }

        [DisplayTab(Name = "Activation interval (ticks)", GroupName = "Turrets", Tab = "Optimizations", Order = 2)]
        public int EnabledSlowdownTurretTurnOffInterval{ get => _enabledSlowdownTurretTurnOffInterval; set => SetValue(ref _enabledSlowdownTurretTurnOffInterval, value); }
      
        //=================================================================================================
        
        [DisplayTab(Name = "On/Off", GroupName = "Safezones", Tab = "Optimizations", Order = 1)]
        public bool EnabledSlowdownSafezoneEnabled { get => _enabledSlowdownSafezoneEnabled; set => SetValue(ref _enabledSlowdownSafezoneEnabled, value); }

        [DisplayTab(Name = "Slowdown times", GroupName = "Safezones", Tab = "Optimizations", Order = 2, Description = "-1 Disables at all pulling out from safezones")]
        public int EnabledSlowdownSafezoneEnabledTimes { get => _enabledSlowdownSafezoneEnabledTimes; set => SetValue(ref _enabledSlowdownSafezoneEnabledTimes, Math.Max(-1, value)); }

        //=================================================================================================
        
        
        [DisplayTab(Name = "Enabled", GroupName = "ProgramBlock", Tab = "Optimizations", Order = 1)]
        public bool SlowPBEnabled { get => _slowPBUpdateEnable; set => SetValue(ref _slowPBUpdateEnable, value); }

        [DisplayTab(Name = "Update 1 Slow", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "Description 1-> vanilla, 5-> 4 frames idle, 1 update")]
        public int SlowPBUpdate1 { get => _slowPBUpdate1; set => SetValue(ref _slowPBUpdate1, Math.Max(1, value)); }

        [DisplayTab(Name = "Update 10 Slow", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "Description 1-> vanilla, 5-> 49 frames idle, 1 update")]
        public int SlowPBUpdate10 { get => _slowPBUpdate10; set => SetValue(ref _slowPBUpdate10, Math.Max(1, value)); }

        [DisplayTab(Name = "Update 100 Slow", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "Description 1-> vanilla, 5-> 499 frames idle, 1 update")]
        public int SlowPBUpdate100 { get => _slowPBUpdate100; set => SetValue(ref _slowPBUpdate100, Math.Max(1, value)); }

        [DisplayTab(Name = "Ignored subtypes", GroupName = "ProgramBlock", Order = 2, Tab = "Optimizations", Description = "This subtypeIds will be ignored. use ' ' or ',' as a separator")]
        public string SlowPBIgnored { get => _slowPBIgnored; set => SetValue(ref _slowPBIgnored, value); }

        //=================================================================================================
        
        [DisplayTab(Name = "GasTank (temporary not working good)", GroupName = "Slowdown", Tab = "Optimizations", Order = 13)]
        public bool EnabledSlowdownGasTank { get => _enabledSlowdownGasTank; set => SetValue(ref _enabledSlowdownGasTank, value); }

        [DisplayTab(Name = "LandingGear (not working yet)", GroupName = "Slowdown", Tab = "Optimizations", Order = 13)]
        public bool EnabledSlowdownLandingGear { get => _enabledSlowdownLandingGear; set => SetValue(ref _enabledSlowdownLandingGear, value); }

        [DisplayTab(Name = "Parachute (untested)", GroupName = "Slowdown", Tab = "Optimizations", Order = 13)]
        public bool EnabledSlowdownParachute { get => _enabledSlowdownParachute; set => SetValue(ref _enabledSlowdownParachute, value); }

        [DisplayTab(Name = "Sensor (untested)", GroupName = "Slowdown", Tab = "Optimizations", Order = 13)]
        public bool EnabledSlowdownSensor { get => _enabledSlowdownSensor; set => SetValue(ref _enabledSlowdownSensor, value); }

        
        [DisplayTab(Name = "Assembler (Test!!!)", GroupName = "Slowdown", Tab = "Optimizations", Order = 13)]
        public bool EnabledSlowdownAssembler { get => _enabledSlowdownAssembler; set => SetValue(ref _enabledSlowdownAssembler, value); }
        
        //=================================================================================================

        [DisplayTab(Name = "Cockpit (Planned)", GroupName = "Slowdown", Tab = "Optimizations", Order = 14, Enabled = false)]
        public bool EnabledSlowdownCockpit { get => _enabledSlowdownCockpit; set => SetValue(ref _enabledSlowdownCockpit, value); }

    }
}