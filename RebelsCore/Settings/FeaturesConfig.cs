using Slime.GUI;
using System;
using Torch;

namespace Slime.Config {
    public enum OptimizationLevel {
        NONE = 0,
        LIGHT = 1,
        SOFT = 2,
        MEDIUM = 3,
        HIGH = 4,
        VERY_HIGH = 5,
        EXTREME = 6
    }

    public enum FixRemoteDeleteStrategy {
        NONE = 0,
        DISABLE_DRILLS = 1,
        BLOCK_DELETE_GRIDS_WITH_WORKING_DRILLS = 2,
        BLOCK_DELETE_GRIDS_WITH_DRILLS = 3,
        BLOCK_DELETE_GRIDS_WITH_DRILLS_AROUND = 4,
    }


    public partial class CoreConfig : ViewModel {
        private bool _enableStaticAntiRamming = false;
        private bool _voxelDamageTweaksEnabled = false;
        private int _voxelDamageTweaksMinSpeed = 30;

        private bool _remoteDeleting = false;
        private string _remoteDeletingText = "You were unable delete grid, because enemy is near";
        private int _remoteDeletingRange = 3000;
        private FixRemoteDeleteStrategy _remoteDeletingStrategy = FixRemoteDeleteStrategy.BLOCK_DELETE_GRIDS_WITH_WORKING_DRILLS;
        private string _remoteDeletingStrategyText = "Remote Delete : Was blocks, try remove drills";

        private bool _fixCharacter = false;
        private bool _fixGPS = false;
        private bool _safezone2 = false;
        private bool _fixSafezone = false;
        private int _fixCharacterInterval = 300;

        private bool _colorFix = false;
        
        private bool _disassembleMultiplierEnabled = false;
        private float _disassembleMultiplier = 1.0f;
        
        private int _pingkickIntervalSeconds;
        private int _minimalPing;
        private bool _pingKickerEnabled;
        private bool _pingKickerIgnoreReservedPlayers;
        
        [DisplayTab(GroupName = "Disassemble Ratio", Tab = "Features", Name = "Dissassemble Multiplier")]
        public bool DisassembleMultiplierEnabled { get { return this._disassembleMultiplierEnabled; } set { this.SetValue(ref this._disassembleMultiplierEnabled, value); } }
        
        [DisplayTab(GroupName = "Disassemble Ratio", Tab = "Features", Name = "Disassemble Ratio")]
        public float DisassembleMultiplier { get { return _disassembleMultiplier; } set { SetValue(ref _disassembleMultiplier, value); } }

        [DisplayTab(GroupName = "RemoteDelete", Tab = "Features", Name = "Enabled")]
        public bool RemoteDeleting { get { return this._remoteDeleting; } set { this.SetValue(ref this._remoteDeleting, value); } }

        [DisplayTab(GroupName = "RemoteDelete", Tab = "Features", Name = "TextToPlayer")]
        public string RemoteDeletingText { get { return this._remoteDeletingText; } set { this.SetValue(ref _remoteDeletingText, value); } }

        [DisplayTab(GroupName = "RemoteDelete", Tab = "Features", Name = "Fix Drills method")]
        public FixRemoteDeleteStrategy RemoteDeletingStrategy { get { return _remoteDeletingStrategy; } set { SetValue(ref _remoteDeletingStrategy, value); } }

        [DisplayTab(GroupName = "RemoteDelete", Tab = "Features", Name = "Fix Drills blocked text")]
        public string RemoteDeletingStrategyText { get { return _remoteDeletingStrategyText; } set { SetValue(ref _remoteDeletingStrategyText, value); } }

        [DisplayTab(GroupName = "RemoteDelete", Tab = "Features", Name = "Range (meters)")]
        public int RemoteDeletingRange { get { return this._remoteDeletingRange; } set { this.SetValue(ref this._remoteDeletingRange, value); } }

        [DisplayTab(GroupName = "Fixes", Tab = "Features", Name = "Color friendly ships (Nanobot)")]
        public bool ColorFix { get { return this._colorFix; } set { this.SetValue(ref this._colorFix, value); } }


        [DisplayTab(GroupName = "Fixes", Tab = "Features", Name = "Fix Character Command Enabled", Description = "Fixes problem with `stuck` character. ")]
        public bool FixCharacter { get { return this._fixCharacter; } set { this.SetValue(ref this._fixCharacter, value); } }

        [DisplayTab(GroupName = "Fixes", Tab = "Features", Name = "Fix Character Interval (seconds)")]
        public int FixCharacterInterval { get { return this._fixCharacterInterval; } set { this.SetValue(ref this._fixCharacterInterval, value); } }

        [DisplayTab(GroupName = "Fixes", Tab = "Features", Name = "Fix GPS Command")]
        public bool FixGPSCommand { get { return this._fixGPS; } set { this.SetValue(ref this._fixGPS, value); } }

        [DisplayTab(GroupName = "Safezone", Tab = "Features", Name = "Nebula Safezones")]
        public bool Safezone { get { return this._safezone2; } set { this.SetValue(ref this._safezone2, value); } }

        [DisplayTab(GroupName = "Fixes", Tab = "Features", Name = "Fix Safezone Command")]
        public bool FixSafezoneCommand { get { return this._fixSafezone; } set { this.SetValue(ref this._fixSafezone, value); } }
        

        [DisplayTab(GroupName = "Protection", Tab = "Features", Name = "Anti Ramming Stations", ToolTip = "If you grid, is static, it dont take damage from")]
        public bool AntiRammingStations { get { return this._enableStaticAntiRamming; } set { this.SetValue(ref this._enableStaticAntiRamming, value); } }

        [DisplayTab(GroupName = "Protection", Tab = "Features", Name = "Voxels damage protection", ToolTip = "When your server hardly lag, player's ship will be protected from beeing destroyed by voxels. Players loose MUCH less grids")]
        public bool VoxelDamageTweaksEnabled { get { return this._voxelDamageTweaksEnabled; } set { this.SetValue(ref this._voxelDamageTweaksEnabled, value); } }

        [DisplayTab(GroupName = "Protection", Tab = "Features", Name = "Voxels damage protection Max Speed (m/s)", ToolTip = "Maximum speed, to protect your grid from damage")]
        public int VoxelDamageTweaksMaxSpeed { get { return this._voxelDamageTweaksMinSpeed; } set { this.SetValue(ref _voxelDamageTweaksMinSpeed, value); } }
    }
}