﻿using NLog;
using Torch;
using Torch.API;
using Torch.API.Session;

namespace RebelsCore
{
    public class RebelsCore : TorchPluginBase
    {
        private bool _init = false;

        //Use this to write messages to the Torch log.
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();

        /// <inheritdoc />
        public override void Init(ITorchBase torch)
        {
            base.Init(torch);

            if (_init) return;
            _init = true;
            Log.Info("[INFO] RebelsCore is initialized.");
            Log.Debug("[DEBUG] RebelsCore is initialized.");
            Log.Trace("[TRACE] RebelsCore is initialized.");
//            torch.CurrentSession.StateChanged += OnSessionStateChanged;
//            Your _init code here, the game is not initialized at this point.
        }

        /// <inheritdoc />
        public override void Update()
        {
//            Put code that should run every tick here.
//            Log.Info("RebelsCore Update()");
        }

        /// <inheritdoc />
        public override void Dispose()
        {
//            Unload your plugin here.
        }

        private void OnSessionStateChanged(ITorchSession session, TorchSessionState newState)
        {
            switch (newState)
            {
                case TorchSessionState.Loading:
                    //Executed before the world loads.
                    break;
                case TorchSessionState.Loaded:
                    //Executed after the world has finished loading.
                    break;
                case TorchSessionState.Unloading:
                    //Executed before the world unloads.
                    break;
                case TorchSessionState.Unloaded:
                    //Executed after the world unloads.
                    break;
            }
        }
    }
}