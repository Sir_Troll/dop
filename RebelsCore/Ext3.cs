﻿using Slime.Features;
using System;
using Torch.Managers.PatchManager;
using NAPI;

namespace Slime
{
	public static class Ext3
	{
		public static void Redirect(this PatchContext _ctx, Type t, params string[] args)
		{
			foreach (var s in args)
			{
				_ctx.Redirect(t, s, typeof(SlowdownHack), s);
			}
		}
	}
}
