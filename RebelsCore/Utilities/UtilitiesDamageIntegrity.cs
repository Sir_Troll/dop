using System.Collections;
using System.Collections.Generic;
using Sandbox.Game;
using Sandbox.Game.Entities.Cube;
using VRage.Game;
using VRageMath;

namespace RebelsCore.Utilities
{
    internal static class UtilitiesDamageIntegrity
    {
        private static readonly MyInventory VirtualInventory = new MyInventory(long.MaxValue, Vector3D.One, MyInventoryFlags.CanReceive | MyInventoryFlags.CanSend);
        public static void DamageBlocks(IEnumerable blocksToDamage, float integrityDamage)
        {
            var mySlimBlocks = (List<MySlimBlock>) blocksToDamage;
            var blocksToRemove = new List<MySlimBlock>();
            foreach (var mySlimBlock in mySlimBlocks)
            {
                mySlimBlock.DecreaseMountLevel(integrityDamage, VirtualInventory);
                mySlimBlock.MoveItemsFromConstructionStockpile(VirtualInventory);
                if (mySlimBlock.IsFullyDismounted)
                {
                    blocksToRemove.Add(mySlimBlock);
                }
            }
            
            foreach (var mySlimBlock in blocksToRemove)
            {
                mySlimBlock.CubeGrid.RemoveBlock(mySlimBlock);
            }
            
            VirtualInventory.ClearItems();
        }
    }
}