using System.Collections;
using System.Linq;
using RebelsCore.Enums;
using Sandbox.Game.Entities;
using VRage.ObjectBuilders;
using VRage.Utils;

namespace RebelsCore.Utilities
{
    internal class UtilitesEntity
    {
        private static IEnumerable GetBlocks(string subtypeId)
        {
            return MyEntities.GetEntities().OfType<MyCubeGrid>().SelectMany(grid =>
                grid.GetBlocks().Where(block => block.BlockDefinition.Id.SubtypeName == subtypeId)
            ).ToList();
        }
        private static IEnumerable GetBlocks(MyObjectBuilderType typeId, string subtypeId)
        {
            return MyEntities.GetEntities().OfType<MyCubeGrid>().SelectMany(grid =>
                grid.GetBlocks().Where(block => block.BlockDefinition.Id.TypeId == typeId)
                    .Where(block => block.BlockDefinition.Id.SubtypeName == subtypeId)
            ).ToList();
        }
        
        public static IEnumerable GetBlocksBySubtypeId(string subtypeId)
        {
            return GetBlocks(subtypeId);
        }
        public static IEnumerable GetRefineryBySubtypeId(string subtypeId)
        {
            return GetBlocks(BlockTypeBuilder.Refinery, subtypeId);
        }
        
        public static IEnumerable GetAssemblerBySubtypeId(string subtypeId)
        {
            return GetBlocks(BlockTypeBuilder.Assembler, subtypeId);
        }
        
        public static IEnumerable GetDrillBySubtypeId(string subtypeId)
        {
            return GetBlocks(BlockTypeBuilder.Drill, subtypeId);
        }
        
        public static IEnumerable GetWelderBySubtypeId(string subtypeId)
        {
            return GetBlocks(BlockTypeBuilder.Welder, subtypeId);
        }
        
        public static IEnumerable GetGrinderBySubtypeId(string subtypeId)
        {
            return GetBlocks(BlockTypeBuilder.Grinder, subtypeId);
        }
        
        public static IEnumerable GetBeaconBySubtypeId(string subtypeId)
        {
            return GetBlocks(BlockTypeBuilder.Beacon, subtypeId);
        }
    }
}