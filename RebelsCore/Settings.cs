﻿using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Weapons;
using Slime.Features;
using Slime.Features.Commands;
using Slime.Mod;
using Slime.Slow;
using System;
using System.Collections.Generic;
using Sandbox.ModAPI;
using Torch.API;
using Torch.Commands;
using NAPI;

namespace Slime
{
	internal class Settings {
        public static int Status = 0;
        public static string paidUtill = "unknown";
        public static string plan = "unknown";
        private static readonly List<Updatable> update1 = new List<Updatable>();
        private static readonly List<Updatable> removeUpdate1 = new List<Updatable>();

        public static void Loaded(ITorchBase Torch) { InitHacks(Torch); }

        public static void Unloaded() {
            SlowLogic.Clear();
            AutoToolsServer.Unload();
        }

        public static void Update() {
            FrameExecutor.Update();
            foreach (var x in update1) { x.Update1(); }

            if (removeUpdate1.Count <= 0) return;
            
            foreach (var x in removeUpdate1) { update1.Remove(x); }
        }

        public static void AddUpdateable(Updatable x) {
            MyAPIGateway.Utilities.InvokeOnGameThread(()=>update1.Add(x));
        }
        
        public static void RemoveUpdateable(Updatable x) {
            removeUpdate1.Add(x);
        }

        public static void AddDelayer(int ticks, Action toDo) {
            AddUpdateable(new Delayer (ticks, toDo));
        } 
        
        class Delayer : Updatable {
            private int ticks;
            private readonly Action toDo;

            public Delayer(int ticks, Action @do) {
                this.ticks = ticks;
                toDo = @do;
            }

            public void Update1() {
                if (ticks == 0) {
                    try { toDo(); } catch (Exception e) { RemoveUpdateable(this); }
                }
                ticks--;
            }
        }

        public static void InitHacks(ITorchBase Torch) {

			var cfg = DOP.Instance.Config;
            if (cfg.VoxelDamageTweaksEnabled || cfg.AntiRammingStations) { CustomDamageSystem.Init(); }

            string options;
            List<string> info;

            var _ctx = DependencyManager.instance._ctx;

            var manager = DOP.Instance.Torch.CurrentSession?.Managers.GetManager(typeof(CommandManager)) as CommandManager;
			manager.RegisterCommands(typeof(FixCharacter), DOP.Instance);

			if (cfg.EnabledSlowdownGrinder || cfg.EnabledSlowdownWelder)
			{
				_ctx.Redirect(typeof(MyShipToolBase), "UpdateAfterSimulation10");
				if (cfg.EnabledSlowdownGrinder) SlowLogic.blockHacks.Add(new HackGrinder());
				if (cfg.EnabledSlowdownWelder) SlowLogic.blockHacks.Add(new HackWelder());
			}

			if (cfg.EnabledSlowdownRefinery || cfg.EnabledSlowdownAssembler)
			{
				_ctx.Redirect(typeof(MyProductionBlock), "UpdateBeforeSimulation10");
				if (cfg.EnabledSlowdownAssembler) SlowLogic.blockHacks.Add(new HackAssembler());
				if (cfg.EnabledSlowdownRefinery) SlowLogic.blockHacks.Add(new HackRefinery());
			}

			if (cfg.EnabledSlowdownGasGen) SlowLogic.blockHacks.Add(new HackH2Gen());
			if (cfg.EnabledSlowdownGasTank) SlowLogic.blockHacks.Add(new HackGasTank());
			if (cfg.EnabledSlowdownCockpit) SlowLogic.blockHacks.Add(new HackCockpit());
			if (cfg.EnabledSlowdownLandingGear) SlowLogic.blockHacks.Add(new HackLanding());
			if (cfg.EnabledSlowdownParachute) SlowLogic.blockHacks.Add(new HackParachute());
			if (cfg.EnabledSlowdownSensor) SlowLogic.blockHacks.Add(new HackSensor());

			if (cfg.EnabledSlowdownSafezoneEnabled) OptimizationSafezone.InitHack(_ctx);

			if (cfg.SlowPBEnabled) { new SlowPB().InitHack(_ctx); }
			if (cfg.EnabledSlowdownThruster) { new HackThrust().InitHack(_ctx); }

			new Optimizations().InitHack(_ctx, cfg.Optimizations);

			foreach (var x in SlowLogic.blockHacks) { x.InitHack(_ctx); }

			SlowLogic.Init();
			update1.Add(new SlowLogic());

			if (cfg.RemoteDeleting) new RemoteDeleting().InitHack(_ctx);

			if (DOP.Instance.Config.FixProjector) new ProjectorFix().InitHack(_ctx);

			if (DOP.Instance.Config.ExplosionProtectionEnabled) new ExplosionFix().InitHack(_ctx);

			if (DOP.Instance.Config.FixTrees) new DrillTreesFix().InitHack(_ctx);
			
			
            
            
            
            if (DOP.Instance.Config.Safezone) {
                new SafezonePermissions2().InitHack(_ctx);
            }

            

            //Settings.update1.Add(new PingKickManager());
            DependencyManager.instance._patchManager.Commit();
        }
    }
}