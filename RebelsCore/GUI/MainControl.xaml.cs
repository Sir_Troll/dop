﻿using Slime.Features.Commands;
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace Slime.GUI
{
	public partial class MainControl : UserControl {
        public MainControl() {
            InitializeComponent();

            Func<PropertyInfo, DisplayTab, bool> f1 = (x, y) => y.Tab == "General";
            Func<PropertyInfo, DisplayTab, bool> f2 = (x, y) => y.Tab == "Optimizations";
            Func<PropertyInfo, DisplayTab, bool> f3 = (x, y) => y.Tab == "Commands" || y.Tab == "Features";
            Func<PropertyInfo, DisplayTab, bool> f4 = (x, y) => y.Tab == "API";
            Func<PropertyInfo, DisplayTab, bool> f5 = (x, y) => y.Tab == "AntiGrief";

            POptimizations.filter = f2;
            PCommands.filter = f3;
            PAPI.filter = f4;
            PAntiGrief.filter = f5;

            var cfg = DOP.Instance.Config;

            POptimizations.DataContext = cfg;
            PCommands.DataContext = cfg;
            PAPI.DataContext = cfg;
            PAntiGrief.DataContext = cfg;

            AuthKeyBox.Text = cfg.AuthKey;
            AuthKeyBox.TextChanged += (o, e) => { cfg.AuthKey = AuthKeyBox.Text; };


            var torch = DOP.Instance.Torch;
            TorchAutoStart.IsChecked = torch.Config.Autostart;
            TorchAutoStart.Checked += TorchAutoStart_Checked;
            TorchAutoStart.Unchecked += TorchAutoStart_Checked;

            UpdateStatus();

            PluginEnabledCheckBox.IsChecked = cfg.PluginEnabled;
            PluginEnabledCheckBox.Checked += (o, e) => { cfg.PluginEnabled = PluginEnabledCheckBox.IsChecked ?? false; };
            PluginEnabledCheckBox.Unchecked += (o, e) => { cfg.PluginEnabled = PluginEnabledCheckBox.IsChecked ?? false; };
        }

        public void UpdateStatus() {
            lock (typeof(Settings)) {
                StatusInfo.Text = Settings.Status == 0 ? "INITING" : Settings.Status == 1 ? "WORKING" : "DISABLED";
                Paid.Text = Settings.paidUtill + " / " + Settings.plan;
                
            }
        }

        private void TorchAutoStart_Checked(object sender, RoutedEventArgs e) {
            var torch = DOP.Instance.Torch;
            torch.Config.Autostart = TorchAutoStart.IsChecked ?? false;
            torch.Config.Save("Torch.cfg");
        }

        private void ExecuteCodeClick(object sender, RoutedEventArgs e) {
            try {
                InGameCompiler.CompileAndExecute(ExcutionCode.Text);
                DOP.Log.Error("Code executed successfully");
            } catch (Exception ee) {
                DOP.Log.Error(ee, "Error While executing");
                MessageBox.Show("There were errors. Check logs");
            }
        }

        private void OpenControlPanel(object sender, RoutedEventArgs e) { System.Diagnostics.Process.Start("http://nebula-se.com/DOP/session-info.html?key=" + DOP.Instance.Config.AuthKey); }
    }
}