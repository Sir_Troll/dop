using Sandbox.Game.World;
using VRage;
using VRage.Game;
using VRageMath;

namespace Slime {
    class BetterInventory : Sandbox.Game.MyInventory {
        private MyFixedPoint cacheMaxMass;
        private MyFixedPoint cacheMaxVolume;
        
        public BetterInventory(MyFixedPoint maxVolume, MyFixedPoint maxMass, Vector3 size, MyInventoryFlags flags) : base(maxVolume, maxMass, size, flags) {
            cacheMaxMass = MyFixedPoint.MultiplySafe(maxMass, MySession.Static.BlocksInventorySizeMultiplier);
            cacheMaxVolume = MyFixedPoint.MultiplySafe(maxMass, MySession.Static.BlocksInventorySizeMultiplier);
        }
        public override MyFixedPoint MaxMass => cacheMaxMass;
        public override MyFixedPoint MaxVolume => cacheMaxVolume;
        public override int MaxItemCount => int.MaxValue;
    }
}