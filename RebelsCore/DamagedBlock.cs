﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RebelsCore
{
    public class DamagedBlock
    {
        private int _blocksId;
        private DateTime _lastBlockDamageTime;

        public int BlocksId
        {
            get => _blocksId;
            set => _blocksId = value;
        }

        public DateTime LastBlockDamageTime
        {
            get => _lastBlockDamageTime;
            set => _lastBlockDamageTime = value;
        }

        public DamagedBlock(int blocksId)
        {
            _blocksId = blocksId;
        }

        public DamagedBlock(int blocksId, DateTime lastBlockDamageTime) : this(blocksId)
        {
            _lastBlockDamageTime = lastBlockDamageTime;
        }
    }
}
