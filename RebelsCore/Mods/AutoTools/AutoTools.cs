﻿using NAPI;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using VRage;
using VRage.Game;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRageMath;

namespace Slime.Mod {
    class AutoToolsServer {
        public static MyObjectBuilderType Ore = MyObjectBuilderType.Parse("MyObjectBuilder_Ore");

        public static Dictionary<string, float> containerRadiuses = new Dictionary<string, float>() {
                                                                                                        {"Welder", 18f},
                                                                                                        {"Welder2", 25f},
                                                                                                        {"Welder3", 32f},
                                                                                                        {"Welder4", 40f},
                                                                                                        {"AngleGrinder", 18f},
                                                                                                        {"AngleGrinder2", 25f},
                                                                                                        {"AngleGrinder3", 32f},
                                                                                                        {"AngleGrinder4", 40f},
                                                                                                        {"HandDrill", 28f},
                                                                                                        {"HandDrill2", 36f},
                                                                                                        {"HandDrill3", 44f} //,{ "HandDrill4", 32f }
                                                                                                    };

        public static double MAX_PICKUP_DISTANCE = 50; // 44f + 6f - distance from player
        public static double MAX_BIND_DISTANCE = 42;

        public static ushort CODE_DRILL = 34524;
        public static ushort CODE_FILLSTOCKPILE = 34525;
        public static ushort CODE_COLLECT_FLOATINGS = 34526;
        public static ushort CODE_MOVE_ITEMS = 34527;
        public static ushort CODE_BIND_INVENTORY = 34528;
        public static ushort CODE_UNBIND_INVENTORY = 34529;
        public static ushort CODE_FREESTOCKPILE = 34530;

        public static MyInventory TempInventory = new MyInventory(float.MaxValue, new Vector3(1000d, 10000d, 10000d), MyInventoryFlags.CanSend);

        public static void Init() {
            MyAPIGateway.Multiplayer.RegisterMessageHandler(CODE_DRILL, DeleteFloatings);
            MyAPIGateway.Multiplayer.RegisterMessageHandler(CODE_FILLSTOCKPILE, FillStockpile);
            MyAPIGateway.Multiplayer.RegisterMessageHandler(CODE_COLLECT_FLOATINGS, CollectFloatings);
            MyAPIGateway.Multiplayer.RegisterMessageHandler(CODE_MOVE_ITEMS, MoveItemsServer);
            MyAPIGateway.Multiplayer.RegisterMessageHandler(CODE_FREESTOCKPILE, PullItemsFromStockpile);
        }

        public static void Unload() {
            MyAPIGateway.Multiplayer.UnregisterMessageHandler(CODE_DRILL, DeleteFloatings);
            MyAPIGateway.Multiplayer.UnregisterMessageHandler(CODE_FILLSTOCKPILE, FillStockpile);
            MyAPIGateway.Multiplayer.UnregisterMessageHandler(CODE_COLLECT_FLOATINGS, CollectFloatings);
            MyAPIGateway.Multiplayer.UnregisterMessageHandler(CODE_MOVE_ITEMS, MoveItemsServer);
            MyAPIGateway.Multiplayer.UnregisterMessageHandler(CODE_FREESTOCKPILE, PullItemsFromStockpile);
        }

        protected static void PullItemsFromStockpile(byte[] data) {
            try {
                //var data = new byte[8+8+1+1+4+8];
                var playerId = BitConverter.ToInt64(data, 0);
                var gridId = BitConverter.ToInt64(data, 8);
                var whereId = BitConverter.ToInt64(data, 28);

                var player = Other.GetPlayer(playerId);
                if (player == null) return;

                if (player.Character == null) return;
                var grid = MyEntities.GetEntityById(gridId) as IMyCubeGrid;
                if (grid == null) return;
                var where = MyEntities.GetEntityById(whereId) as IMyTerminalBlock;
                if (where == null) return;

                if (!where.HasInventory) return;
                if (!where.IsFunctional) return;
                if (!where.HasPlayerAccess(playerId)) return;
                if ((where.WorldMatrix.Translation - player.Character.WorldMatrix.Translation).Length() > 400) return;

                var x = BitConverter.ToInt32(data, 16);
                var y = BitConverter.ToInt32(data, 20);
                var z = BitConverter.ToInt32(data, 24);

                var block = grid.GetCubeBlock(new Vector3I(x, y, z));
                if (block == null) {
                    return;
                }

                if ((block.GetWorldPosition() - player.Character.WorldMatrix.Translation).Length() > 15) return;
                block.MoveItemsFromConstructionStockpile(where.GetInventory());
            } catch (Exception e) { DOP.Log.Error(e); }
        }

        protected static void MoveItemsServer(byte[] data) {
            try {
                //var data = new byte[8+8+1+1+4+8];
                var fromId = BitConverter.ToInt64(data, 0);
                var toId = BitConverter.ToInt64(data, 8);
                var fromInv = data[16];
                var toInv = data[17];

                var itemId = BitConverter.ToUInt32(data, 18);
                var amount = BitConverter.ToInt64(data, 22);

                var from = MyEntities.GetEntityById(fromId);
                if (from == null) {
                    Common.SendChatMessage("from null" + fromId);
                    return;
                }

                var to = MyEntities.GetEntityById(toId);
                if (to == null) {
                    Common.SendChatMessage("to Null");
                    return;
                }

                var fromI = from.GetInventory(fromInv);
                var toI = to.GetInventory(toInv);

                if (fromI == null) {
                    Common.SendChatMessage("fromI Null");
                    return;
                }

                if (toI == null) {
                    Common.SendChatMessage("toI Null");
                    return;
                }

                var am2 = new MyFixedPoint();
                am2.RawValue = amount;

                MyFixedPoint? am = am2 <= MyFixedPoint.Zero ? (MyFixedPoint?) null : am2;

                var index = fromI.GetItemIndexByID(itemId);
                if (index >= 0) {
                    var rest = (fromI as IMyInventory).TransferItemTo(toI as IMyInventory, index, checkConnection: false, amount: am);
                }
            } catch (Exception e) {
                DOP.Log.Error("Error in MoveItemsServer:" + data[16] + " " + data[17]);
                DOP.Log.Error("Error in MoveItemsServer:" + e.Message + " " + e.StackTrace);
            }
        }

        public static void DeleteFloatings(byte[] bytes) {
            var ids = MyAPIGateway.Utilities.SerializeFromBinary<List<long>>(bytes);
            foreach (var x in ids) {
                IMyEntity ent;
                if (MyAPIGateway.Entities.TryGetEntityById(x, out ent)) {
                    var floaing = ent as MyFloatingObject;
                    if (floaing != null && floaing.Item.Content.TypeId == Ore) { floaing.Close(); }
                }
            }
        }

        public static void CollectFloatings(byte[] bytes) {
            var player = BitConverter.ToInt64(bytes, 0);
            var takeEntity = BitConverter.ToInt64(bytes, 8);
            IMyEntity takeEnt;
            IMyEntity floatEnt;

            if (!MyAPIGateway.Entities.TryGetEntityById(takeEntity, out takeEnt)) { return; }


            var maxD = MAX_PICKUP_DISTANCE * MAX_PICKUP_DISTANCE;
            for (var x = 16; x < bytes.Length - 7; x += 8) {
                var entId = BitConverter.ToInt64(bytes, x);
                if (MyAPIGateway.Entities.TryGetEntityById(entId, out floatEnt)) {
                    var floating = floatEnt as MyFloatingObject;
                    if (floating != null && (floating.WorldMatrix.Translation - takeEnt.WorldMatrix.Translation).LengthSquared() < maxD) { (takeEnt.GetInventory() as MyInventory).TakeFloatingObject(floating); }
                }
            }
        }

        public static void FillStockpile(byte[] bytes) {
            try {
                var g = BitConverter.ToInt64(bytes, 0);
                var player = BitConverter.ToInt64(bytes, 8);
                var pX = BitConverter.ToInt32(bytes, 16);
                var pY = BitConverter.ToInt32(bytes, 20);
                var pZ = BitConverter.ToInt32(bytes, 24);


                var grid = MyEntities.GetEntityByIdOrDefault(g) as IMyCubeGrid;
                IMySlimBlock block = null;

                if (grid == null) return;
                block = grid.GetCubeBlock(new Vector3I(pX, pY, pZ));
                if (block == null) { return; }

                for (var x = 28; x + 15 < bytes.Length; x += 16) {
                    try {
                        var entId = BitConverter.ToInt64(bytes, x);
                        var itemId = BitConverter.ToUInt32(bytes, x + 8); 
                        var amount = BitConverter.ToInt32(bytes, x + 12);

                        IMyEntity ent;
                        MyAPIGateway.Entities.TryGetEntityById(entId, out ent);
                        if (ent != null) {
                            for (var i = 0; i < ent.InventoryCount; i++) {
                                var inv = ent.GetInventory(i);
                                var index = inv.GetItemIndexByID(itemId);
                                if (index >= 0) {
                                    inv.TransferItemTo(TempInventory, index, amount: amount, checkConnection: false);
                                    break;
                                }
                            }
                        }
                    } catch (Exception e) {
                        //Common.ShowNotification ("E1", 2000);
                        DOP.Log.Error(e, "Couldn't move stockpile 1");
                    }
                }

                //Common.ShowNotification ("Took needed components", 2000, playerId: player);
                block.MoveItemsToConstructionStockpile(TempInventory);

                if (TempInventory.ItemCount > 0) { MoveBack(bytes); }

                TempInventory.ClearItems();
            } catch (Exception ee) {
                DOP.Log.Error(ee);
                //Common.ShowNotification ("E2" + ee.Message +" "+ ee.StackTrace, 2000);
                //Log.Error (ee, "Couldn't move stockpile 2");
            }
        }

        private static void MoveBack(byte[] bytes) {
            for (var x = 28; x + 15 < bytes.Length; x += 16) {
                try {
                    var entId = BitConverter.ToInt64(bytes, x);
                    IMyEntity ent;
                    MyAPIGateway.Entities.TryGetEntityById(entId, out ent);
                    if (ent != null) {
                        var cargo = ent as IMyCargoContainer;
                        if (cargo != null) {
                            var inv = ent.GetInventory();
                            inv.MoveAllItemsFrom(TempInventory);
                            if (TempInventory.ItemCount == 0) return;
                        }
                    }
                } catch (Exception e) { DOP.Log.Error(e); }
            }
        }
    }
}