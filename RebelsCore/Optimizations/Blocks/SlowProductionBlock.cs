﻿using System.Reflection;
using Sandbox;
using Sandbox.Game.Entities.Cube;

namespace Slime.Features {
    abstract class SlowProductionBlock : SlowdownHack {
        public static readonly FieldInfo delta = typeof(MyProductionBlock).GetField("m_lastUpdateTime", BindingFlags.NonPublic | BindingFlags.Instance);
        public MyProductionBlock block;
        public int extraTime = 0;
        public float powerBase = 0;

        public SlowProductionBlock(MyProductionBlock block) : base(block) { this.block = block; }

        public void buff(int extratime) { this.extraTime += extraTime; }

        public void setDelta(int delt, int MAX_DELTA_TIME) {
            var value = (int) delta.GetValue(block);
            delta.SetValue(block, value);
        }

        public void fixProduction(int MAX_DELTA_TIME) {
            if (delta == null) {
                DOP.Log.Error("Production block error : m_lastUpdateTime in unknown");
                return;
            }

            var value = (int) delta.GetValue(block);
            var now = MySandboxGame.TotalGamePlayTimeInMilliseconds;
            var delt = now - value;
        }
    }
}