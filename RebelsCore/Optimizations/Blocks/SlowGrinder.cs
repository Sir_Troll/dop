﻿using NAPI;
using Sandbox.Game.Weapons;
using Sandbox.ModAPI;
using Torch.Managers.PatchManager;
using VRage.Game;
using VRage.Game.ModAPI;


namespace Slime.Features
{
	class HackGrinder : Hack {
        public static int INTERVAL_GRINDER = 10;

        public override void InitHack(PatchContext patchContext) {
            INTERVAL_GRINDER = DOP.Instance.Config.EnabledSlowdownGrinderInterval;
            MyAPIGateway.Session.DamageSystem.RegisterBeforeDamageHandler(1, HandleDamage);
        }

        public static void HandleDamage(object target, ref MyDamageInformation damage) {
            if (damage.Type == MyDamageType.Grind) {
                if (target is IMySlimBlock) {
                    var attacker = MyAPIGateway.Entities.GetEntityById(damage.AttackerId);
                    if (attacker is IMyCubeBlock) { damage.Amount *= DOP.Instance.Config.EnabledSlowdownGrinderBoost; }
                }
            }
        }

        public override void CreateHack(IMyCubeBlock block) { new SlowGrinder(block as MyShipToolBase); }
        public override bool validateFat(IMyCubeBlock block, string sn) { return (block is MyShipGrinder) && base.validateFat(block, sn); }
        public override void Clear() { }
    }


    class SlowGrinder : SlowdownHack {
        public AutoTimer timer = new AutoTimer(HackGrinder.INTERVAL_GRINDER, DOP.Random.Next(HackGrinder.INTERVAL_GRINDER));
        public SlowGrinder(MyShipToolBase block) : base(block) { }

        protected override bool After10() {
            return !DOP.Instance.Config.EnabledSlowdownGrinder || timer.tick();
        }
    }
}