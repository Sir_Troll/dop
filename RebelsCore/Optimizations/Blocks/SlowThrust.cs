﻿using System;
using System.Collections.Generic;
using NAPI;
using Sandbox.Game.Entities;
using Torch.Managers.PatchManager;

namespace Slime.Features
{
	class HackThrust : Hack {
        private static Dictionary<long, AutoTimer> refs = new Dictionary<long, AutoTimer>();
        private static readonly int INTERVAL_DAMAGE = 10;

        public override void InitHack(PatchContext patchContext) {
            patchContext.Disable(typeof(MyThrust), "UpdateAfterSimulation");
            patchContext.Disable(typeof(MyThrust), "UpdateAfterSimulation10");
            patchContext.Disable(typeof(MyThrust), "UpdateAfterSimulation100");

            if (DOP.Instance.Config.EnabledThrusterDamage) { patchContext.Prefix(typeof(MyThrust), "UpdateBeforeSimulation100", typeof(HackThrust), new[] {"__instance"}); } else { patchContext.Disable(typeof(MyThrust), "UpdateBeforeSimulation100"); }
        }

        public static bool UpdateBeforeSimulation100(MyThrust __instance) {
            if (DOP.Instance.Config.EnabledThrusterDamage) {
                try { return getOrCreate(__instance).tick(); } catch (Exception e) { Log.Fatal(e, "SlowThruster"); }
            }

            return false;
        }

        private static AutoTimer sharedTimer = new AutoTimer(INTERVAL_DAMAGE, DOP.Random.Next(INTERVAL_DAMAGE));

        private static AutoTimer getOrCreate(MyThrust __instance) {
            if (!HackThrust.refs.ContainsKey(__instance.EntityId)) {
                try {
                    var sl = new AutoTimer(INTERVAL_DAMAGE, DOP.Random.Next(INTERVAL_DAMAGE));
                    __instance.BlockDefinition.FlameDamage *= INTERVAL_DAMAGE;
                    HackThrust.refs.Add(__instance.EntityId, sl);
                    return sl;
                } catch (Exception e) { return sharedTimer; }
            } else {
                //DOP.LogSpamming(236461, (x,y)=>x.Append("Thrust"));
                return HackThrust.refs[__instance.EntityId];
            }
        }
    }
}