﻿using System.Reflection;
using NAPI;
using Sandbox.Game.Weapons;
using SpaceEngineers.Game.Entities.Blocks;
using Torch.Managers.PatchManager;
using VRage.Game.ModAPI;

namespace Slime.Features
{
	class HackWelder : Hack {
        public override void InitHack(PatchContext patchContext) {
            var method = typeof(MyShipWelder).GetField("WELDER_AMOUNT_PER_SECOND", BindingFlags.Static | BindingFlags.Public | BindingFlags.IgnoreCase);
            var value = method.GetValue(null) as float? ?? 4f;
            method.SetValue(null, value * DOP.Instance.Config.EnabledSlowdownWelderBoost);
        }

        public override void CreateHack(IMyCubeBlock block) { new SlowWelder(block as MyShipToolBase); }
        public override bool validateFat(IMyCubeBlock block, string sn) { return (block is MyShipWelder) && base.validateFat(block, sn); }
        public override void Clear() { }
    }

    class SlowWelder : SlowdownHack {
        AutoTimer timer = new AutoTimer(DOP.Instance.Config.EnabledSlowdownWelderInterval, DOP.Random.Next(DOP.Instance.Config.EnabledSlowdownWelderInterval));
        public SlowWelder(MyShipToolBase block) : base(block) { }

        protected override bool After10() {
            return !DOP.Instance.Config.EnabledSlowdownGrinder || timer.tick();
        }
    }
}