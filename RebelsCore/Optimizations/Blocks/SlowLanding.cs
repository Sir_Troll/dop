﻿using System;
using NAPI;
using SpaceEngineers.Game.Entities.Blocks;
using Torch.Managers.PatchManager;
using VRage.Game.ModAPI;

namespace Slime.Features
{
	class HackLanding : Hack {
        public override void CreateHack(IMyCubeBlock block) { new SlowLanding(block as MyLandingGear); }

        public override void InitHack(PatchContext patchContext) {
            //patchContext.Prefix(typeof(MyLandingGear), "UpdateBeforeSimulation", typeof(SlowLanding), "UpdateBeforeSimulation");
            //patchContext.Prefix(typeof(MyLandingGear), "UpdateAfterSimulation10", typeof(SlowLanding), new[] {"__instance"});
        }

        public override bool validateFat(IMyCubeBlock block, string sn) { return (block is MyLandingGear) && base.validateFat(block, sn); }
    }

    class SlowLanding : SlowdownHack {
        private AutoTimer timer10 = new AutoTimer(4, DOP.Random.Next(4));
        public SlowLanding(MyLandingGear block) : base(block) { }

        protected override bool After10() {
            return !DOP.Instance.Config.EnabledSlowdownLandingGear || timer10.tick();
        }

        private static SlowdownHack getOrCreate(object g) {
            var gear = g as MyLandingGear;
            if (SlowLogic.refsHack.ContainsKey(gear.EntityId)) return SlowLogic.refsHack[gear.EntityId];
            try {
                var sl = new SlowLanding(gear);
                SlowLogic.refsHack.Add(gear.EntityId, sl);
                return sl;
            } catch (Exception e) {
                return null;
            }
        }

        public static bool UpdateAfterSimulation10(object __instance) {
            return (getOrCreate((__instance)) as SlowLanding)?.After10() ?? true;
        }
    }
}