﻿using System.Collections.Generic;
using NAPI;
using Sandbox.Definitions;
using Sandbox.Game.Entities.Blocks;
using Torch.Managers.PatchManager;
using VRage.Game.ModAPI;

namespace Slime.Features
{
	class HackH2Gen : Hack {
        public override void CreateHack(IMyCubeBlock block) { new SlowH2Gen(block as MyGasGenerator); }

        public override void InitHack(PatchContext patchContext) { patchContext.Redirect(typeof(MyGasGenerator), "UpdateAfterSimulation", "UpdateAfterSimulation100"); }

        public override bool validateFat(IMyCubeBlock block, string sn) { return (block is MyGasGenerator) && base.validateFat(block, sn); }

        public override void Clear() { SlowH2Gen.hacked.Clear(); }
    }

    class SlowH2Gen : SlowdownHack {
        public static int INTERVAL_H2 = 240;
        public static HashSet<MyCubeBlockDefinition> hacked = new HashSet<MyCubeBlockDefinition>();

        private AutoTimer timer = new AutoTimer(INTERVAL_H2, DOP.Random.Next(INTERVAL_H2));
        private AutoTimer timer2 = new AutoTimer(5, DOP.Random.Next(5));

        public SlowH2Gen(MyGasGenerator block) : base(block) {
            (block as Sandbox.ModAPI.IMyGasGenerator).ProductionCapacityMultiplier *= INTERVAL_H2;
        }
        protected override bool After1() { return !DOP.Instance.Config.EnabledSlowdownGasGen || timer.tick(); }
        protected override bool After100() { return !DOP.Instance.Config.EnabledSlowdownGasGen || timer2.tick(); }
    }
}