﻿using System.Collections.Generic;
using Sandbox.Definitions;
using Sandbox.Game.Entities.Cube;
using Torch.Managers.PatchManager;
using VRage.Game.ModAPI;
using NAPI;

namespace Slime.Features
{
	class HackRefinery : Hack {
        public override void CreateHack(IMyCubeBlock block) { new SlowRefinery(block as MyRefinery); }
        public override bool validateFat(IMyCubeBlock block, string sn) { return (block is MyRefinery) && base.validateFat(block, sn); }

        public override void InitHack(PatchContext patchContext) { }

        public override void Clear() { SlowRefinery.hacked.Clear(); }
    }

    class SlowRefinery : SlowProductionBlock {
        public const int SLOW = 30;
        public const int MAX_SPEED_BOOST = 3;
        public const int MAX_DELTA_TIME = (int) ((SLOW * 10 * 16.6d) * MAX_SPEED_BOOST); //300 frames * 16.6 ms * 10 times

        public static HashSet<MyCubeBlockDefinition> hacked = new HashSet<MyCubeBlockDefinition>();

        private AutoTimer timer = new AutoTimer(SLOW, DOP.Random.Next(SLOW));

        //public static bool UpdateBeforeSimulation(MyEntity __instance) { return get(__instance)?.Before1() ?? true; }

        public SlowRefinery(MyRefinery block) : base(block) {
            this.block = block;
            Init(block);
        }

        protected override bool Before10() {
            if (!DOP.Instance.Config.EnabledSlowdownRefinery) return true;
            if (block == null) {
                DOP.Log.Error("Refinery block is null");
                return true;
            }

            if (block.OutputInventory == null) { //Projected / Closed just skip
                return true;
            }
            
            return timer.tick();
        }

        public void Init(MyRefinery refinery) {
            if (!hacked.Contains(block.BlockDefinition)) {
                hacked.Add(block.BlockDefinition);
                ((MyRefineryDefinition) refinery.BlockDefinition).OreAmountPerPullRequest *= MAX_SPEED_BOOST;
            }
        }
    }
}