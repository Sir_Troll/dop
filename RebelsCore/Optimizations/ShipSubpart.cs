﻿using System.Collections.Generic;
using System.Diagnostics;
using NAPI;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.Weapons;
using Sandbox.ModAPI;

namespace Slime.Features
{
	//9e333c09-e4c3-4aef-bd7b-36cfdf983ce2
	public class ShipSubpart : SlowdownShip {
        private static Stopwatch sw = new Stopwatch();
        
        public HashSet<MyLargeTurretBase> turrets;
        public HashSet<MyProductionBlock> productionBlocks;
        public HashSet<MyCryoChamber> cryos;
        public HashSet<MyProjectorBase> projectors;
        public HashSet<MyCubeBlock> excluded = new HashSet<MyCubeBlock>();
        public HashSet<IMyFunctionalBlock> excludedWorking = new HashSet<IMyFunctionalBlock>();
        //private static List<IMyBlockGroup> blockGroups = new List<IMyBlockGroup>();
        //private static List<IMyFunctionalBlock> blocksToActivate = new List<IMyFunctionalBlock>();

        public ShipSubpart(MyCubeGrid grid, HashSet<MyLargeTurretBase> turrets, HashSet<MyProductionBlock> productionBlocks, HashSet<MyCryoChamber> cryos, HashSet<MyProjectorBase> projectors, HashSet<MyCubeBlock> excluded, HashSet<IMyFunctionalBlock> excludedWorking) : base(grid) {
            this.turrets = turrets;
            this.productionBlocks = productionBlocks;
            this.cryos = cryos;
            this.projectors = projectors;
            this.excluded = excluded;
            this.excludedWorking = excludedWorking;
        }
        
        public override void Tick() {
            
        }

        public override void Hack() { /** DO NOTHING **/ }

        public void BlockAddedOrRemoved(MyCubeBlock block, bool added) {
            
            switch (block) {
                case MyLargeTurretBase bb: {
                    turrets.AddOrRemove(bb, added);
                    break;
                }
                case MyProductionBlock bb: {
                    productionBlocks.AddOrRemove(bb, added);
                    break;
                }
                case MyCryoChamber bb: {
                    cryos.AddOrRemove(bb, added);
                    break;
                }
                case MyProjectorBase bb: {
                    projectors.AddOrRemove(bb, added);
                    break;
                }
            }

            var sn = block.BlockDefinition.Id.SubtypeName;
        }

        public bool HasWorkingProduction() {
            foreach (var pp in productionBlocks) {
                if (pp.IsProducing) { return true; }
            }

            return false;
        }
        
        public bool IsExcluded() {
            if (excluded.Count > 0) return true;
            foreach (var x in excludedWorking) {
                if (x.IsWorking) { return true; }
            }

            return false;
        }
    }
}