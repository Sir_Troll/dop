﻿using System;

namespace NAPI
{
    class Disabled
    {
        public static bool DisableMethod(Object __instance)
        {
            return false; //!DOP.Instance.Config.EnabledExtra;
        }

        public static bool DisableMethodStatic()
        {
            return false; //!DOP.Instance.Config.EnabledExtra;
        }
    }
}