﻿using NAPI;

namespace SharedLibTorch
{
    public class TorchLogger : ILogger
    {
        NLog.ILogger logger;
        public TorchLogger(NLog.ILogger logger)
        {
            this.logger = logger;
        }

        public void Info(string s)
        {
            logger.Info (s);
        }

        public void Warn(string s)
        {
            logger.Warn(s);
        }

        public void Write(string s)
        {
            logger.Error(s);
        }
    }
}
