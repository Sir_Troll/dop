﻿using Sandbox.Engine.Physics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torch.Managers.PatchManager;

namespace NAPI
{
    public static class PhysicsTracker
    {
        private static Stopwatch stopwatch = new Stopwatch();
        public static TimeSpan elapsed = TimeSpan.Zero;
        private static bool patched = false;

        public static bool SimulateStart(Object __instance)
        {
            stopwatch.Start();
            return true;
        }

        public static void SimulateEnd(Object __instance)
        {
            stopwatch.Stop();
            elapsed = stopwatch.Elapsed;
            stopwatch.Reset();
        }

        public static void Init(PatchContext patchContext)
        {
            lock (PhysicsTracker.stopwatch)
            {
                if (!patched)
                {
                    patched = true;
                    patchContext.Prefix(typeof(MyPhysics), "Simulate", typeof(PhysicsTracker), "SimulateStart");
                    patchContext.Suffix(typeof(MyPhysics), "Simulate", typeof(PhysicsTracker), "SimulateEnd");
                }
            }
        }
    }
}
