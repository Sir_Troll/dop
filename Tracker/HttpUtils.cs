﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using TorchPlugin;

namespace NAPI
{
    class HttpUtils
    {
        private static int packetId = 0;
        public static int CRASH = 0;
        public static int PLAYERS_SIM = 1;
        public static int LOG_FATAL = 2;
        public static int LOG = 3;
        public static int CUSTOM = 4;

        public static void Track(int type, String data, bool async = true)
        {
            try
            {
                var pars = new Dictionary<string, string> { { "sid", "" + TrackerPlugin.SessionId }, { "packet", "" + (packetId++) }, { "data", "" + data }, { "type", "" + type } };
                SendRequest("http://nebula-se.com/DOP/track.php", pars, new List<string>() { "sid" }, out var time, 10000, async: async);
            }
            catch (Exception e)
            {
                Log.LogSpamming(234522, (x, y) => x.Append("Couldn't send track:" + e.Message));
            }
        }


        public static string SendRequestInternal(string url, Dictionary<string, string> pars, List<String> hashed, out string time, int timeoutMs, bool waitAnswer, bool async, int attempts)
        {
            while (attempts > 0)
            {
                attempts--;
                try
                {
                    time = "" + SharpUtils.timeStamp();
                    HttpClient client = new HttpClient();
                    var key = TrackerPlugin.Config.AuthKey;

                    var version = "1.4.4";

                    var toHash = "dont_be_salty_" + time + version + key;
                    foreach (var x in hashed) { toHash += pars[x]; }

                    toHash += "_please";
                    var hash = CreateMD5(toHash);

                    var values = new Dictionary<string, string> { { "key", key }, { "time", time }, { "version", version }, { "hash", hash } };

                    foreach (var x in pars) { values.Add(x.Key, x.Value); }

                    var rr = client.PostAsync(url, new FormUrlEncodedContent(values));
                    if (async) return null;
                    var started = DateTime.Now;
                    var awaiter = rr.GetAwaiter();

                    while (true)
                    {
                        Thread.Sleep(50);
                        if (awaiter.IsCompleted) break;
                        if ((DateTime.Now - started).TotalMilliseconds > timeoutMs) throw new TimeoutException();
                    }

                    if (!waitAnswer) { return null; }

                    var response = awaiter.GetResult().Content.ReadAsStringAsync();
                    var awaiter2 = response.GetAwaiter();

                    while (true)
                    {
                        Thread.Sleep(50);
                        if (awaiter2.IsCompleted) break;
                        if ((DateTime.Now - started).TotalMilliseconds > timeoutMs) break;
                    }

                    if (!awaiter2.IsCompleted) { throw new TimeoutException(); }

                    return response.Result;
                }
                catch (Exception e)
                {
                    Log.Error(e);
                    if (attempts <= 0) throw e;
                }
            }

            time = "error";
            return "error";
        }

        public static string SendRequest(string url, Dictionary<string, string> pars, List<String> hashed, out string time, int timeoutMs = 10000, bool waitAnswer = true, bool async = false, int attempts = 1)
        { //
            var result = SendRequestInternal(url, pars, hashed, out var time2, timeoutMs, waitAnswer, async, attempts);
            time = time2;
            return result;
        }


        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++) { sb.Append(hashBytes[i].ToString("x2")); }

                return sb.ToString();
            }
        }
    }
}