﻿using Torch.Managers.PatchManager;
using System.Windows.Controls;
using Torch.API;
using Slime.Config;
using Torch;
using System.IO;
using RebelsCore.Features;

namespace TorchPlugin
{
    public class TrackerPlugin : CommonPlugin
    {
        public static long SessionId;
        public static Persistent<CoreConfig> _config;
        public static CoreConfig Config => _config?.Data;

        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            _config = Persistent<CoreConfig>.Load(Path.Combine(StoragePath, "Tracker.cfg"));
        }

        public override UserControl CreateControl()
        {
            return null;
        }

        public override void Update()
        {
            base.Update();
            if (Config.TrackEnabled) Tracker.Update();
        }

        public override void Patch(PatchContext patchContext)
        {
            Tracker.Init(patchContext);
        }
    }
}