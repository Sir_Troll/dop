﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Management;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Torch.API.Managers;
using Torch.Server.Managers;
using Torch.Server.ViewModels;
using NAPI;
using TorchPlugin;

namespace Slime.Help
{
    static class Protection
    {
        public static HttpClient client = new HttpClient();
        public static int SessionId = -1;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ConfigDedicatedViewModel GetConfig() { return TrackerPlugin.Torch.Managers.GetManager<InstanceManager>().DedicatedConfig; }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static List<string> httpProtection()
        {
            var pars = new Dictionary<string, string>();
            var cfg = TrackerPlugin.Config;
            var cfg3 = GetConfig();

            var settings = "n=" + cfg3.ServerName;

            var sb = new StringBuilder();

            Ext2.PrintProperties(sb, cfg);
            settings += sb.ToString();


            var worldName = Regex.Replace(TrackerPlugin.Torch.CurrentSession.KeenSession.Name, "[^\\p{L}\\d\\s]+", "");
            if (worldName.Length == 0) { throw new Exception("Your world name doesn't contain any letter"); }

            pars.Add("world", worldName);
            pars.Add("pc", PCInfo());
            pars.Add("settings", settings);

            var result = HttpUtils.SendRequest("http://nebula-se.com/DOP/auth-v5.php", pars, new List<string> { "world", "pc" }, out var time, 30000, true, false, 3);
            var me = HttpUtils.CreateMD5(HttpUtils.CreateMD5(TrackerPlugin.Config.AuthKey + time + "_slime") + "_salty");

            var answer = result.Split(',');
            if (me != answer[0])
            {
                //DOP.Log.Error("ANSWER WAS:[" + result + "] / [" + me + "][" + DOP.Instance.Config.AuthKey + "]");
                throw new Exception("Your authkey expired or wrong");
            }

            var md2 = HttpUtils.CreateMD5(HttpUtils.CreateMD5(time + answer[3] + "_radio") + "_salty");
            if (md2 != answer[2])
            {
                throw new Exception("Not so easy");
            }

            Log.Error("SESSION ID:" + answer[1] + " PLAN: " + answer[3] + " ENDS: " + answer[4] + " ABOUT: " + answer[5]);
            SessionId = int.Parse(answer[1]);
            return new List<String>() { answer[3], answer[4] };
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void timeProtection()
        {
            //if (DateTime.Now > new DateTime(2020, 7, 1)) { throw new Exception("You are using illegal version of plugin (Error=2)"); }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static string PCInfo()
        {
            StringBuilder sb = new StringBuilder();

            ManagementClass managClass = new ManagementClass("win32_processor");
            ManagementObjectCollection managCollec = managClass.GetInstances();
            foreach (var o in managCollec)
            {
                var managObj = (ManagementObject)o;
                var name = managObj.TryGet("Name");
                sb.Append("name=").Append(name);
                break;
            }

            try
            {
                double totalCapacity = 0;
                ObjectQuery objectQuery = new ObjectQuery("select * from Win32_PhysicalMemory");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(objectQuery);
                ManagementObjectCollection vals = searcher.Get();
                foreach (var o in vals)
                {
                    var val = (ManagementObject)o;
                    totalCapacity += Convert.ToDouble(val.GetPropertyValue("Capacity"));
                }

                sb.Append("|memory=").Append(totalCapacity / 1048576);
            }
            catch (Exception)
            { // ignored
            }


            return sb.ToString();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static string TryGet(this ManagementObject data, string prop)
        {
            try { return data.Properties[prop].Value?.ToString(); } catch (Exception e) { return null; }
        }
    }
}