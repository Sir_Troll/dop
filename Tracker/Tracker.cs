﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Torch.Managers.PatchManager;
using Sandbox.Game.World;
using VRage.Game.ModAPI;
using Sandbox;
using Sandbox.Engine.Physics;
using NAPI;
using TorchPlugin;
using Sandbox.ModAPI;

namespace RebelsCore.Features
{



    public class Tracker
    {
        private static DateTime lastSent = DateTime.Now;

        public static void Init(PatchContext patchContext)
        {
            lastSent = DateTime.Now;
            if (TrackerPlugin.Config.TrackCrashes)
            {
                patchContext.Prefix(typeof(Torch.Server.Initializer), "HandleException", typeof(Tracker), new[] { "__instance", "sender", "e" });
            }
        }

        static double load_min = 100000;
        static double load_max = -100000;
        static double load_total;
        static double phys_total;
        static int load_took;

        static StringBuilder sb = new StringBuilder();
        static List<IMyPlayer> players = new List<IMyPlayer>();

        public static void Update()
        {
            //Thread.CurrentThread.ManagedThreadId == MySandboxGame.Static.UpdateThread.ManagedThreadId;
            if (MySession.Static.ServerSaving)
            {
                return;
            }

            var load = MySandboxGame.Static.CPULoad;
            if (load < 0)
            {
                return;
            }

            load_total += load;
            load_took++;
            load_max = Math.Max(load_max, load);
            load_min = Math.Min(load_min, load);
            phys_total += PhysicsTracker.elapsed.TotalMilliseconds; ;

            if ((DateTime.Now - lastSent).TotalSeconds > TrackerPlugin.Config.TrackInterval)
            {
                lastSent = DateTime.Now;


                sb.Append("lmin=").Append(Math.Round(load_min)).Append("\nlmax=").Append(Math.Round(load_max)).Append("\nlavrg=").Append(Math.Round(load_total / load_took));

                if (TrackerPlugin.Config.TrackPlayersCount)
                {
                    MyAPIGateway.Players.GetPlayers(players);
                    sb.Append("\npl=").Append(players.Count);
                }

                if (TrackerPlugin.Config.TrackPhysics)
                {
                    sb.Append("\nphys=").AppendFormat("{0:N2}", (phys_total / load_took));
                }

                //var fr = DOP.Instance.freezer;
                //if (fr.Config.Enabled) {
                //    var c = (float) fr.Concealed.Count / (float) MyCubeGridGroups.Static.Physical.Groups.Count;
                //    sb.Append("\nfr=").Append(c);
                //}

                HttpUtils.Track(HttpUtils.PLAYERS_SIM, sb.ToString());
                sb.Clear();
                players.Clear();
                load_min = 100000;
                load_max = -100000;
                load_total = 0;
                load_took = 0;
                phys_total = 0;
            }
        }

        public static bool HandleException(Object __instance, object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                var ex = (Exception)e.ExceptionObject;

                if (ex != null)
                {
                    var s = ex.ToString();

                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                        s += "\r\n" + ex.ToString();
                    }

                    HttpUtils.Track(HttpUtils.CRASH, s, async: false);
                }
                return true;
            }
            catch (Exception ee)
            {
                Log.Error(ee, "DOP:Couldn't send log");
                return true;
            }
        }


    }
}