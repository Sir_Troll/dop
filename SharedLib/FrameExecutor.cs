using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace NAPI
{
    public static class FrameExecutor
    {
        private static int frame = 0;
        public static int currentFrame { get { return frame; } }

        private static readonly List<Pair<Action1<long>, bool>> onEachFrameLogic = new List<Pair<Action1<long>, bool>>();
        private static bool needRemoveFrameLogic = false;

        public static void Update()
        {
            foreach (var x in onEachFrameLogic)
            {
                if (!x.v) continue;
                try
                {
                    x.k.run(frame);
                } catch (Exception e)
                {
                    Log.LogSpamming (7243752, (sb, t)=>sb.Append("Error while executing:").Append(x.k.GetType()).Append(e));
                }
                
            }

            frame++;

            if (needRemoveFrameLogic)
            {
                needRemoveFrameLogic = false;
                var l = onEachFrameLogic.Count;
                for (var x = 0; x < l; x++)
                {
                    var kk = onEachFrameLogic[x];
                    if (!kk.v)
                    {
                        onEachFrameLogic.RemoveAt(x);
                        x--;
                        l--;
                    }
                }
            }
        }

        public static void addFrameLogic(Action1<long> action)
        {
            if (action == null)
            {
                Log.Error ("Action is null at:" + new StackTrace().ToString());
                return;
            }
            onEachFrameLogic.Add(new Pair<Action1<long>, bool>(action, true));
        }
        public static void removeFrameLogic(Action1<long> action)
        {
            needRemoveFrameLogic = true;
            foreach (var x in onEachFrameLogic)
            {
                if (x.k == action)
                {
                    x.v = false;
                    return;
                }
            }
        }

        public static void addDelayedLogic(long frames, Action1<long> action)
        {
            onEachFrameLogic.Add(new Pair<Action1<long>, bool>(new DelayerAction(frames, action), true));
        }

        private class DelayerAction : Action1<long>
        {
            private long timer;
            private Action1<long> action;
            public DelayerAction(long timer, Action1<long> action)
            {
                this.timer = timer;
                this.action = action;
            }


            public void run(long k)
            {
                if (timer > 0)
                {
                    timer--; return;
                }
                FrameExecutor.removeFrameLogic(this);
                action.run(k);
            }
        }
    }
}