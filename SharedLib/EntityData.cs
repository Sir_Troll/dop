﻿using System;
using Sandbox.ModAPI;
using VRageMath;

namespace NAPI
{
    public class SimpleData
    {
        public double d1, d2, d3;
        public long l1, l2, l3;
        public string s1, s2, s3;
        public Vector3 v1, v2, v3;

        public String Serialize()
        {
            return MyAPIGateway.Utilities.SerializeToXML(this);
        }

        public static SimpleData Deserialize(string s)
        {
            return MyAPIGateway.Utilities.SerializeFromXML<SimpleData>(s);
        }
    }
}