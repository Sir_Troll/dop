using System;
using System.Collections.Generic;
using NAPI;
using Sandbox.Definitions;
using VRageMath;
using VRage.Game.ModAPI;
using Sandbox.Game.Entities;
using VRage.Game.ObjectBuilders.Definitions.SessionComponents;

namespace Scripts.Shared {
    /// <summary>
    /// Very old class. Unreleable. Only `IsInVoxels(MatrixD worldMatrix, BoundingBoxD LocalAABB, float maxAlowed = 0.2f)` - is ok
    /// </summary>
    public static class Voxels {

        public static bool IsInVoxels(MatrixD worldMatrix, BoundingBoxD LocalAABB, float maxAlowed = 0.2f) //TODO TEST
        {
            try
            {
                var grid_vsettings = new VoxelPlacementSettings();
                grid_vsettings.PlacementMode = VoxelPlacementMode.Volumetric;
                grid_vsettings.MaxAllowed = maxAlowed;
                grid_vsettings.MinAllowed = 0;

                MyGridPlacementSettings grid_settings = new MyGridPlacementSettings();//WTF HERE?? grid_settings это настройка проверки с заполнением вокселями блока в 20% для быстрой проверки
                grid_settings.CanAnchorToStaticGrid = true;
                grid_settings.EnablePreciseRotationWhenSnapped = true;
                grid_settings.SearchHalfExtentsDeltaAbsolute = 0;
                grid_settings.SearchHalfExtentsDeltaRatio = 0;
                grid_settings.SnapMode = SnapMode.Base6Directions;
                grid_settings.VoxelPlacement = grid_vsettings;

                MyGridPlacementSettings settings = new MyGridPlacementSettings();//WTF HERE?? settings это настройка проверки с заполнением вокселями блока в 95% для медленной проверки потому что после создания свойство VoxelPlacementSettings.MaxAllowed не поменять

                grid_settings = settings;//WTF HERE??

                var vsettings = new VoxelPlacementSettings();
                vsettings.PlacementMode = VoxelPlacementMode.Volumetric;
                vsettings.MaxAllowed = 0.95f;
                vsettings.MinAllowed = 0;
                settings.VoxelPlacement = vsettings;

                return MyCubeGrid.IsAabbInsideVoxel(worldMatrix, LocalAABB, grid_settings);
            }
            catch (Exception e)
            {
                Log.Error(e, "[Player_SOS_Rescue] ");
                return false;
            }
        }



        public static bool IsInsideVoxel(this IMySlimBlock block, MyGridPlacementSettings settings) { //TODO Add check for slim blocks too
            var def = block.BlockDefinition;
            if (!(def is MyCubeBlockDefinition)) return false;
            
            var CurrentBlockDefinition = def as MyCubeBlockDefinition;
            var cellSize = MyDefinitionManager.Static.GetCubeSize(CurrentBlockDefinition.CubeSize);
            var localBB = new BoundingBoxD(-CurrentBlockDefinition.Size * cellSize * 0.5f, CurrentBlockDefinition.Size * cellSize * 0.5f);
            var isAllowed = MyCubeGrid.IsAabbInsideVoxel(block.FatBlock.WorldMatrix, localBB, settings);
            return !isAllowed;
        }

        public static bool IsInsideVoxels(this IMyPlayer Me_Player) {
            try {
                var Me = Me_Player.Character;
                if (Me == null) return false; 
                var vsettings = new VoxelPlacementSettings {PlacementMode = VoxelPlacementMode.Volumetric, MaxAllowed = 0.95f, MinAllowed = 0};
                var settings = new MyGridPlacementSettings {
                   CanAnchorToStaticGrid = true,
                   EnablePreciseRotationWhenSnapped = true,
                   SearchHalfExtentsDeltaAbsolute = 0,
                   SearchHalfExtentsDeltaRatio = 0,
                   SnapMode = SnapMode.Base6Directions,
                   VoxelPlacement = vsettings
                };

                var worldMatrix = Me.WorldMatrix;
                var localAabb = Me.LocalAABB;

                return MyCubeGrid.IsAabbInsideVoxel(worldMatrix, localAabb, settings);
            } catch (Exception e) {
                Log.Error(e, "[Player_SOS_Rescue]");
                return false;
            }
        }

        public static bool VoxelDetectorForGrids(IMyCubeGrid cubeGrid) { 
            if (IsInVoxels(cubeGrid.WorldMatrix, cubeGrid.LocalAABB))
            {
                var grid_vsettings = new VoxelPlacementSettings();
                grid_vsettings.PlacementMode = VoxelPlacementMode.Volumetric;
                grid_vsettings.MaxAllowed = 0.95f;
                grid_vsettings.MinAllowed = 0;

                MyGridPlacementSettings grid_settings = new MyGridPlacementSettings();//WTF HERE?? grid_settings это настройка проверки с заполнением вокселями блока в 20% для быстрой проверки
                grid_settings.CanAnchorToStaticGrid = true;
                grid_settings.EnablePreciseRotationWhenSnapped = true;
                grid_settings.SearchHalfExtentsDeltaAbsolute = 0;
                grid_settings.SearchHalfExtentsDeltaRatio = 0;
                grid_settings.SnapMode = SnapMode.Base6Directions;
                grid_settings.VoxelPlacement = grid_vsettings;

                return CheckEachGridBlock(cubeGrid, grid_settings);
            }
            return false;
        }

        



        public static bool CheckEachGridBlock(IMyCubeGrid cubeGrid, MyGridPlacementSettings settings)  { // TODO: ready for slow mode optimization
            var blocks = new List<IMySlimBlock>();
            cubeGrid.GetBlocks(blocks, block => true);
            foreach (IMySlimBlock block in blocks) {
                if (!block.IsInsideVoxel(settings)) continue;
                var Block_Type = block.BlockDefinition.Id.SubtypeName;
                Log.Error("Grid in Voxels " + Block_Type);
                return true;
            }

            return false;
        }
    }
}