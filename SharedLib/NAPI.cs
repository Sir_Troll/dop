﻿using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Sandbox.Game.EntityComponents;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRageMath;
using Sandbox.Definitions;

namespace NAPI
{
    public static class NAPI
    {
        private const int FREEZE_FLAG = 4;
        public static bool isFrozen(this IMyEntity grid) { return ((int)grid.Flags | FREEZE_FLAG) == (int)grid.Flags; }
        public static void setFrozen(this IMyEntity grid) { grid.Flags = grid.Flags | (EntityFlags)FREEZE_FLAG; }
        public static void setUnFrozen(this IMyEntity e) { e.Flags &= ~(EntityFlags)FREEZE_FLAG; }

        public static T As<T>(this long entityId)
        {
            IMyEntity entity;
            if (!MyAPIGateway.Entities.TryGetEntityById(entityId, out entity)) return default(T);
            if (entity is T) { return (T)entity; } else { return default(T); }
        }

        public static void FindFatBlocks<T>(this IMyCubeGrid grid, List<T> blocks, Func<IMyCubeBlock, bool> filter)
        {
            var gg = grid as MyCubeGrid;
            var ff = gg.GetFatBlocks();
            foreach (var x in ff)
            {
                var fat = (IMyCubeBlock)x;
                if (filter(fat)) { blocks.Add((T)fat); }
            }
        }

        public static void OverFatBlocks(this IMyCubeGrid grid, Action<IMyCubeBlock> action)
        {
            var gg = grid as MyCubeGrid;
            var ff = gg.GetFatBlocks();
            foreach (var x in ff)
            {
                var fat = (IMyCubeBlock)x;
                action(fat);
            }
        }

        public static List<IMyCubeGrid> GetConnectedGrids(this IMyCubeGrid grid, GridLinkTypeEnum with) { return MyAPIGateway.GridGroups.GetGroup(grid, with); }

        public static IMyFaction PlayerFaction(this long playerId) { return MyAPIGateway.Session.Factions.TryGetPlayerFaction(playerId); }

        public static IMyPlayer GetPlayer(this IMyCharacter character) { return MyAPIGateway.Players.GetPlayerControllingEntity(character); } //CAN BE NULL IF IN COCKPIT
        public static IMyPlayer GetPlayer(this IMyShipController cockpit) { return MyAPIGateway.Players.GetPlayerControllingEntity(cockpit); }

        public static bool IsSameFaction(this long playerId, long player2Id)
        {
            if (playerId == player2Id) return true;

            var f1 = MyAPIGateway.Session.Factions.TryGetPlayerFaction(playerId);
            var f2 = MyAPIGateway.Session.Factions.TryGetPlayerFaction(player2Id);

            if (f1 == f2) { return f1 != null; }

            return false;
        }

        public static Vector3D GetWorldPosition(this IMyCubeBlock slim) { return slim.CubeGrid.GridIntegerToWorld(slim.Position); }

        public static BoundingBoxD GetWorldAABB(this IMyCubeBlock slim)
        {
            var cb = slim.CubeGrid as MyCubeGrid;
            return new BoundingBoxD(slim.Min * cb.GridSize - cb.GridSizeHalfVector, slim.Max * cb.GridSize + cb.GridSizeHalfVector).TransformFast(cb.PositionComp.WorldMatrix);
        }
        public static MatrixD GetWorldMatrix(this IMyCubeBlock slim)
        { //TODO: under development
            var cb = slim.CubeGrid as MyCubeGrid;
            return new MatrixD();
        }

        public static List<IMyEntity> GetEntitiesInSphere(this IMyEntities entities, Vector3D pos, double radius, Func<IMyEntity, bool> filter = null)
        {
            var sphere = new BoundingSphereD(pos, radius);
            var list = entities.GetEntitiesInSphere(ref sphere);
            if (filter != null) { list.RemoveAll((x) => !filter(x)); }
            return list;
        }


        public static void SendMessageToOthersProto(this IMyMultiplayer multi, ushort id, object o, bool reliable = true)
        {
            var bytes = MyAPIGateway.Utilities.SerializeToBinary(o);
            multi.SendMessageToOthers(id, bytes, reliable);
        }

        // Calculates how much components are welded for block
        public static void GetRealComponentsCost(this IMySlimBlock block, Dictionary<string, int> dictionary, Dictionary<string, int> temp = null)
        {
            if (temp == null) temp = new Dictionary<string, int>();
            var components = (block.BlockDefinition as MyCubeBlockDefinition).Components;
            foreach (var component in components)
            {
                string name = component.Definition.Id.SubtypeName;
                int count = component.Count;

                if (dictionary.ContainsKey(name)) dictionary[name] += count;
                else dictionary.Add(name, count);
            }

            temp.Clear();
            block.GetMissingComponents(temp);

            foreach (var component in temp)
            {
                string name = component.Key;
                int count = component.Value;

                if (dictionary.ContainsKey(name)) dictionary[name] -= count;
                else dictionary.Add(name, count);
            }
        }
    }

    public static class Serialization
    {
        public static StringBuilder Serialize(this StringBuilder sb, float f) { sb.Append(f.ToString(CultureInfo.InvariantCulture)); return sb; }
        public static StringBuilder Serialize(this StringBuilder sb, double f) { sb.Append(f.ToString(CultureInfo.InvariantCulture)); return sb; }
        public static StringBuilder Serialize(this StringBuilder sb, int f) { sb.Append(f.ToString(CultureInfo.InvariantCulture)); return sb; }
        public static StringBuilder Serialize(this StringBuilder sb, long f) { sb.Append(f.ToString(CultureInfo.InvariantCulture)); return sb; }

        public static MyModStorageComponentBase GetOrCreateStorage(this IMyEntity entity) { return entity.Storage = entity.Storage ?? new MyModStorageComponent(); }
        public static bool HasStorage(this IMyEntity entity) { return entity.Storage != null; }


        public static string GetStorageData(this IMyEntity entity, Guid guid)
        {
            if (entity.Storage == null) return null;
            string data = null;
            if (entity.Storage.TryGetValue(guid, out data))
            {
                return data;
            }
            else
            {
                return null;
            }
        }

        public static string GetAndSetStorageData(this IMyEntity entity, Guid guid, string newData)
        {
            var data = GetStorageData(entity, guid);
            SetStorageData(entity, guid, newData);
            return data;
        }

        public static void SetStorageData(this IMyEntity entity, Guid guid, String data)
        {
            if (data == null)
            {
                entity.Storage?.RemoveValue(guid);
                return;
            }
            entity.GetOrCreateStorage().SetValue(guid, data);
        }
    }

    public static class Sharp
    {
        public static int NextExcept(this Random r, HashSet<int> ignored, int max)
        {
            while (true)
            {
                var n = r.Next(max);
                if (!ignored.Contains(n)) { return n; }
            }
        }
    }

    public static class Bytes
    {
        public static int Pack(this byte[] bytes, int pos, int what)
        {
            var b1 = BitConverter.GetBytes(what);
            bytes[pos + 0] = b1[0];
            bytes[pos + 1] = b1[1];
            bytes[pos + 2] = b1[2];
            bytes[pos + 3] = b1[3];
            return 4;
        }

        public static int Pack(this byte[] bytes, int pos, byte what)
        {
            bytes[pos] = what;
            return 1;
        }

        public static int Pack(this byte[] bytes, int pos, uint what)
        {
            var b1 = BitConverter.GetBytes(what);
            bytes[pos + 0] = b1[0];
            bytes[pos + 1] = b1[1];
            bytes[pos + 2] = b1[2];
            bytes[pos + 3] = b1[3];
            return 4;
        }

        public static int Pack(this byte[] bytes, int pos, long what)
        {
            var b1 = BitConverter.GetBytes(what);
            bytes[pos + 0] = b1[0];
            bytes[pos + 1] = b1[1];
            bytes[pos + 2] = b1[2];
            bytes[pos + 3] = b1[3];
            bytes[pos + 4] = b1[4];
            bytes[pos + 5] = b1[5];
            bytes[pos + 6] = b1[6];
            bytes[pos + 7] = b1[7];
            return 8;
        }

        public static int Pack(this byte[] bytes, int pos, ulong what)
        {
            var b1 = BitConverter.GetBytes(what);
            bytes[pos + 0] = b1[0];
            bytes[pos + 1] = b1[1];
            bytes[pos + 2] = b1[2];
            bytes[pos + 3] = b1[3];
            bytes[pos + 4] = b1[4];
            bytes[pos + 5] = b1[5];
            bytes[pos + 6] = b1[6];
            bytes[pos + 7] = b1[7];
            return 8;
        }

        public static long Long(this byte[] bytes, int pos) { return BitConverter.ToInt64(bytes, pos); }
        public static ulong ULong(this byte[] bytes, int pos) { return BitConverter.ToUInt64(bytes, pos); }
        public static double Double(this byte[] bytes, int pos) { return BitConverter.ToDouble(bytes, pos); }
        public static int Int(this byte[] bytes, int pos) { return BitConverter.ToInt32(bytes, pos); }
        public static float Float(this byte[] bytes, int pos) { return BitConverter.ToSingle(bytes, pos); }
        public static short Short(this byte[] bytes, int pos) { return BitConverter.ToInt16(bytes, pos); }
        public static ushort UShort(this byte[] bytes, int pos) { return BitConverter.ToUInt16(bytes, pos); }
    }
}